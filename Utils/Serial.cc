
#include "Serial.h"

Serial *dev;

Serial::Serial(const char *_device) {
  device = _device;
  while ((tty_fd = open(device, O_RDWR
       | O_NONBLOCK
        )) < 0)
      ;
  tcgetattr(tty_fd,&old_config);

  memset(&config,0,sizeof(struct termios));
  config.c_iflag=0;
  config.c_oflag=0;
  config.c_cflag=CS8|CREAD|CLOCAL;  // 8n1, see termios.h for more information
  config.c_lflag=0;
  config.c_cc[VMIN]=1;
  config.c_cc[VTIME]=5;

  cfsetospeed(&config,B115200);            // 115200 baud
  cfsetispeed(&config,B115200);            // 115200 baud

  tcsetattr(tty_fd,TCSANOW,&config);
}

Serial::~Serial(void) {
    tcsetattr(tty_fd,TCSANOW,&old_config);
    close(tty_fd);
}

ssize_t Serial::read(char *buff, size_t length) {
  tty_errno = 0;
  ssize_t ret = ::read(tty_fd, buff, length);
  if (ret < 0) {
    tty_errno = errno;
  }
  return ret;
}

ssize_t Serial::write(const char *buff, size_t length) {
  tty_errno = 0;
  ssize_t ret = ::write(tty_fd, buff, length);
  if (ret < 0) {
    tty_errno = errno;
  }
  return ret;
}

int Serial::getErrno() {
  return tty_errno;
}
