
#pragma once

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <pthread.h>

class Serial {
private:
  const char *device;
  struct termios old_config;
  struct termios config;
  speed_t old_ispeed;
  speed_t old_ospeed;
  speed_t ispeed;
  speed_t ospeed;
  int tty_fd;
  int tty_errno;
public:
  
  Serial(const char *_device);
  ~Serial(void);
  
  ssize_t read(char *buff, size_t length);
  ssize_t write(const char *buff, size_t length);  
  
  int getErrno();
};
