// EPOS Castalia Module

#ifndef __epos_module_h
#define __epos_module_h

#include <tstp.h>

__BEGIN_SYS

// EPOS Castalia Module
class EPOS_Module
{
public:
    EPOS_Module() {}
    ~EPOS_Module() {}

    virtual cMessage * EPOS_scheduleAt(simtime_t t, cMessage *msg) = 0;
    virtual void EPOS_cancelAndDelete(cMessage *msg) = 0;

    // *** Just copy the functions below for your EPOS module ***
    // cMessage * EPOS_scheduleAt(simtime_t t, cMessage *msg) { Enter_Method("EPOS_scheduleAt"); cMessage * msg2 = msg->dup(); scheduleAt(t, msg); return msg2; }
    // void EPOS_cancelAndDelete(cMessage *msg) { Enter_Method("EPOS_cancelAndDelete"); cancelAndDelete(msg); }

    virtual TSTP::Microsecond now() = 0;
};

__END_SYS

#endif
