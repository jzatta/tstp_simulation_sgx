// EPOS ARM Cortex Smart Transducer Declarations

#ifndef __cortex_transducer_h
#define __cortex_transducer_h

#include <smart_data.h>
#include <tstp.h>

__BEGIN_SYS

enum CUSTOM_UNITS
{
    UNIT_RFID = TSTP::Unit::DIGITAL | 1,
    UNIT_SWITCH = TSTP::Unit::DIGITAL | 2,
    UNIT_TEST = TSTP::Unit::DIGITAL | 3,
};

class Temperature_Sensor // TODO
{
public:
    static const unsigned int UNIT = TSTP::Unit::Temperature;
    static const unsigned int NUM = TSTP::Unit::I32;
    static const int ERROR = 0; // Unknown

    static const bool INTERRUPT = false;
    static const bool POLLING = true;

    typedef _UTIL::Observer Observer;
    typedef _UTIL::Observed Observed;
    typedef Smart_Data<Temperature_Sensor>::Value Value;

    static const unsigned int SIZE = 100;

public:
    Temperature_Sensor() { }

    static void sense(unsigned int dev, Smart_Data<Temperature_Sensor> * data) {
        if(!_serie) bootstrap();
        if(_index < SIZE)
            data->_value = _serie[_index++];
        else
            data->_value = 0;
    }

    static void actuate(unsigned int dev, Smart_Data<Temperature_Sensor> * data, const Smart_Data<Temperature_Sensor>::Value & command) {}

    static void attach(Observer * obs) { _observed.attach(obs); }
    static void detach(Observer * obs) { _observed.detach(obs); }

    static void bootstrap() {
        _serie = new Value[SIZE];
        for(unsigned int i = 0 ; i < SIZE; i++){
            for(unsigned int j = 0; i < SIZE && j < SIZE/4; i++, j++)
                _serie[i] = i*2+00;
            for(unsigned int j = 0; i < SIZE && j < SIZE/4; i++, j++)
                _serie[i] = i*3+00;
            for(unsigned int j = 0; i < SIZE && j < SIZE/4; i++, j++)
                _serie[i] = i*4+00;
            for(unsigned int j = 0; i < SIZE && j < SIZE/4; i++, j++)
                _serie[i] = i*10+00;
        }
        _index = 0;
    }

private:
    static bool notify() { return _observed.notify(); }

    static void init();

private:
    static Observed _observed;
    static Value * _serie;
    static unsigned int _index;
};


__END_SYS

typedef Smart_Data<Temperature_Sensor> Temperature;

//#include "transducer.cc" // Static attributes go here

#endif
