// EPOS Periodic Thread Component Declarations

// Periodic threads are achieved by programming an alarm handler to invoke
// p() on a control semaphore after each job (i.e. task activation). Base
// threads are created in BEGINNING state, so the scheduler won't dispatch
// them before the associate alarm and semaphore are created. The first job
// is dispatched by resume() (thus the _state = SUSPENDED statement)

#ifndef __periodic_thread_h
#define __periodic_thread_h

#include "CastaliaModule.h"
#include "PeriodicThreadMessage_m.h"
#include <tstp_common.h>
#include <epos_module.h>
#include <atomic>
#include <tuple>
#include <type_traits>
#include <simutil.h>

#define THREAD_MAX_SIZE 256

__BEGIN_SYS

//#####################################################################
template<class T, T... Ints> struct integer_sequence { };
template<class S> struct next_integer_sequence;
template<class T, T... Ints> struct next_integer_sequence<integer_sequence<T, Ints...>>
{
    using type = integer_sequence<T, Ints..., sizeof...(Ints)>;
};
template<class T, T I, T N> struct make_int_seq_impl;
template<class T, T N>
    using make_integer_sequence = typename make_int_seq_impl<T, 0, N>::type;
template<class T, T I, T N> struct make_int_seq_impl
{
    using type = typename next_integer_sequence<
        typename make_int_seq_impl<T, I+1, N>::type>::type;
};
template<class T, T N> struct make_int_seq_impl<T, N, N>
{
    using type = integer_sequence<T>;
};
template<std::size_t... Ints> using index_sequence = integer_sequence<std::size_t, Ints...>;
template<std::size_t N> using make_index_sequence = make_integer_sequence<std::size_t, N>;

template< typename... Ts >
      using index_sequence_for = make_index_sequence< sizeof...( Ts ) >;
//#####################################################################


class Entry_Helper
{
public:
    // A handler function
    typedef void (Function)();

public:
    Entry_Helper() {}
    virtual ~Entry_Helper() {}

    virtual void operator()() = 0;
};


template<typename ... Tn>
class Functor_Helper : public Entry_Helper
{
public:
    typedef int (Functor)(Tn...);

    Functor_Helper(Functor * h, Tn ... an): _entry(h), _arguments(an...) {}

    void operator()() { callFunc(index_sequence_for<Tn...>{}); }

private:
    Functor * _entry;

    std::tuple<Tn...> _arguments;

    template<std::size_t... Is>
    void callFunc(index_sequence<Is...>){
        _entry(std::get<Is>(_arguments) ...);
    }
};

// Periodic Thread
class Periodic_Thread
{
public:
    typedef TSTP_Common::Microsecond Microsecond;

public:
    template<typename ... Tn>
    Periodic_Thread(EPOS_Module * epos_module, const Microsecond & p, int (* entry)(Tn ...), Tn ... an)
    : _epos_module(epos_module), _period(p), _thread_index(seq_index++) {

        if(_thread_index >= THREAD_MAX_SIZE)
            opp_error("ThreadPeriodica(): threadIndex=%i is too large", _thread_index);

        _entry = new Functor_Helper<Tn...>(entry, an...);

        reset_timer();
        threads[_thread_index] = this;
    }

    ~Periodic_Thread() {
        cancel_timer();
        threads[_thread_index] = NULL;
    }

    void cancel_timer() {
        if (_tmp != NULL && _tmp->isScheduled())
            _epos_module->EPOS_cancelAndDelete(_tmp);
        _tmp = NULL;
    }

    void reset_timer() {
        cancel_timer();
        PeriodicThreadMessage * tmp = new PeriodicThreadMessage("PeriodicThreadMessage", PERIODIC_THREAD_MESSAGE);
        tmp->setThreadIndex(_thread_index);
        _tmp = check_and_cast<PeriodicThreadMessage*>(_epos_module->EPOS_scheduleAt(simTime() + simtime_t(_period, SIMTIME_US), tmp));
        delete tmp;
    }

    const Microsecond & period() const { return _period; }
    void period(const Microsecond & p) { _period = p; }

    static volatile bool wait_next() { return false; }

    int getId() { return _thread_index; }

    static void handleMessage(cMessage * msg) {
        PeriodicThreadMessage *periodicThreadMessage = check_and_cast <PeriodicThreadMessage*>(msg);
        Periodic_Thread::getThread(periodicThreadMessage->getThreadIndex())->timerFiredCallback(msg);
    }

private:
    EPOS_Module * _epos_module;
    Microsecond _period;
    int _thread_index;
    PeriodicThreadMessage * _tmp = 0;
    Entry_Helper * _entry;

protected:
    static std::atomic<int> seq_index;
    static Periodic_Thread * threads[THREAD_MAX_SIZE];

    std::ostream & trace() { return (ostream &) DebugInfoWriter::getStream() << "\n"; }// << setw(18) << simTime() << setw(40) << _cm->getFullPath() << " "; }
    std::ostream & debugC() { return (ostream &) DebugInfoWriter::getStream() << "\n";}// << setw(18) << simTime() << setw(40) << _cm->getFullPath() << " (DEBUG) PeriodicThread: "; }

public:
    static Periodic_Thread * getThread(int threadIndex){ return Periodic_Thread::threads[threadIndex]; }
    void timerFiredCallback(cMessage * msg);
};

__END_SYS

#endif
