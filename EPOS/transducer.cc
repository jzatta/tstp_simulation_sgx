// EPOS ARM Cortex Smart Transducer Static Attributes
// File included at the end of include/machine/cortex/transducer.h
// We use this mechanism to avoid always including smart_data.h (which uses periodic thread, which requires specific scheduling policies) when it is not needed by the application

#include <transducer.h>

#ifdef __cortex_transducer_h

__BEGIN_SYS

Temperature_Sensor::Observed Temperature_Sensor::_observed;

__END_SYS

#endif

Temperature_Sensor::Value * Temperature_Sensor::_serie = 0;
unsigned int Temperature_Sensor::_index = 0;
