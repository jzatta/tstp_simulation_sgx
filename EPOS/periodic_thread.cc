#include <periodic_thread.h>

__BEGIN_SYS

std::atomic<int> Periodic_Thread::seq_index;
Periodic_Thread * Periodic_Thread::threads[THREAD_MAX_SIZE];
Periodic_Thread * getThread(int threadIndex);
void handleMessage(cMessage * msg);

void Periodic_Thread::timerFiredCallback(cMessage * msg)
{
    if (((int)msg->getKind()) == PERIODIC_THREAD_MESSAGE) {
        (*_entry)();
        reset_timer();
    }
}

__END_SYS
