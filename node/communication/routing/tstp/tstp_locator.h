

class Locator: private IEEE802_15_4::Observer
{
    friend class TSTP::Router;

    typedef char RSSI;

    struct Peer {
        Coordinates coordinates;
        Percent confidence;
        RSSI rssi;
    };

public:
    Locator(TSTP * tstp) : _tstp(tstp), _bootstrapped(false) {
        _tstp->trace() << "TSTP::Locator()";
        _n_peers = 0;
        _confidence = 0;
        _here = Coordinates(Random::random(), Random::random(), Random::random());
    }
    virtual ~Locator();

    Coordinates here() { return _here; }

    void set_sink_coordinates();
    void bootstrap();

    bool synchronized() { return _confidence >= 80; }

    bool bootstrapped() { return _bootstrapped; }

    void marshal(Buffer * buf);

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

private:
    void add_peer(Coordinates coord, Percent conf, RSSI r) {
        _tstp->trace() << "TSTP::Locator::add_peer(c=" << coord << ",conf=" << conf << ",r=" << static_cast<int>(r) << ")";

        unsigned int idx = -1u;

        for(unsigned int i = 0; i < _n_peers; i++) {
            if(_peers[i].coordinates == coord) {
                if(_peers[i].confidence > conf)
                    return;
                else {
                    idx = i;
                    break;
                }
            }
        }

        if(idx == -1u) {
            if(_n_peers < 3)
                idx = _n_peers++;
            else
                for(unsigned int i = 0; i < _n_peers; i++)
                    if((_peers[i].confidence <= conf) && ((idx == -1u) || (conf >= _peers[i].confidence)))
                        idx = i;
        }

        if(idx != -1u) {
            _peers[idx].coordinates = coord;
            _peers[idx].confidence = conf;
            _peers[idx].rssi = r;

            if(_n_peers == 3) {
                Coordinates new_here = _here.trilaterate(_peers[0].coordinates, _peers[0].rssi + 128, _peers[1].coordinates, _peers[1].rssi + 128, _peers[2].coordinates, _peers[2].rssi + 128);
                if(new_here != TSTP::sink()) {
                    _here = new_here;
                    _confidence = (_peers[0].confidence + _peers[1].confidence + _peers[2].confidence) * 80 / 100 / 3;
                    _tstp->trace() << "TSTP::Locator: Location updated: " << _here << ", confidence = " << _confidence << "%";
                } else {
                    _tstp->trace() << "TSTP::Locator: Dropped trilateration that resulted in here == sink";
                }
            }
        }
    }

private:
    Coordinates _here;
    unsigned int _n_peers;
    Percent _confidence;
    Peer _peers[3];
    TSTP * _tstp;
    bool _bootstrapped;
};
