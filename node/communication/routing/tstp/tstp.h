#ifndef TSTP_H_
#define TSTP_H_

#include <ostream>
#include <cmath>

#include <omnetpp.h>
#include "VirtualRouting.h"
#include "VirtualMobilityManager.h"
#include "TSTPPacket_m.h"

#include <tstp_common.h>
#include <hash.h>
#include <tstp_mac_nomf.h>
#include <tstp_mac.h>
#include <array.h>
#include <list.h>
#include <diffie_hellman.h>
#include <poly1305.h>

#include "Serial.h"

__USING_SYS

typedef NIC_Common::NIC_Base<IEEE802_15_4, true> NIC;

class TSTP: public TSTP_Common, private IEEE802_15_4::Observer, public VirtualRouting
{
    friend class TSTP_MAC_NED;
    friend class TSTP_MAC_NOMF_NED;
    friend class TSTP_SYNC_MAC_NED;
    friend class TSTP_SYNC_MAC_NOMF_NED;
    template<typename> friend class Smart_Data;
    friend class Epoch;
    friend class Security;

    struct Value_Timestamp {
        typedef uint32 Value;
        typedef simtime_t Timestamp;

        Value value;
        Timestamp timestamp;
    };

protected:
    enum {
        LIFEKEEPER_TIMER,
        KEY_MANAGER_TIMER,
        AUTH_REQUEST_TIMER,
        CHECK_SGX,
    };

    // Castalia bindings
    void initialize();
    void startup();
    void finishSpecific();
    void fromApplicationLayer(cPacket *, const char *);
    void fromMacLayer(cPacket *, int, double, double);
    void timerFiredCallback(int);
    static char _app_id[2048];
public:
    std::ostream & trace() {return VirtualRouting::trace();}
    typedef NIC::Buffer Buffer;
    typedef Radio_Engine Radio;

    // Packet
    template<Scale S>
    class _Packet: public Header
    {
    private:
        typedef unsigned char Data[MTU];

    public:
        _Packet() {}

        Header * header() { return this; }

        template<typename T>
        T * data() { return reinterpret_cast<T *>(&_data); }

        friend OStream & operator<<(OStream & db, const _Packet & p) {
            switch(reinterpret_cast<const Header &>(p).type()) {
                case INTEREST:
                    db << reinterpret_cast<const Interest &>(p);
                    break;
                case RESPONSE:
                    db << reinterpret_cast<const Response &>(p);
                    break;
                case COMMAND:
                    db << reinterpret_cast<const Command &>(p);
                    break;
                case CONTROL: {
                    switch(reinterpret_cast<const Control &>(p).subtype()) {
                        case DH_RESPONSE:
                            db << reinterpret_cast<const DH_Response &>(p);
                            break;
                        case AUTH_REQUEST:
                            db << reinterpret_cast<const Auth_Request &>(p);
                            break;
                        case DH_REQUEST:
                            db << reinterpret_cast<const DH_Request &>(p);
                            break;
                        case AUTH_GRANTED:
                            db << reinterpret_cast<const Auth_Granted &>(p);
                            break;
                        case REPORT:
                            db << reinterpret_cast<const Report &>(p);
                            break;
                        case KEEP_ALIVE:
                            db << reinterpret_cast<const Keep_Alive &>(p);
                            break;
                        case EPOCH:
                            db << reinterpret_cast<const Epoch &>(p);
                            break;
                        default:
                            break;
                    }
                }
                default:
                    break;
            }
            return db;
        }

    private:
        Data _data;
    } __attribute__((packed));
    typedef _Packet<SCALE> Packet;


    // TSTP observer/d conditioned to a message's address (ID)
    typedef _UTIL::Data_Observer<Buffer, int64> Observer;
    typedef _UTIL::Data_Observed<Buffer, int64> Observed;


    // Hash to store TSTP Observers by type
    class Interested;
    typedef _UTIL::Hash<Interested, 10, Unit> Interests;
    class Responsive;
    typedef _UTIL::Hash<Responsive, 10, Unit> Responsives;


    // TSTP Messages
    // Each TSTP message is encapsulated in a single package. TSTP does not need nor supports fragmentation.

    // Interest/Response Modes
    enum Mode {
        // Response
        SINGLE = 0, // Only one response is desired for each interest job (desired, but multiple responses are still possible)
        ALL    = 1, // All possible responses (e.g. from different sensors) are desired
        // Interest
        REVOKE = 2  // Revoke an interest
    };

#include "tstp_types.h"
    
    class Router;

    // TSTP Locator
#include "tstp_locator.h"


    // TSTP Timekeeper
#include "tstp_timekeeper.h"


    // TSTP Router
#include "tstp_router.h"


    // TSTP Security
#include "tstp_security.h"
    

    // TSTP Life Keeper explicitly asks for Metadata whenever it's needed
    class Life_Keeper
    {
        friend class TSTP;

        static unsigned int PERIOD;

        Life_Keeper(TSTP * tstp) : _tstp(tstp) {
            PERIOD = _tstp->par("keepAlivePeriod").doubleValue();
            if(PERIOD)
                _tstp->setTimer(_tstp->LIFEKEEPER_TIMER, simtime_t(PERIOD, SIMTIME_US));
        }

    private:
        int life_keeper() {
            _tstp->setTimer(_tstp->LIFEKEEPER_TIMER, simtime_t(PERIOD, SIMTIME_US));
            if(_tstp->here() != _tstp->sink()) {
                _tstp->trace()
                    << "TSTP::Life_Keeper: sending keep alive message";
                _tstp->keep_alive();
            }
            return 0;
        }

        TSTP * _tstp;
    };

public:
    TSTP();
    ~TSTP();

    // Local network Space-Time
    Coordinates here() { return _locator->here(); }
    Time now() { return _timekeeper->now(); }
    static Coordinates sink(unsigned int unit = 0) { return *_sinks[unit]->object(); }
    Time local(const Time & global) { return global - _epoch; }
    Coordinates local(Global_Coordinates global) {
        global -= _global_coordinates;
        return Coordinates(global.x, global.y, global.z);
    }

    void reset_lifekeeper();

    // Global Space-Time
    Global_Coordinates absolute(const Coordinates & coordinates) { return _global_coordinates + coordinates; }
    Time absolute(const Time & t) {
        if((t == static_cast<Time>(-1)) || (t == 0))
           return t;
        return _epoch + t;
    }
    void epoch(const Time & t) { _epoch = t - now(); }
    void coordinates(const Global_Coordinates & c) { _global_coordinates = c; }

    void attach(Observer * obs, void * subject) { _observed.attach(obs, int64(subject)); }
    void detach(Observer * obs, void * subject) { _observed.detach(obs, int64(subject)); }
    bool notify(void * subject, Buffer * buf);

    template<unsigned int UNIT>
    static void init(const NIC & nic);

protected:
    void bootstrap();

private:
    Region destination(Buffer * buf) {
        switch(buf->frame()->data<Frame>()->type()) {
            case INTEREST:
                return buf->frame()->data<Interest>()->region();
            case RESPONSE:
                return Region(sink(), 0, buf->frame()->data<Response>()->time(), buf->frame()->data<Response>()->expiry());
            case COMMAND:
                return buf->frame()->data<Command>()->region();
            case CONTROL:
                switch(buf->frame()->data<Control>()->subtype()) {
                    default:
                    case DH_RESPONSE:
                    case AUTH_REQUEST: {
                        Time origin = buf->frame()->data<Header>()->time();
                        Time deadline = origin + min(static_cast<unsigned long long>(_security->KEY_MANAGER_PERIOD), _security->KEY_EXPIRY) / 2;
                        return Region(sink(), 0, origin, deadline);
                    }
                    case DH_REQUEST: {
                        Time origin = buf->frame()->data<Header>()->time();
                        Time deadline = origin + min(static_cast<unsigned long long>(_security->KEY_MANAGER_PERIOD), _security->KEY_EXPIRY) / 2;
                        return Region(buf->frame()->data<DH_Request>()->destination().center, buf->frame()->data<DH_Request>()->destination().radius, origin, deadline);
                    }
                    case AUTH_GRANTED: {
                        Time origin = buf->frame()->data<Header>()->time();
                        Time deadline = origin + min(static_cast<unsigned long long>(_security->KEY_MANAGER_PERIOD), _security->KEY_EXPIRY) / 2;
                        return Region(buf->frame()->data<Auth_Granted>()->destination().center, buf->frame()->data<Auth_Granted>()->destination().radius, origin, deadline);
                    }
                    case REPORT: {
                        return Region(sink(), 0, buf->frame()->data<Report>()->time(), -1/*TODO*/);
                    }
                    case KEEP_ALIVE: {
                        while(true) {
                            Coordinates fake(here().x + (Random::random() % (RADIO_RANGE / 3)), here().y + (Random::random() % (RADIO_RANGE / 3)), (here().z + Random::random() % (RADIO_RANGE / 3)));
                            if(fake != here())
                                return Region(fake, 0, 0, -1); // Should never be destined_to_me
                        }
                    }
                    case EPOCH: {
                        return buf->frame()->data<Epoch>()->destination();
                    }
                }
            default:
                trace() << "TSTP::destination(): ERROR: unrecognized frame type " << buf->frame()->data<Frame>()->type();
                return Region(TSTP::here(), 0, TSTP::now() - 2, TSTP::now() - 1);
        }
    }

    void keep_alive(bool unset_time_request = false) {
        trace() << "TSTP::keep_alive()";
        Buffer * buf = alloc(sizeof(Keep_Alive));
        Keep_Alive * keep_alive = new (buf->frame()->data<Keep_Alive>()) Keep_Alive;
//        id++;
//        id = id & Microframe::ID_MAX;
//        keep_alive->_crc = id;
        marshal(buf);
        buf->deadline = now() + Life_Keeper::PERIOD;
        if(unset_time_request)
            buf->frame()->data<Header>()->time_request(false);
        trace() << "TSTP::keep_alive():keep_alive = " << keep_alive << " => " << (*keep_alive);
        send(buf);
    }

    void marshal(Buffer * buf) {
        _locator->marshal(buf);
        _timekeeper->marshal(buf);
        _router->marshal(buf);
        _security->marshal(buf);
    }

    Buffer * alloc(unsigned int size) {
        _allocated_buffers++;
        return _nic->alloc(0, NIC::Address::BROADCAST, NIC::TSTP, 0, 0, size - sizeof(Header));
    }

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

    typedef _UTIL::Vector<Coordinates, MAX_SINKS> Sinks;

    void schedule_auth_request() {
        // Castalia workaround
        Enter_Method("schedule_auth_request()");
        setTimer(AUTH_REQUEST_TIMER, simtime_t(_security->DHREQUEST_AUTH_DELAY, SIMTIME_US));
    }
    
    int send(NIC::Buffer * buf) {
        return _nic->send(buf);
    }

private:
    NIC * _nic;
    Interests _interested;
    Responsives _responsives;
    Observed _observed; // Channel protocols are singletons
    Time _epoch;
    Global_Coordinates _global_coordinates;

    unsigned int _unit;
    static unsigned int _units;
    static TSTP * _instances[2048];
    static TSTP * _instance;


    bool _sink;

    Locator * _locator;
    Timekeeper * _timekeeper;
    Router * _router;
    Security * _security;
    Life_Keeper * _life_keeper;

    std::vector<std::pair<Coordinates, Time>> _last_message;

    Time _expiry;

    static Sinks _sinks;
    Sinks::Element * _sinks_element;
    static unsigned int _n_sinks;
    unsigned int _allocated_buffers;
    static Frame_ID id;
    bool _encrypt;
};

#endif
