
#include "tstp.h"

void TSTP::Locator::set_sink_coordinates()
{
    assert(_tstp->par("anchor"));
    assert(_tstp->_sink);

    VirtualMobilityManager *nodeMobilityModule = check_and_cast<
        VirtualMobilityManager*>(
            _tstp->getParentModule()->getParentModule()->getParentModule()->getSubmodule(
                "node", _tstp->self)->getSubmodule("MobilityManager"));

    Coordinates::Number x = nodeMobilityModule->getLocation().x * 100.0;
    Coordinates::Number y = nodeMobilityModule->getLocation().y * 100.0;
    Coordinates::Number z = nodeMobilityModule->getLocation().z * 100.0;

    _here = Coordinates(x, y, z);
    _confidence = 100;

    _tstp->trace() << "_here = " << _here;
    _tstp->_sinks_element = new Sinks::Element(&_here);
    _tstp->_sinks.insert(_tstp->_sinks_element, _tstp->_n_sinks++);
}

void TSTP::Locator::bootstrap()
{
    _tstp->trace() << "Locator::bootstrap()";

    _tstp->_nic->attach(this, NIC::TSTP);

    if(_tstp->_sink) {
        _bootstrapped = true;

        _tstp->_timekeeper->bootstrap();
    } else if(_tstp->par("anchor")) {

        VirtualMobilityManager *nodeMobilityModule = check_and_cast<
            VirtualMobilityManager*>(
                _tstp->getParentModule()->getParentModule()->getParentModule()->getSubmodule(
                    "node", _tstp->self)->getSubmodule("MobilityManager"));

        Coordinates::Number x = nodeMobilityModule->getLocation().x * 100.0;
        Coordinates::Number y = nodeMobilityModule->getLocation().y * 100.0;
        Coordinates::Number z = nodeMobilityModule->getLocation().z * 100.0;

        _here = Coordinates(x, y, z);

        _tstp->trace() << "_here = " << _here;

        _confidence = 100;
        _bootstrapped = true;

        _tstp->_timekeeper->bootstrap();
    } else {
        _here = _tstp->sink() + Coordinates(1, 1, 1);
        _confidence = 0;
    }
}

// TSTP::Locator
// Class attributes


// Methods
void TSTP::Locator::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    bool was_bootstrapped = bootstrapped();

    _tstp->trace() << "TSTP::Locator::update(obs=" << obs << ",buf=" << buf << ")";
    if(buf->is_microframe) {
        buf->sender_distance = buf->hint; // This would fit better in the Router, but Timekeeper uses this info
        if(!synchronized())
            buf->relevant = true;
        else if(!buf->downlink)
            buf->my_distance = here() - TSTP::sink();
    } else {
        Coordinates dst = _tstp->destination(buf).center;
        buf->sender_distance = buf->frame()->data<Header>()->last_hop() - dst;
        if(_confidence < 100) {
            Header * h = buf->frame()->data<Header>();
            if(h->confidence() > 80)
                add_peer(h->last_hop(), h->confidence(), buf->rssi);
        }

        buf->my_distance = here() - dst;
        buf->downlink = dst != TSTP::sink(); // This would fit better in the Router, but Timekeeper uses this info

        // Respond to Keep Alive if sender is low on location confidence
        if(synchronized()) {
            _bootstrapped = true;
            Header * header = buf->frame()->data<Header>();
            if(header->type() == CONTROL) {
                Control * control = buf->frame()->data<Control>();
                if((control->subtype() == KEEP_ALIVE) && (header->confidence() < 80))
                    _tstp->keep_alive();
            }
        }
    }

    if((!was_bootstrapped) && bootstrapped())
        _tstp->_timekeeper->bootstrap();
}

void TSTP::Locator::marshal(Buffer * buf)
{
    _tstp->trace() << "TSTP::Locator::marshal(buf=" << buf << ")";
    Coordinates dst = _tstp->destination(buf).center;
    buf->my_distance = here() - dst;
    if(buf->is_new)
        buf->sender_distance = buf->my_distance;
    buf->downlink = dst != TSTP::sink(); // This would fit better in the Router, but Timekeeper uses this info
    buf->frame()->data<Header>()->confidence(_confidence);
    Coordinates here = _tstp->here();
    buf->frame()->data<Header>()->origin(here);
    buf->frame()->data<Header>()->last_hop(here);
}

TSTP::Locator::~Locator()
{
    _tstp->trace() << "TSTP::~Locator()";
    if(_tstp->_sinks_element) {
        _tstp->_sinks.remove(_tstp->_sinks_element);
        _tstp->_n_sinks--;
        delete _tstp->_sinks_element;
    }
    //_tstp->_nic->detach(this, 0);
}
