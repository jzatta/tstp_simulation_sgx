
#include "tstp.h"

unsigned int TSTP::Router::RADIO_RANGE;
bool TSTP::Router::track_offsets;
vector<unsigned long long> TSTP::Router::_offsets;

void TSTP::Router::bootstrap()
{
    _tstp->trace() << "TSTP::Router::bootstrap()";

    expiry_metric = _tstp->par("useExpiryMetric");
    effort_metric = _tstp->par("useEffortMetric");
    random_metric = _tstp->par("useRandomMetric");
    distance_metric = _tstp->par("useDistanceMetric");
    zero_metric = !(expiry_metric || effort_metric || random_metric || distance_metric);

    _forwarder = _tstp->par("forwarder");
    RADIO_RANGE = TSTP_Common::RADIO_RANGE;
    EXPIRY_BETHA = _tstp->par("expiryBetha").doubleValue() * 1000000.0;

    if(!track_offsets)
        track_offsets = _tstp->par("trackOffsets");

    _tstp->_nic->attach(this, NIC::TSTP);

    _tstp->_security->bootstrap();
}

// TSTP::Router
// Class attributes

// Methods
void TSTP::Router::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    _tstp->trace() << "TSTP::Router::update(obs=" << obs << ",buf=" << buf << ")";
    if(buf->is_microframe) {
        buf->progress_bits = buf->my_distance < buf->sender_distance;
        if(!buf->relevant)
            buf->relevant = _forwarder && buf->progress_bits;
    } else {
        Header * header = buf->frame()->data<Header>();
        // Keep Alive messages are never forwarded
        if((header->type() == CONTROL) && (buf->frame()->data<Control>()->subtype() == KEEP_ALIVE))
            buf->destined_to_me = false;
        else {
            Region dst = _tstp->destination(buf);
            buf->destined_to_me = ((header->origin() != _tstp->here()) && (dst.contains(_tstp->here(), dst.t0)));

            if(forward(buf)) {

                _tstp->trace() << "Forwarding message";

                // Forward or ACK the message

                Buffer * send_buf = _tstp->alloc(buf->size());

                // Copy frame contents
                memcpy(send_buf->frame(), buf->frame(), buf->size());

                // Copy Buffer Metainformation
                send_buf->size(buf->size());
                send_buf->id = buf->id;
                send_buf->destined_to_me = buf->destined_to_me;
                send_buf->downlink = buf->downlink;
                send_buf->deadline = buf->deadline;
                send_buf->my_distance = buf->my_distance;
                send_buf->sender_distance = buf->sender_distance;
                send_buf->is_new = false;
                send_buf->is_microframe = false;
                send_buf->progress_bits = buf->progress_bits;

                // Calculate offset
                offset(send_buf);

                // Adjust Last Hop location
                Header * header = send_buf->frame()->data<Header>();
                header->last_hop(_tstp->here());
                send_buf->sender_distance = send_buf->my_distance;

                header->confidence(_tstp->_locator->_confidence);
                header->time_request(_tstp->_timekeeper->sync_required());

                send_buf->hint = send_buf->my_distance;

                _tstp->send(send_buf);
            }
        }
    }
}

void TSTP::Router::marshal(Buffer * buf)
{
    _tstp->trace() << "TSTP::Router::marshal(buf=" << buf << ")";
    TSTP::Region dest = _tstp->destination(buf);
    buf->downlink = dest.center != _tstp->sink();
    buf->destined_to_me = (buf->frame()->data<Header>()->origin() != _tstp->here()) && (dest.contains(_tstp->here(), _tstp->now()));
    buf->hint = buf->my_distance;
    buf->progress_bits = 1;

    offset(buf);
}

TSTP::Router::~Router()
{
    _tstp->trace() << "TSTP::~Router()";
    //_tstp->_nic->detach(this, 0);
}
