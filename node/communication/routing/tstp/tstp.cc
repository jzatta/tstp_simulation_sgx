#include <castalia_nic.h>

#include <tstp.h>

#include <cstring>
#include <numeric>

Define_Module (TSTP);

#ifdef EXPLICIT_SYNC_ONLY
__BEGIN_SYS
template<>
TSTP_Common::Time TSTP_Common::Header::last_hop_time() const {
    if(type() == CONTROL && reinterpret_cast<const Control*>(this)->subtype() == KEEP_ALIVE)
        return reinterpret_cast<const Keep_Alive*>(this)->last_hop_time();
    assert(false);
    return 0;
}

template<>
void TSTP_Common::Header::last_hop_time(const Time & t) {
    if(type() == CONTROL && reinterpret_cast<Control*>(this)->subtype() == KEEP_ALIVE)
        reinterpret_cast<Keep_Alive*>(this)->last_hop_time(t);
}
__END_SYS
#endif

__USING_SYS

char TSTP::_app_id[2048];
unsigned int TSTP_Common::RADIO_RANGE;
bool TSTP_Common::drop_expired;
TSTP::Sinks TSTP::_sinks;
unsigned int TSTP::_n_sinks;
unsigned int TSTP::Life_Keeper::PERIOD;
TSTP::Frame_ID TSTP::id;
TSTP * TSTP::_instances[2048];
TSTP * TSTP::_instance;
unsigned int TSTP::_units;

TSTP::TSTP() : _nic(0),  _unit(_units++), _locator(0), _timekeeper(0), _router(0), _security(0), _life_keeper(0), _sinks_element(0), _allocated_buffers(0)
{
    trace() << "TSTP::TSTP()";
    assert(_units <= sizeof(_instances) / sizeof(TSTP *));
    _instances[_unit] = this;
    _locator = new Locator(this);
    _timekeeper = new Timekeeper(this);
    _router = new Router(this);
    if (_unit == 0) {
#include "FULL_FILE"
#if FULL==42
        _security = new Security_Full(this);
#else
        _security = new Security_Stub(this);
#endif
        _instance = this;
    }
    else {
        _security = new Security_EPOS(this);
    }
}

TSTP::~TSTP()
{
    trace() << "TSTP::~TSTP()";
    if(_life_keeper)
        delete _life_keeper;
    if(_security)
        delete _security;
    if(_router)
        delete _router;
    if(_locator)
        delete _locator;
    if(_timekeeper)
        delete _timekeeper;
    //if(_nic)
    //    _nic->detach(this, 0);
}

void TSTP::timerFiredCallback(int timer)
{
    switch(timer) {
    case LIFEKEEPER_TIMER: {
        _life_keeper->life_keeper();
    } break;
    case KEY_MANAGER_TIMER: {
        _security->key_manager();
    } break;
    case AUTH_REQUEST_TIMER: {
        _security->auth_request();
    } break;
    case CHECK_SGX: {
        ((TSTP::Security_Stub *)_security)->check();
    } break;
    }
}

void TSTP::initialize() // Called before any startup() method
{
    VirtualRouting::initialize();

    trace() << "TSTP::startup()";
    _sink = par("sink");
    _expiry = par("expiry").doubleValue();
    _encrypt = par("encrypt");
    TSTP_Common::RADIO_RANGE = par("radioRange");
    TSTP_Common::drop_expired = par("dropExpired");
    trace() << "I am a " << (_sink ? "sink" : "sensor");
    trace() << "_expiry = " << _expiry;

    if(_sink)
        _locator->set_sink_coordinates();

    _timekeeper->_adjusts = new cOutVector("Timer adjusts");
}

void TSTP::startup()
{
    _life_keeper = new Life_Keeper(this);
    _locator->bootstrap();
}

void TSTP::fromMacLayer(cPacket * pkt, int a, double b, double c)
{
    if(!strcmp(pkt->getName(), "TSTP pointer check")) {
        if(_nic != reinterpret_cast<NIC *>(pkt->getBitLength()))
            throw cRuntimeError("NIC pointers got mixed up!");
    } else if(!strcmp(pkt->getName(), "TSTP data hook")) {
        TSTPPacket * p = check_and_cast<TSTPPacket *>(pkt);
        toApplicationLayer(p->decapsulate());
    }
}

void TSTP::bootstrap()
{
    trace() << "TSTP::bootstrap()";
    _nic->attach(this, NIC::TSTP);
}

void TSTP::finishSpecific()
{
    recordScalar("Here x", here().x);
    recordScalar("Here y", here().y);
    recordScalar("Here z", here().z);
    recordScalar("Using Effort Metric", _router->effort_metric);
    recordScalar("Using Expiry Metric", _router->expiry_metric);
    recordScalar("Using Random Metric", _router->random_metric);
    recordScalar("Using Distance Metric", _router->distance_metric);
    recordScalar("Keepalive period", _life_keeper->PERIOD);
    recordScalar("Expiry", _expiry);
    recordScalar("Time requests", _timekeeper->_time_request);
    recordScalar("Timekeeper always relevant", _timekeeper->_always_relevant);
    if(_unit == 0) {
        recordScalar("Max sync diff", _timekeeper->max_sync_diff);
        if(_router->track_offsets) {
            unsigned long long min = 0, max = 0;
            double mean = 0, stdev = 0, sum = 0;

            if(_router->_offsets.size()) {
                min = *min_element(_router->_offsets.begin(), _router->_offsets.end());
                max = *max_element(_router->_offsets.begin(), _router->_offsets.end());

                sum = std::accumulate(_router->_offsets.begin(), _router->_offsets.end(), 0.0);
                mean = sum / _router->_offsets.size();
            }

            if(min != max) {
                std::vector<double> diff(_router->_offsets.size());
                std::transform(_router->_offsets.begin(), _router->_offsets.end(), diff.begin(), [mean](double x) { return x - mean; });

                double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
                stdev = std::sqrt(sq_sum / _router->_offsets.size());
            }

            recordScalar("Offset max", max);
            recordScalar("Offset min", min);
            recordScalar("Offset mean", mean);
            recordScalar("Offset sum", sum);
            recordScalar("Offset standard deviation", stdev);
        }

        recordScalar("Global time requests", _timekeeper->_global_time_request);

#ifdef EXPLICIT_SYNC_ONLY
        recordScalar("EXPLICIT_SYNC_ONLY", true);
#else
        recordScalar("EXPLICIT_SYNC_ONLY", false);
#endif
    }

    recordScalar("Number of allocated buffers", _allocated_buffers);

    TimerService::recordTimerDrift();

    recordScalar("Number of timer adjustments", _timekeeper->_n_adjusts);
    recordScalar("Total timer error (timestamp)", _timekeeper->_total_error);
    recordScalar("Total timer error (us)", _nic->count2us(_timekeeper->_total_error));
    recordScalar("Max timer error (timestamp)", _timekeeper->_max_error);
    recordScalar("Max timer error (us)", _nic->count2us(_timekeeper->_max_error));

    if(_timekeeper->_n_adjusts) {
        recordScalar("Average timer error (timestamp)", (double)(_timekeeper->_total_error) / (double)(_timekeeper->_n_adjusts));
        recordScalar("Average timer error (us)", (double)(_nic->count2us(_timekeeper->_total_error)) / (double)(_timekeeper->_n_adjusts));
    }
}

void TSTP::fromApplicationLayer(cPacket * pkt, const char * destination)
{
    assert(_nic);
    ApplicationPacket * appPkt = dynamic_cast<ApplicationPacket *>(pkt);
    if(appPkt == NULL)
        return;

    strcpy(_app_id, appPkt->getApplicationID());

    Buffer * buf = alloc(sizeof(Response));
    Response * resp = new (buf->frame()->data<Response>()) Response(this, 0, 0);
    resp->time(now());
    resp->expiry(now() + _expiry);
    resp->origin(here());

    Value_Timestamp * vt = resp->data<Value_Timestamp>();
    vt->value = appPkt->getData();
    vt->timestamp = simTime();
//    id++;
//    id = id & Microframe::ID_MAX;
//    *(resp->data<uint32>()) = id;
    buf->size(sizeof(Response) - sizeof(Response::Data) + sizeof(Value_Timestamp));
    marshal(buf);

    send(buf);

    cancelAndDelete(pkt);
}

bool TSTP::notify(void * subject, Buffer * buf)
{
    static unsigned int seq_num = 0;
    trace() << "TSTP::notify(s=" << subject << ",b=" << buf << ")";
    if((subject == 0) || (!_observed.notify(int64(subject), buf))) {
        if(_sink) {
            Header * h = buf->frame()->data<Header>();
            bool found = false;
            for(auto it = _last_message.begin(); it != _last_message.end(); ++it) {
                if((*it).first == h->origin()) {
                    found = true;
                    if ((*it).second < h->time()) {
                        (*it).second = h->time();
                        break;
                    }
                    else
                        return false;
                }
            }
            if(!found)
                _last_message.push_back(std::make_pair(h->origin(), h->time()));

            Value_Timestamp * vt = buf->frame()->data<Response>()->data<Value_Timestamp>();

            ApplicationPacket * newPacket = new ApplicationPacket(
                "TSTP data hook", APPLICATION_PACKET);
            newPacket->setApplicationID(_app_id);

            newPacket->setData(vt->value);
            newPacket->setSequenceNumber(seq_num++);
            newPacket->setByteLength(sizeof(Value_Timestamp::Value));
            TSTPPacket * tp = new TSTPPacket("TSTP data hook",
                                             NETWORK_LAYER_PACKET);

            AppNetInfoExchange_type info = newPacket->getAppNetInfoExchange();
            info.timestamp = vt->timestamp;
            newPacket->setAppNetInfoExchange(info);
            tp->encapsulate(newPacket);
            _nic->mac2net(tp); // Castalia thinks we are still in the MAC layer
        }
    }

    return true;
}


// TSTP
// Class attributes


// Methods

void TSTP::reset_lifekeeper() {
    Enter_Method("reset_lifekeeper()");
    if(_life_keeper->PERIOD) {
        cancelTimer(LIFEKEEPER_TIMER);
        setTimer(LIFEKEEPER_TIMER, simtime_t(_life_keeper->PERIOD, SIMTIME_US));
    }
}

void TSTP::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    trace() << "TSTP::update(obs=" << obs << ",buf=" << buf << ")";

    if(buf->is_microframe || !buf->destined_to_me || !buf->trusted)
        return;

    Packet * packet = buf->frame()->data<Packet>();

    if(packet->time() > now())
        return;

    switch(packet->type()) {
    case INTEREST: {
        Interest * interest = reinterpret_cast<Interest *>(packet);
        trace() << "TSTP::update:interest=" << interest << " => " << *interest;
        // Check for local capability to respond and notify interested observers
        Responsives::List * list = _responsives[interest->unit()]; // TODO: What if sensor can answer multiple formats (e.g. int and float)
        if(list)
            for(Responsives::Element * el = list->head(); el; el = el->next()) {
                Responsive * responsive = el->object();
                if((now() < interest->region().t1) && interest->region().contains(responsive->origin(), interest->region().t1))
                    notify(responsive, buf);
            }
    } break;
    case RESPONSE: {
        Response * response = reinterpret_cast<Response *>(packet);
        trace() << "TSTP::update:response=" << response << " => " << *response;
        trace() << "response->time() = " << response->time();
        trace() << "now() = " << now();
        if(response->time() < now()) {
            // Check region inclusion and notify interested observers
            Interests::List * list = _interested[response->unit()];
            if(list && !list->empty()) {
                for(Interests::Element * el = list->head(); el; el = el->next()) {
                    Interested * interested = el->object();
                    if(interested->region().contains(response->origin(), response->time()))
                        notify(interested, buf);
                }
            } else if(_sink)
                notify(0, buf);
        }
    } break;
    case COMMAND: {
        Command * command = reinterpret_cast<Command *>(packet);
        trace() << "TSTP::update:command=" << command << " => " << *command;
        // Check for local capability to respond and notify interested observers
        Responsives::List * list = _responsives[command->unit()]; // TODO: What if sensor can answer multiple formats (e.g. int and float)
        if(list)
            for(Responsives::Element * el = list->head(); el; el = el->next()) {
                Responsive * responsive = el->object();
                if(command->region().contains(responsive->origin(), now()))
                    notify(responsive, buf);
            }
    } break;
    case CONTROL: {
        switch(buf->frame()->data<Control>()->subtype()) {
            case DH_REQUEST:
                trace() << "TSTP::update: DH_Request: " << *buf->frame()->data<DH_Request>();
                break;
            case DH_RESPONSE:
                trace() << "TSTP::update: DH_Response: " << *buf->frame()->data<DH_Response>();
                break;
            case AUTH_REQUEST:
                trace() << "TSTP::update: Auth_Request: " << *buf->frame()->data<Auth_Request>();
                break;
            case AUTH_GRANTED:
                trace() << "TSTP::update: Auth_Granted: " << *buf->frame()->data<Auth_Granted>();
                break;
            case REPORT: {
                trace() << "TSTP::update: Report: " << *buf->frame()->data<Report>();
                Report * report = reinterpret_cast<Report *>(packet);
                if(report->time() < now()) {
                    // Check region inclusion and advertise interested observers
                    Interests::List * list = _interested[report->unit()];
                    if(list)
                        for(Interests::Element * el = list->head(); el; el = el->next()) {
                            Interested * interested = el->object();
                            if(interested->region().contains(report->origin(), report->time()))
                                interested->advertise();
                        }
                    if(report->epoch_request() && (here() == sink())) {
                        trace() << "TSTP::update: responding to Epoch request";
                        Buffer * buf = alloc(sizeof(Epoch));
                        //Epoch * epoch = new (buf->frame()->data<Epoch>()) Epoch(Region(report->origin(), 0, now(), now() + Life_Keeper::PERIOD)); // TODO
                        marshal(buf);
                        //trace() << "TSTP::update:epoch = " << epoch << " => " << (*epoch);
                        send(buf);
                    }
                }
            } break;
            case KEEP_ALIVE:
                trace() << "TSTP::update: Keep_Alive: " << *buf->frame()->data<Keep_Alive>();
                break;
            case EPOCH: {
                trace() << "TSTP::update: Epoch: " << *buf->frame()->data<Epoch>();
                Epoch * epoch = reinterpret_cast<Epoch *>(packet);
                if(here() != sink()) {
                    _global_coordinates = epoch->coordinates();
                    _epoch = epoch->epoch();
                    trace() << "TSTP::update: Epoch: adjusted epoch Space-Time to: " << _global_coordinates << ", " << _epoch;
                }
            } break;
            default:
                trace() << "TSTP::update: Unrecognized Control subtype: " << buf->frame()->data<Control>()->subtype();
                break;
        }
    } break;
    default:
        trace() << "TSTP::update: Unrecognized packet type: " << packet->type();
        break;
    }
}
