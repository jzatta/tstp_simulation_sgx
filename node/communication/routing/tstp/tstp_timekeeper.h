
class Timekeeper: private IEEE802_15_4::Observer {
    friend class TSTP;
    typedef Radio::Timer::Time_Stamp Time_Stamp;
    typedef Radio::Timer::Offset Offset;

    unsigned int SYNC_PERIOD;

public:
    Timekeeper(TSTP * tstp) : _tstp(tstp), _disable_explicit_sync(false), _total_error(0), _max_error(0), _n_adjusts(0), _first_pass(true), _adjusts(0), _bootstrapped(false), _time_request(0), _last_sync(0) {
        _tstp->trace() << "TSTP::Timekeeper()";
        _next_sync = 0;
        _global_time_request = 0;
        max_sync_diff = 10000000;
    }
    virtual ~Timekeeper();

    Time now() {
        return Radio::Timer::count2us(_tstp->_nic->read());
    }

    bool sync_required() {
        return (!_disable_time_request) && (now() >= _next_sync);
    }

    bool sync_recommended() {
        return now() >= (_next_sync - SYNC_PERIOD / 2);
    }

    void bootstrap();
    bool bootstrapped() { return _bootstrapped; }

    void marshal(Buffer * buf);

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

private:
    Time_Stamp _next_sync;
    Coordinates _peer;
    TSTP * _tstp;
    bool _disable_explicit_sync;
    Time_Stamp _total_error;
    Offset _max_error;
    unsigned int _n_adjusts;
    bool _first_pass;
    cOutVector * _adjusts;
    bool _bootstrapped;
    unsigned int _time_request;
    static unsigned int _global_time_request;
    bool _always_relevant;
    bool _disable_time_request;
    simtime_t _last_sync;
    static simtime_t sync_diff_sum;
    static simtime_t max_sync_diff;
};
