

class Security: private IEEE802_15_4::Observer
{
    friend class TSTP;

    unsigned int KEY_MANAGER_PERIOD;
    unsigned long long KEY_EXPIRY;
    unsigned int DHREQUEST_AUTH_DELAY;
    unsigned int POLY_TIME_WINDOW;

public:
    typedef Diffie_Hellman::Shared_Key Master_Secret;

public:

//     virtual void add_peer(const unsigned char * peer_id, unsigned int id_len, const Region & valid_region) = 0;
    virtual void bootstrap() = 0;
    virtual bool bootstrapped() = 0;
    virtual bool synchronized() = 0;
    virtual void marshal(Buffer * buf) = 0;
    virtual void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf) = 0;
private:
    virtual int key_manager() = 0;
    virtual void auth_request() = 0;
};

#include "tstp_security_epos.h"
#include "tstp_security_stub.h"
#include "tstp_security_full.h"
