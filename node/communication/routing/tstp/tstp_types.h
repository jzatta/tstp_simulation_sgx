
// Interest Message
class Interest: public Header
{
public:
    Interest(TSTP * tstp, const Region & region, const Unit & unit, const Mode & mode, const Error & precision, const Microsecond & expiry, const Microsecond & period = 0)
//     : Header(INTEREST, 0, 0, tstp->now(), tstp->here(), tstp->here()), _region(region), _unit(unit), _mode(mode), _precision(0), _expiry(expiry), _period(period) {}
    : Header(INTEREST, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _region(region), _unit(unit), _mode(mode), _precision(0), _expiry(expiry), _period(period) {}

    const Unit & unit() const { return _unit; }
    const Region & region() const { return _region; }
    Microsecond period() const { return _period; }
    Time_Offset expiry() const { return _expiry; }
    Mode mode() const { return static_cast<Mode>(_mode); }
    Error precision() const { return static_cast<Error>(_precision); }

    bool time_triggered() { return _period; }
    bool event_driven() { return !time_triggered(); }

    friend OStream & operator<<(OStream & db, const Interest & m) {
        db << reinterpret_cast<const Header &>(m) << ",u=" << m._unit << ",m=" << ((m._mode == ALL) ? 'A' : 'S') << ",e=" << int(m._precision) << ",x=" << m._expiry << ",re=" << m._region << ",p=" << m._period;
        return db;
    }

protected:
    Region _region;
    Unit _unit;
    unsigned char _mode : 2;
    unsigned char _precision : 6;
    Time_Offset _expiry;
    Microsecond _period; // TODO: should be Time_Offset
    CRC _crc;
} __attribute__((packed));

// Response (Data) Message
class Response: public Header
{
    friend class TSTP;
public:
    typedef unsigned char Data[MTU - sizeof(Unit) - sizeof(Error) - sizeof(Time_Offset) - sizeof(Message_Auth) - sizeof(CRC)];

public:
    Response(TSTP * tstp, const Unit & unit, const Error & error = 0, const Time & expiry = 0)
    : Header(RESPONSE, 0, 0, tstp->now(), tstp->here(), tstp->here()), _unit(unit), _error(error), _expiry(expiry), _crc(0) {
//     : Header(RESPONSE, 0, 0, tstp->now(), tstp->here(), tstp->here()), _unit(unit), _error(error), _expiry(expiry), _auth(0), _crc(0) {
        memset(_data, 0, sizeof(Data));
    }

    const Unit & unit() const { return _unit; }
    Error error() const { return _error; }
    Time expiry() const { return _time + _expiry; }
    void expiry(const Time & e) { _expiry = e - _time; }

    template<typename T>
    void value(const T & v) { *reinterpret_cast<Value<Unit::GET<T>::NUM> *>(&_data) = v; }

    template<typename T>
    T value() { return *reinterpret_cast<Value<Unit::GET<T>::NUM> *>(&_data); }

    template<typename T>
    T * data() { return reinterpret_cast<T *>(&_data); }

    friend std::ostream & operator<<(std::ostream & db, const Response & m) {
        db << reinterpret_cast<const Header &>(m) << ",u=" << m._unit << ",e=" << int(m._error) << ",x=" << m._expiry << ",d=" << std::hex << *const_cast<Response &>(m).data<unsigned>() << std::dec;
        return db;
    }

protected:
    Unit _unit;
    Error _error;
    Time_Offset _expiry;
    Data _data;
    Message_Auth _auth; // Data size is variable, so _auth is not directly accessible
    CRC _crc;
} __attribute__((packed));

// Command Message
class Command: public Header
{
private:
    typedef unsigned char Data[MTU - sizeof(Region) - sizeof(Unit) - sizeof(CRC)];

public:
    Command(TSTP * tstp, const Unit & unit, const Region & region)
//     : Header(COMMAND, 0, 0, tstp->now(), tstp->here(), tstp->here()), _region(region), _unit(unit) {}
    : Header(COMMAND, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _region(region), _unit(unit) {}

    const Region & region() const { return _region; }
    const Unit & unit() const { return _unit; }

    template<typename T>
    T * command() { return reinterpret_cast<T *>(&_data); }

    template<typename T>
    T * data() { return reinterpret_cast<T *>(&_data); }

    friend OStream & operator<<(OStream & db, const Command & m) {
        db << reinterpret_cast<const Header &>(m) << ",u=" << m._unit << ",reg=" << m._region;
        return db;
    }

protected:
    Region _region;
    Unit _unit;
    Data _data;
    CRC _crc;
} __attribute__((packed));

// Diffie-Hellman Request Security Bootstrap Control Message
class DH_Request: public Control
{
public:
    DH_Request(TSTP * tstp, const Region::Space & dst, const Public_Key & k)
//     : Control(DH_REQUEST, 0, 0, tstp->now(), tstp->here(), tstp->here()), _destination(dst), _public_key(k) { }
    : Control(DH_REQUEST, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _destination(dst), _public_key(k) { }

    const Region::Space & destination() { return _destination; }
    void destination(const Region::Space & d) { _destination = d; }

    Public_Key key() { return _public_key; }
    void key(const Public_Key & k) { _public_key = k; }

    friend OStream & operator<<(OStream & db, const DH_Request & m) {
        db << reinterpret_cast<const Control &>(m) << ",d=" << m._destination << ",k=" << m._public_key;
        return db;
    }

private:
    Region::Space _destination;
    Public_Key _public_key;
    CRC _crc;
//} __attribute__((packed)); // TODO
};

// Diffie-Hellman Response Security Bootstrap Control Message
class DH_Response: public Control
{
public:
    DH_Response(TSTP * tstp, const Public_Key & k)
//     : Control(DH_RESPONSE, 0, 0, tstp->now(), tstp->here(), tstp->here()), _public_key(k) { }
    : Control(DH_RESPONSE, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _public_key(k) { }

    Public_Key key() { return _public_key; }
    void key(const Public_Key & k) { _public_key = k; }

    friend OStream & operator<<(OStream & db, const DH_Response & m) {
        db << reinterpret_cast<const Control &>(m) << ",k=" << m._public_key;
        return db;
    }

private:
    Public_Key _public_key;
    CRC _crc;
//} __attribute__((packed)); // TODO
};

// Authentication Request Security Bootstrap Control Message
class Auth_Request: public Control
{
public:
    Auth_Request(TSTP * tstp, const Node_Auth & a, const OTP & o)
    : Control(AUTH_REQUEST, 0, 0, tstp->now(), tstp->here(), tstp->here()), _auth(a), _otp(o) { }
//     : Control(AUTH_REQUEST, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _auth(a), _otp(o) { }


    const Node_Auth & auth() const { return _auth; }
    void auth(const Node_Auth & a) { _auth = a; }

    const OTP & otp() const { return _otp; }
    void otp(const OTP & o) { _otp = o; }

    friend OStream & operator<<(OStream & db, const Auth_Request & m) {
        db << reinterpret_cast<const Control &>(m) << ",a=" << m._auth << ",o=" << m._otp;
        return db;
    }

private:
    Node_Auth _auth;
    OTP _otp;
    CRC _crc;
// } __attribute__((packed));
};

// Authentication Granted Security Bootstrap Control Message
class Auth_Granted: public Control
{
public:
    Auth_Granted(TSTP * tstp, const Region::Space & dst, const Node_Auth & a)
    : Control(AUTH_GRANTED, 0, 0, tstp->now(), tstp->here(), tstp->here()), _destination(dst), _auth(a) { }
//     : Control(AUTH_GRANTED, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _destination(dst), _auth(a) { }

    const Region::Space & destination() { return _destination; }
    void destination(const Coordinates  & d) { _destination = d; }

    const Node_Auth & auth() const { return _auth; }
    void auth(const Node_Auth & a) { _auth = a; }

    friend OStream & operator<<(OStream & db, const Auth_Granted & m) {
        db << reinterpret_cast<const Control &>(m) << ",d=" << m._destination << ",a=" << m._auth;
        return db;
    }

private:
    Region::Space _destination;
    Node_Auth _auth;
    CRC _crc;
// } __attribute__((packed)); // TODO
};

// Report Control Message
class Report: public Control
{
public:
    Report(TSTP * tstp, const Unit & unit, const Error & error = 0, bool epoch_request = false)
    : Control(REPORT, 0, 0, tstp->now(), tstp->here(), tstp->here()), _unit(unit), _error(error), _epoch_request(epoch_request) { }
//     : Control(REPORT, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _unit(unit), _error(error), _epoch_request(epoch_request) { }

    const Unit & unit() const { return _unit; }
    Error error() const { return _error; }
    bool epoch_request() const { return _epoch_request; }

    friend OStream & operator<<(OStream & db, const Report & r) {
        db << reinterpret_cast<const Control &>(r) << ",u=" << r._unit << ",e=" << r._error;
        return db;
    }

private:
    Unit _unit;
    Error _error;
    bool _epoch_request;
    CRC _crc;
} __attribute__((packed));

// Epoch Control Message
class Epoch: public Control
{
public:
//     Epoch(const Region & dst, const Time & ep, const Global_Coordinates & coordinates)
    Epoch(TSTP * tstp, const Region & dst, const Time & ep, const Global_Coordinates & coordinates)
    : Control(EPOCH, 0, 0, tstp->now(), tstp->here(), tstp->here()), _destination(dst), _epoch(ep), _coordinates(coordinates) { }
//     : Control(EPOCH, 0, 0, 0, Coordinates(0,0,0), Coordinates(0,0,0)), _destination(dst), _epoch(ep), _coordinates(coordinates) { }

    const Region & destination() const { return _destination; }
    const Time epoch() const { return _epoch; }
    const Global_Coordinates & coordinates() const { return _coordinates; }

    friend OStream & operator<<(OStream & db, const Epoch & e) {
        db << reinterpret_cast<const Control &>(e) << ",e=" << e._epoch << ",c=" << e._coordinates;
        return db;
    }

private:
    Region _destination;
    Time _epoch;
    Global_Coordinates _coordinates;
    CRC _crc;
} __attribute__((packed));

// TSTP Smart Data bindings
// Interested (binder between Interest messages and Smart Data)
class Interested: public Interest
{
public:
    template<typename T>
    Interested(TSTP * tstp, T * data, const Region & region, const Unit & unit, const Mode & mode, const Precision & precision, const Microsecond & expiry, const Microsecond & period = 0)
    : Interest(tstp, region, unit, mode, precision, expiry, period), _tstp(tstp), _link(this, T::UNIT) {
        _tstp->trace() << "TSTP::Interested(d=" << data << ",r=" << region << ",p=" << period << ") => " << reinterpret_cast<const Interest &>(*this);
        _tstp->_interested.insert(&_link);
        advertise();
    }
    ~Interested() {
        _tstp->trace() << "TSTP::~Interested(this=" << this << ")";
        _tstp->_interested.remove(&_link);
        revoke();
    }

    void advertise() { send(); }
    void revoke() { _mode = REVOKE; send(); }

    template<typename Value>
    void command(const Value & v) {
        _tstp->trace() << "TSTP::Interested::command(v=" << v << ")";
        Buffer * buf = _tstp->alloc(sizeof(Command));
        Command * command = new (buf->frame()->data<Command>()) Command(_tstp, unit(), region());
        memcpy(command->command<Value>(), &v, sizeof(Value));
        _tstp->marshal(buf);
        _tstp->trace() << "TSTP::Interested::command:command=" << command << " => " << (*command);
        _tstp->send(buf);
    }

private:
    void send(void) {
        _tstp->trace() << "TSTP::Interested::send() => " << reinterpret_cast<const Interest &>(*this);
        Buffer * buf = _tstp->alloc(sizeof(Interest));
        memcpy(buf->frame()->data<Interest>(), this, sizeof(Interest));
        _tstp->marshal(buf);
        _tstp->send(buf);
    }

private:
    TSTP * _tstp;
    Interests::Element _link;
};

// Responsive (binder between Smart Data (Sensors) and Response messages)
class Responsive: public Response
{
public:
    template<typename T>
    Responsive(TSTP * tstp, T * data, const Unit & unit, const Error & error, const Time & expiry, bool epoch_request = false)
    : Response(tstp, unit, error, expiry), _tstp(tstp), _size(sizeof(Response) - sizeof(Response::Data) + sizeof(typename T::Value)), _t0(0), _t1(0), _link(this, T::UNIT) {
        _tstp->trace() << "TSTP::Responsive(d=" << data << ",s=" << _size << ") => " << this;
        _tstp->trace() << "TSTP::Responsive() => " << reinterpret_cast<const Response &>(*this);
        _tstp->_responsives.insert(&_link);
        advertise(epoch_request);
    }
    ~Responsive() {
        _tstp->trace() << "TSTP::~Responsive(this=" << this << ")";
        _tstp->_responsives.remove(&_link);
    }

    using Header::time;
    using Header::origin;

    void t0(const Time & t) { _t0 = t; }
    void t1(const Time & t) { _t1 = t; }
    Time t0() const { return _t0; }
    Time t1() const { return _t1; }

    void respond(const Time & expiry) { send(expiry); }
    void advertise(bool epoch_request = false) {
        _tstp->trace() << "TSTP::Responsive::advertise()";
        Buffer * buf = _tstp->alloc(sizeof(Report));
//         Report * report = new (buf->frame()->data<Report>()) Report(unit(), error(), epoch_request);
        new (buf->frame()->data<Report>()) Report(_tstp, unit(), error(), epoch_request);
        _tstp->marshal(buf);
//         _tstp->trace() << "TSTP::Responsive::advertise:report=" << report << " => " << (*report);
        _tstp->send(buf);
    }

private:
    void send(const Time & expiry) {
        if((_time >= _t0) && (_time <= _t1)) {
            assert(expiry > _tstp->now());
            _tstp->trace() << "TSTP::Responsive::send(x=" << expiry << ")";
            Buffer * buf = _tstp->alloc(_size);
            Response * response = buf->frame()->data<Response>();
            memcpy(response, this, _size);
            response->expiry(expiry);
            _tstp->marshal(buf);
            _tstp->trace() << "TSTP::Responsive::send:response=" << response << " => " << (*response);
            _tstp->send(buf);
        }
    }

private:
    TSTP * _tstp;
    unsigned int _size;
    Time _t0;
    Time _t1;
    Responsives::Element _link;
};
