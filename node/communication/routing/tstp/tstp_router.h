
class Router: private IEEE802_15_4::Observer
{
    friend class TSTP;
public:
    bool effort_metric;
    bool expiry_metric;
    bool random_metric;
    bool distance_metric;
    bool zero_metric;
    double EXPIRY_BETHA;
    static bool track_offsets;

private:
    static const unsigned int CCA_TX_GAP = IEEE802_15_4::CCA_TX_GAP;
    static unsigned int RADIO_RANGE;

public:
    Router(TSTP * tstp) : _tstp(tstp) {
        track_offsets = false;
        _tstp->trace() << "TSTP::Router()";
    }
    virtual ~Router();

    void bootstrap();

    bool synchronized() { return true; }

    void marshal(Buffer * buf);

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

private:
    bool forward(Buffer * buf) {
        if(!_forwarder) {
            _tstp->trace() << "not a forwarder";
            return false;
        }

        if(!buf->destined_to_me && (buf->my_distance >= buf->sender_distance)) {
            _tstp->trace() << "not forwarding message from node closer to destination";
            return false;
        }

        // Do not forward messages that come from too far away, to avoid radio range asymmetry
        Coordinates::Distance d = _tstp->here() - buf->frame()->data<Header>()->last_hop();
        if(d > RADIO_RANGE) {
            _tstp->trace() << "not forwarding message that comes from too far away";
            return false;
        }

        TSTP::Time expiry = buf->deadline;
        const TSTP::Time infinite = -1;

        if(expiry == infinite) {
            buf->progress_bits = 1;
            return true;
        } else if (expiry <= _tstp->now()) {
            if(drop_expired)
                return false;

            buf->progress_bits = 1;
            return true;
        }

        if(expiry < _tstp->now()) {
            _tstp->trace() << "not forwarding expired message";
            return false;
        }

        TSTP::Time best_case_delivery_time = (buf->my_distance + RADIO_RANGE - 1) / RADIO_RANGE * _tstp->_nic->period();
        TSTP::Time relative_expiry = expiry - _tstp->now();

        if(best_case_delivery_time > relative_expiry) {
            _tstp->trace() << "not forwarding message that will expire soon";
            return false;
        }

        buf->deadline -= best_case_delivery_time;

        buf->progress_bits = 1;
        return true;
    }

    void offset(Buffer * buf) {
        if(zero_metric || (buf->my_distance == 0)) {
            buf->offset = 0;
            return;
        }

        if(random_metric)
            buf->offset = Random::random() % buf->offset;

        if(distance_metric && !buf->is_new) {
            // forward() guarantees that my_distance < sender_distance
            buf->offset *= RADIO_RANGE + buf->my_distance - buf->sender_distance;
            buf->offset /= RADIO_RANGE;
        }

        if(expiry_metric) {
            TSTP::Time expiry = buf->deadline;
            const TSTP::Time infinite = -1;
            if(expiry != infinite) {
                Time now = _tstp->now();
                if(expiry > now) {
                    buf->offset *= expiry - now;
                    buf->offset /= expiry;
                } else
                    buf->offset = 0;
            }
        }

        if(effort_metric) {
            if(_tstp->_nic->statistics().tx_packets && _tstp->_nic->statistics().tx_relayed) {
                buf->offset *= _tstp->_nic->statistics().tx_relayed;
                buf->offset /= _tstp->_nic->statistics().tx_packets;
            }
        }

        if(track_offsets)
            _offsets.push_back(buf->offset);
    }

    TSTP * _tstp;
    bool _forwarder;
    static vector<unsigned long long> _offsets;
};
