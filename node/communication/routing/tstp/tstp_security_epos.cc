
#include "tstp.h"

void TSTP::Security_EPOS::bootstrap()
{
    _tstp->trace() << "TSTP::Security_EPOS::bootstrap()";

    assert(_tstp->_sinks.size() == 1); // Security_EPOS only works in single-sink mode for now

    KEY_EXPIRY = _tstp->par("keyExpiry").doubleValue();
    DHREQUEST_AUTH_DELAY = _tstp->par("dhRequestProcessingDelay").doubleValue();
    KEY_MANAGER_PERIOD = _tstp->par("keyManagerPeriod").doubleValue();
    POLY_TIME_WINDOW = _tstp->par("securityTimeWindow").doubleValue();

    _tstp->_nic->attach(this, NIC::TSTP);
    
    Peer * peer = new Peer(_id, Region(_tstp->sink(), 0, 0, -1), _tstp);
    _pending_peers.insert(peer->link());

    if(bootstrapped())
        _tstp->bootstrap();
}

// TSTP::Security_EPOS
// Class attributes

// Methods
void TSTP::Security_EPOS::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    bool was_bootstrapped = bootstrapped();

    _tstp->trace() << "TSTP::Security_EPOS::update(obs=" << obs << ",buf=" << buf << ")";

    if(!buf->is_microframe && buf->destined_to_me) {

        switch(buf->frame()->data<Header>()->type()) {

            case CONTROL: {
                _tstp->trace() << "TSTP::Security_EPOS::update(): Control message received";
                switch(buf->frame()->data<Control>()->subtype()) {
                    case REPORT: {
                        buf->trusted = true;
                    } break;

                    case DH_REQUEST: {
                        DH_Request * dh_req = buf->frame()->data<DH_Request>();
                        _tstp->trace() << "TSTP::Security_EPOS::update(): DH_Request message received: " << *dh_req;

                        //while(CPU::tsl(_peers_lock));
                        //CPU::int_disable();
                        bool valid_peer = false;
                        for(Peers::Element * el = _pending_peers.head(); el; el = el->next())
                            if(el->object()->valid_deploy(dh_req->origin(), _tstp->now())) {
                                valid_peer = true;
                                _tstp->trace() << "Found pending peer";
                                break;
                            }
                        if(!valid_peer)
                            for(Peers::Element * el = _trusted_peers.head(); el; el = el->next())
                                if(el->object()->valid_deploy(dh_req->origin(), _tstp->now())) {
                                    valid_peer = true;
                                    _trusted_peers.remove(el);
                                    _pending_peers.insert(el);
                                    _tstp->trace() << "Moving trusted peer to pending";
                                    break;
                                }
                        //_peers_lock = false;
                        //CPU::int_enable();

                        if(valid_peer) {
                            _tstp->trace() << "TSTP::Security_EPOS::update(): Sending DH_Response";
                            // Respond to Diffie-Hellman request
                            Buffer * resp = _tstp->alloc(sizeof(DH_Response));
                            new (resp->frame()) DH_Response(_tstp, _dh.public_key());
                            _tstp->marshal(resp);
                            _tstp->send(resp);

                            // Calculate Master Secret
                            Pending_Key * pk = new Pending_Key(buf->frame()->data<DH_Request>()->key(), _tstp);
                            pk->master_secret();
                            //Master_Secret ms = pk->master_secret();
                            //while(CPU::tsl(_peers_lock));
                            //CPU::int_disable();
                            _pending_keys.insert(pk->link());
                            //_peers_lock = false;
                            //CPU::int_enable();

                            _tstp->schedule_auth_request(); // Castalia workaround
                        }
                    } break;

                    case DH_RESPONSE: {
                        if(_dh_requests_open) {
                            DH_Response * dh_resp = buf->frame()->data<DH_Response>();
                            _tstp->trace() << "TSTP::Security_EPOS::update(): DH_Response message received: " << *dh_resp;

                            //CPU::int_disable();
                            bool valid_peer = false;
                            for(Peers::Element * el = _pending_peers.head(); el; el = el->next())
                                if(el->object()->valid_deploy(dh_resp->origin(), _tstp->now())) {
                                    valid_peer = true;
                                    _tstp->trace() << "Valid peer found: " << *el->object();
                                    break;
                                }

                            if(valid_peer) {
                                _dh_requests_open--;
                                Pending_Key * pk = new Pending_Key(buf->frame()->data<DH_Response>()->key(), _tstp);
                                _pending_keys.insert(pk->link());
                                _tstp->trace() << "TSTP::Security_EPOS::update(): Inserting new Pending Key: " << *pk;
                            }
                            //CPU::int_enable();
                        }
                    } break;

                    case AUTH_REQUEST: {

                        Auth_Request * auth_req = buf->frame()->data<Auth_Request>();
                        _tstp->trace() << "TSTP::Security_EPOS::update(): Auth_Request message received: " << *auth_req;

                        //CPU::int_disable();
                        Peer * auth_peer = 0;
                        for(Peers::Element * el = _pending_peers.head(); el; el = el->next()) {
                            Peer * peer = el->object();

                            if(peer->valid_request(auth_req->auth(), auth_req->origin(), _tstp->now())) {
                                _tstp->trace() << "valid_request ";
                                for(Pending_Keys::Element * pk_el = _pending_keys.head(); pk_el; pk_el = pk_el->next()) {
                                    Pending_Key * pk = pk_el->object();
                                    if(verify_auth_request(pk->master_secret(), peer->id(), auth_req->otp())) {
                                        peer->master_secret(pk->master_secret());
                                        _pending_peers.remove(el);
                                        _trusted_peers.insert(el);
                                        auth_peer = peer;

                                        _pending_keys.remove(pk_el);
                                        delete pk_el->object();

                                        break;
                                    }
                                }
                                if(auth_peer)
                                    break;
                            }
                        }
                        //CPU::int_enable();

                        if(auth_peer) {
                            Node_Auth encrypted_auth;
                            encrypt(auth_peer->auth(), auth_peer, encrypted_auth);

                            Buffer * resp = _tstp->alloc(sizeof(Auth_Granted));
                            new (resp->frame()) Auth_Granted(_tstp, Region::Space(auth_peer->valid().center, auth_peer->valid().radius), encrypted_auth);
                            _tstp->marshal(resp);
                            _tstp->trace() << "TSTP::Security_EPOS: Sending Auth_Granted message " << resp->frame()->data<Auth_Granted>();
                            _tstp->send(resp);
                        } else
                            _tstp->trace() << "TSTP::Security_EPOS::update(): No peer found";
                    } break;

                    case AUTH_GRANTED: {
                        Auth_Granted * auth_grant = buf->frame()->data<Auth_Granted>();
                        _tstp->trace() << "TSTP::Security_EPOS::update(): Auth_Granted message received: " << *auth_grant;
                        //CPU::int_disable();
                        bool auth_peer = false;
                        for(Peers::Element * el = _pending_peers.head(); el; el = el->next()) {
                            Peer * peer = el->object();
                            for(Pending_Keys::Element * pk_el = _pending_keys.head(); pk_el; pk_el = pk_el->next()) {
                                Pending_Key * pk = pk_el->object();
                                Node_Auth decrypted_auth;
                                OTP key = otp(pk->master_secret(), peer->id());
                                _cipher.decrypt(auth_grant->auth(), key, decrypted_auth);
                                if(decrypted_auth == _auth) {
                                    peer->master_secret(pk->master_secret());
                                    _pending_peers.remove(el);
                                    _trusted_peers.insert(el);
                                    auth_peer = true;
                                    _pending_keys.remove(pk_el);
                                    delete pk_el->object();

                                    break;
                                }
                            }
                            if(auth_peer)
                                break;
                        }
                        if(auth_peer) {
                            _tstp->trace() << "Auth Granted!";
                            _bootstrapped = true;
                        }
                        else
                            _tstp->trace() << "No peer found for Auth code";
                        //CPU::int_enable();
                    } break;

                    default: break;
                }
            } break;
            case RESPONSE: {
                _tstp->trace() << "TSTP::Security_EPOS::update(): Response message received";
                _tstp->trace() << "from " << buf->frame()->data<Header>()->origin();
                Response * resp = buf->frame()->data<Response>();
                Time reception_time = _tstp->_nic->count2us(buf->sfd_time_stamp);
                for(Peers::Element * el = _trusted_peers.head(); el; el = el->next()) {
                    if(el->object()->valid_deploy(buf->frame()->data<Header>()->origin(), _tstp->now())) {
                        _tstp->trace() << "Found valid peer";
                        unsigned char * data = resp->data<unsigned char>();
                        if(unpack(el->object(), data, &data[sizeof(Master_Secret)], reception_time)) {
                            buf->trusted = true;
                            _tstp->trace() << "Unpack done!";
                            break;
                        } else {
                            _tstp->trace() << "Unpack failed";
                        }
                    }
                }
            } break;
            case INTEREST: {
                buf->trusted = true;
            } break;
            default: break;
        }
    } else
        buf->trusted = true;

    if((!was_bootstrapped) && bootstrapped())
        _tstp->bootstrap();
}

void TSTP::Security_EPOS::marshal(Buffer * buf)
{
    _tstp->trace() << "TSTP::Security_EPOS::marshal(buf=" << buf << ")";
    buf->trusted = false;

    if(buf->frame()->data<Header>()->type() == TSTP::RESPONSE) {
        _tstp->trace() << "TSTP::Security_EPOS::marshal => TSTP::RESPONSE";
        Peer * peer = 0;
        for(Peers::Element * el = _trusted_peers.head(); el; el = el->next())
            if(el->object()->valid_deploy(_tstp->destination(buf).center, _tstp->now())) {
                peer = el->object();
                break;
            }
        if(!peer)
            return;

        // Pad data to the size of the key
        unsigned int data_size = buf->size() - (sizeof(Response) - sizeof(Response::Data));
        Response * response = buf->frame()->data<Response>();
        unsigned char * data = response->data<unsigned char>();
        buf->size(sizeof(Response) - sizeof(Response::Data) + sizeof(Master_Secret));
        for(unsigned int i = data_size; i < sizeof(Master_Secret); i++)
            data[i] = 0;

        pack(data, peer);
        buf->trusted = true;
    }
}

TSTP::Security_EPOS::~Security_EPOS()
{
    _tstp->trace() << "TSTP::~Security_EPOS()";
    while(auto el = _trusted_peers.remove_head())
        delete el->object();
    while(auto el = _pending_peers.remove_head())
        delete el->object();
    while(auto el = _pending_keys.remove_head())
        delete el->object();

//    _tstp->_nic->detach(this, 0);
}
