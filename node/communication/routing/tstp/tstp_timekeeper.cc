
#include "tstp.h"

void TSTP::Timekeeper::bootstrap()
{
    _tstp->trace() << "Timekeeper::bootstrap()";
    SYNC_PERIOD = _tstp->par("ptpExplicitSyncPeriod").doubleValue();
    _always_relevant = _tstp->par("timekeeperAlwaysRelevant");
    _disable_time_request = _tstp->par("disableTimeRequests");

    bool start_synchronized = _tstp->par("startSynchronized");
    if(_tstp->_sink) {
        // Just so that the sink will always have sync_required() and sync_recommended() return false
        _next_sync = -1ull;
        _bootstrapped = true;
    } else if(start_synchronized) {
        _next_sync = now() + SYNC_PERIOD;
        _bootstrapped = true;
    } else
        _next_sync = 0;

    _tstp->_nic->attach(this, NIC::TSTP);

    if(bootstrapped())
        _tstp->_router->bootstrap();
}

// TSTP::Timekeeper
// Class attributes
unsigned int TSTP::Timekeeper::_global_time_request;
simtime_t TSTP::Timekeeper::max_sync_diff;

// Methods
void TSTP::Timekeeper::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    bool was_bootstrapped = bootstrapped();

    _tstp->trace() << "TSTP::Timekeeper::update(obs=" << obs << ",buf=" << buf << ")";

    if(buf->is_microframe) {
        if(sync_recommended() || _always_relevant)
            buf->relevant = true;
    } else {
        buf->deadline = _tstp->destination(buf).t1;

        Header * header = buf->frame()->data<Header>();

        if(header->time_request()) {
            _tstp->trace() << "TSTP::Timekeeper::update: time_request received";
            // Respond to Time Request if able
            if(!sync_required()) {
                bool peer_closer_to_sink = buf->downlink ?
                    (_tstp->here() - _tstp->sink() > header->last_hop() - _tstp->sink()) :
                    (buf->my_distance > buf->sender_distance);

                if(!peer_closer_to_sink) {
                    _tstp->trace() << "TSTP::Timekeeper::update: responding to time request";
                    _tstp->keep_alive(true);
                }
            }
        } else {
            bool peer_closer_to_sink = buf->downlink ?
                (_tstp->here() - _tstp->sink() > header->last_hop() - _tstp->sink()) :
                (buf->my_distance > buf->sender_distance);

            if(peer_closer_to_sink) {
#ifdef EXPLICIT_SYNC_ONLY
                    if(header->type() == CONTROL && reinterpret_cast<const Control*>(header)->subtype() == KEEP_ALIVE) {
#endif
                Time_Stamp t0 = header->last_hop_time() + Radio::Timer::us2count(IEEE802_15_4::PHY_HEADER_SIZE * 1000000 / IEEE802_15_4::BYTE_RATE);
                Time_Stamp t1 = buf->sfd_time_stamp;

                _tstp->_nic->adjust(t0, t1);
                _tstp->reset_lifekeeper();

                simtime_t diff = simTime() - _last_sync;
                if(diff > max_sync_diff)
                    max_sync_diff = diff;
                _last_sync = simTime();
                _next_sync = now() + SYNC_PERIOD;

                _adjusts->record(_tstp->_nic->frequency_error());

                _tstp->trace() << "TSTP::Timekeeper::update: timer offset of " << t0 - t1 << " corrected.";
                _tstp->trace() << "now() = " << now();
                _bootstrapped = true;
#ifdef EXPLICIT_SYNC_ONLY
                    }
#endif
            }
        }
    }

    if((!was_bootstrapped) && bootstrapped())
        _tstp->_router->bootstrap();
}

void TSTP::Timekeeper::marshal(Buffer * buf)
{
    _tstp->trace() << "TSTP::Timekeeper::marshal(buf=" << buf << ")";
    if((buf->frame()->data<Header>()->type() == CONTROL) && (buf->frame()->data<Control>()->subtype() == KEEP_ALIVE)) {
        if(sync_required()) {
            _time_request++;
            _global_time_request++;
            buf->frame()->data<Header>()->time_request(true);
        } else
            buf->frame()->data<Header>()->time_request(sync_recommended());
    } else {
        if(sync_required()) {
            _time_request++;
            _global_time_request++;
            buf->frame()->data<Header>()->time_request(true);
        } else
            buf->frame()->data<Header>()->time_request(false);
    }

    buf->frame()->data<Header>()->time(now());
    buf->deadline = _tstp->destination(buf).t1; // deadline must be set after origin time for Security messages
}

TSTP::Timekeeper::~Timekeeper()
{
    _tstp->trace() << "TSTP::~Timekeeper()";
    if(_adjusts)
        delete _adjusts;
    //_tstp->_nic->detach(this, 0);
}
