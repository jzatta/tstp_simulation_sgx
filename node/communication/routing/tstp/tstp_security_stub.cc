
#include "tstp.h"
#include <ostream>


TSTP::Security_Stub::Security_Stub(TSTP * tstp) : _key_manager(false), _tstp(tstp), _bootstrapped(false) {
    _tstp->trace() << "TSTP::Security_Stub() => id=" << _tstp->_unit;
    // SGX::create
    _answer = NULL;
    _sgx = new Serial("/tmp/serialSIM");
    CHECK_SGX_PERIOD = 10000;
}

TSTP::Security_Stub::~Security_Stub() {
    _tstp->trace() << "TSTP::~Security_Stub()";
    // SGX::destroy
    delete _sgx;
//    _tstp->_nic->detach(this, 0);
}

void TSTP::Security_Stub::bootstrap() {
    _tstp->trace() << "TSTP::Security_Stub::bootstrap()";

    assert(_tstp->_sinks.size() == 1); // Security_Stub only works in single-sink mode for now

    KEY_EXPIRY = _tstp->par("keyExpiry").doubleValue();
    DHREQUEST_AUTH_DELAY = _tstp->par("dhRequestProcessingDelay").doubleValue();
    KEY_MANAGER_PERIOD = _tstp->par("keyManagerPeriod").doubleValue();
    POLY_TIME_WINDOW = _tstp->par("securityTimeWindow").doubleValue();

    _tstp->_nic->attach(this, NIC::TSTP);

    for(unsigned int i = 0; i < _tstp->_units; i++) {
        if(i == _tstp->_unit)
            continue;
        // Bootstrap might not have been called yet at the other nodes
        VirtualMobilityManager *nodeMobilityModule = check_and_cast<
                    VirtualMobilityManager*>(
                    _tstp->getParentModule()->getParentModule()->getParentModule()->getSubmodule(
                        "node", i)->getSubmodule("MobilityManager"));

        Coordinates::Number x = nodeMobilityModule->getLocation().x * 100.0;
        Coordinates::Number y = nodeMobilityModule->getLocation().y * 100.0;
        Coordinates::Number z = nodeMobilityModule->getLocation().z * 100.0;

        Coordinates there(x, y, z);

//         add_peer(((TSTP::Security_EPOS *)_tstp->_instances[i]->_security)->_id, sizeof(Node_ID), Region(there, 0, 0, -1));
        struct CallAddPeer *peer = (struct CallAddPeer *)malloc(sizeof(struct CallAddPeer));
        memcpy(peer->id, ((TSTP::Security_EPOS *)_tstp->_instances[i]->_security)->_id, sizeof(Node_ID));
        peer->valid_region = Region(there, 0, 0, -1);
        write(ADD_PEER, peer, sizeof(struct CallAddPeer));
    }
    _tstp->setTimer(_tstp->CHECK_SGX, simtime_t(CHECK_SGX_PERIOD, SIMTIME_US));
    _tstp->setTimer(_tstp->KEY_MANAGER_TIMER, simtime_t(KEY_MANAGER_PERIOD, SIMTIME_US));
    
    _bootstrapped = true;

    if(bootstrapped())
        _tstp->bootstrap();
}

// TSTP::Security_Stub
// Class attributes

// Methods
void TSTP::Security_Stub::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf) {
    uint8_t *trust;
    _tstp->trace() << "TSTP::Security_Stub::update(obs=" << obs << ",buf=" << buf << ")";
    write(UPDATE, buf, sizeof(Buffer));
    trust = (uint8_t *)getAnswer();
    _tstp->trace() << "TSTP::Security_Stub::update(tr=" << buf->trusted << ")";
    buf->trusted = (*trust > 0)? true : false;
    _tstp->trace() << "TSTP::Security_Stub::update(tr=" << buf->trusted << ")";
    free(trust);
    return;
}

void TSTP::Security_Stub::marshal(Buffer * buf) {
    _tstp->trace() << "TSTP::Security_Stub::marshal(buf=" << buf << ") => Security does nothing in gateway"; // Only actuates only in sensor
    buf->trusted = false;
}

int TSTP::Security_Stub::key_manager() {
    // SGX::key_manager
    _tstp->trace() << "TSTP::Security_Stub::key_manager()";
    
    write(KEY_MANAGER, NULL, 0);
    free(getAnswer());
    
    _tstp->setTimer(_tstp->KEY_MANAGER_TIMER, simtime_t(KEY_MANAGER_PERIOD, SIMTIME_US));
    return 0;
}



// ----- Serial Communication Auxiliary Functions ------------//

void TSTP::Security_Stub::check_sgx(void) {
    CallControl cc;
    void *data;
    bool hasData;
    usleep(CHECK_SGX_PERIOD/10);
    hasData = read(&cc, &data);
    while (hasData) {
        _tstp->trace() << "TSTP::Security_Stub::check_sgx(cc=" << cc << ")";
        switch(cc.type) {
            case CALL_ANSWER: {
                setAnswer(data);
            } break;
            case ALLOC: {
                uint16_t *sz = (uint16_t *)data;
                uint16_t size = *sz;
                free(sz);
                Buffer *buf = _tstp->alloc(size);
                write(CALL_ANSWER, buf, sizeof(Buffer));
                delete buf;
            } break;
            case SEND: {
                Buffer *buf;
                int32_t ret;
                buf = (Buffer *)data;
                ret = _tstp->send(buf);
                write(CALL_ANSWER, &ret, sizeof(int32_t));
            } break;
            case NOW: {
                Time t;
                t = _tstp->now();
                _tstp->trace() << "TSTP::Security_Stub::check_sgx(t=" << t << ")";
                write(CALL_ANSWER, &t, sizeof(Time));
            } break;
            case HERE: {
                Coordinates c;
                c = _tstp->here();
                _tstp->trace() << "TSTP::Security_Stub::check_sgx(c=" << c << ")";
                write(CALL_ANSWER, &c, sizeof(Coordinates));
            } break;
            case MARSHAL_SEND: {
                Buffer *buf;
                int32_t ret;
                buf = (Buffer *)data;
                _tstp->marshal(buf);
                ret = _tstp->send(buf);
                write(CALL_ANSWER, &ret, sizeof(int32_t));
            } break;
            default: {
                free(data);
                break;
            } break;
        }
        hasData = read(&cc, &data);
    }
}

void TSTP::Security_Stub::check(void) {
    check_sgx();
    _tstp->setTimer(_tstp->CHECK_SGX, simtime_t(CHECK_SGX_PERIOD, SIMTIME_US));
}

void TSTP::Security_Stub::setAnswer(void *data) {
    void *ptr = _answer;
    if (ptr != NULL) {
        free(ptr);
    }
    _answer = data;
}

void *TSTP::Security_Stub::getAnswer() {
    void *ptr;
    cout << "TSTP::Security_Stub::getAnswer(" << ")" << endl;
    while (_answer == NULL) {
        check_sgx();
    }
    ptr = _answer;
    _answer = NULL;
    return ptr;
}

bool TSTP::Security_Stub::read(CallControl *cc, void **buf) {
    int ret;
    ret = _sgx->read((char *)cc, sizeof(CallControl));
    if (ret > 0) {
        while (ret < sizeof(CallControl)) {
            int count;
            char *ptr = (char *)cc;
            count = _sgx->read(&ptr[ret], sizeof(CallControl)-ret);
            if (count >= 0) {
                ret += count;
            }
        }
        _tstp->trace() << "TSTP::Security_Stub::read(cc=" << *cc << ")";
        cout << "TSTP::Security_Stub::read(cc=" << *cc << ")" << endl;
        if (cc->dataSize > 0) {
            *buf = malloc(cc->dataSize);
            ret = _sgx->read((char *)*buf, cc->dataSize);
            while (ret < cc->dataSize) {
                int count;
                char *ptr = (char *)*buf;
                count = _sgx->read(&ptr[ret], sizeof(CallControl)-ret);
                if (count >= 0) {
                    ret += count;
                }
            }
        } else {
            *buf = NULL;
        }
        return true;
    }
    return false;
}

void TSTP::Security_Stub::write(enum CallTypes type, const void *buf, uint16_t length) {
    CallControl cc;
    cc.type = type;
    cc.dataSize = length;
    _tstp->trace() << "TSTP::Security_Stub::write(cc=" << cc << ")";
    cout << "TSTP::Security_Stub::write(cc=" << cc << ")" << endl;
    _sgx->write((const char *)&cc, sizeof(CallControl));
    _sgx->write((const char *)buf, cc.dataSize);
}

void TSTP::Security_Stub::go(void) {
    cout << "TSTP::Security_Stub::go()" << endl;
    write(GO, NULL, 0);
}
