
class Security_Stub: public Security
{
    friend class TSTP;
    
#include "sgx_com_data_structures.h"

public:
    Security_Stub(TSTP * tstp);
    
    virtual ~Security_Stub();

    void bootstrap();
    bool bootstrapped() { return _bootstrapped; }

    bool synchronized() { return true; }

    void marshal(Buffer * buf);

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

    void go(void);
    static Security_Stub *instance() {
        return (Security_Stub *)(TSTP::_instances[0]->_security);
    }
private:

    void write(enum CallTypes type, const void *buf, uint16_t length);
    bool read(CallControl *cc, void **buf);
    void *getAnswer();
    void setAnswer(void *data);
    
    int key_manager();
    void check_sgx(void);
    void check(void);
    
    void auth_request() {}
    

private:
    Serial *_sgx;
    bool _key_manager;
    TSTP * _tstp;
    bool _bootstrapped;
    void *_answer;
    
    uint64_t CHECK_SGX_PERIOD;
};
