
// TSTP Security
class Security_Full: public Security
{
    friend class TSTP;

private:
    class Peer;
    typedef _UTIL::Simple_List<Peer> Peers;
    class Peer
    {
    public:
        Peer(const Node_ID & id, const Region & v, TSTP * tstp)
            : _id(id), _valid(v), _el(this), _auth_time(0), _tstp(tstp) {
            ((TSTP::Security_Full *)_tstp->_security)->_cipher.encrypt(_id, _id, _auth);
        }

        void valid(const Region & r) { _valid = r; }
        const Region & valid() const { return _valid; }

        bool valid_deploy(const Coordinates & where, const Time & when) {
            return _valid.contains(where, when);
        }

        bool valid_request(const Node_Auth & auth, const Coordinates & where, const Time & when) {
            return !memcmp(auth, _auth, sizeof(Node_Auth)) && _valid.contains(where, when);
        }

        const Time & authentication_time() { return _auth_time; }

        Peers::Element * link() { return &_el; }

        const Master_Secret & master_secret() const { return _master_secret; }
        void master_secret(const Master_Secret & ms) {
            _master_secret = ms;
            _auth_time = _tstp->now();
        }

        const Node_Auth & auth() const { return _auth; }
        const Node_ID & id() const { return _id; }

        friend OStream & operator<<(OStream & db, const Peer & p) {
            db << "{id=" << p._id << ",au=" << p._auth << ",v=" << p._valid << ",ms=" << p._master_secret << ",el=" << &p._el << "}";
            return db;
        }

    private:
        Node_ID _id;
        Node_Auth _auth;
        Region _valid;
        Master_Secret _master_secret;
        Peers::Element _el;
        Time _auth_time;
        TSTP * _tstp;
    };

    struct Pending_Key;
    typedef _UTIL::Simple_List<Pending_Key> Pending_Keys;
    class Pending_Key
    {
    public:
        Pending_Key(const Public_Key & pk, TSTP * tstp) : _tstp(tstp), _master_secret_calculated(false), _creation(_tstp->now()), _public_key(pk), _el(this) {}

        bool expired() { return _tstp->now() - _creation > ((TSTP::Security_Full *)_tstp->_security)->KEY_EXPIRY; }

        const Master_Secret & master_secret() {
            if(_master_secret_calculated)
                return _master_secret;
            _master_secret = ((TSTP::Security_Full *)_tstp->_security)->_dh.shared_key(_public_key);
            _master_secret_calculated = true;
            //db<TSTP>(INF) << "TSTP::Security_Full::Pending_Key: Master Secret set: " << _master_secret;
            return _master_secret;
        }

        Pending_Keys::Element * link() { return &_el; };

        friend OStream & operator<<(OStream & db, const Pending_Key & p) {
            db << "{msc=" << p._master_secret_calculated << ",c=" << p._creation << ",pk=" << p._public_key << ",ms=" << p._master_secret << ",el=" << &p._el << "}";
            return db;
        }

    private:
        TSTP * _tstp;
        bool _master_secret_calculated;
        Time _creation;
        Public_Key _public_key;
        Master_Secret _master_secret;
        Pending_Keys::Element _el;
    };

public:
    Security_Full(TSTP * tstp) : _key_manager(false), _dh_requests_open(0), _tstp(tstp), _bootstrapped(false) {
        //db<TSTP>(TRC) << "TSTP::Security_Full()";
        Bignum<sizeof(Node_ID)> id(_tstp->_unit);
        new (&_id) Node_ID(&id, sizeof(id));
        assert(Cipher::KEY_SIZE == sizeof(Node_ID));
        _cipher.encrypt(_id, _id, _auth);
    }
    virtual ~Security_Full();

    void add_peer(const unsigned char * peer_id, unsigned int id_len, const Region & valid_region) {
        Node_ID id(peer_id, id_len);
        Peer * peer = new Peer(id, valid_region, _tstp);
        //while(CPU::tsl(_peers_lock));
        _pending_peers.insert(peer->link());
        //_peers_lock = false;
        if(!_key_manager) {
            _tstp->setTimer(_tstp->KEY_MANAGER_TIMER, simtime_t(KEY_MANAGER_PERIOD, SIMTIME_US));
            _key_manager = true;
        }
    }
    void bootstrap();
    bool bootstrapped() { return _bootstrapped; }

    bool synchronized() { return true; }

    void marshal(Buffer * buf);

    void update(NIC::Observed * obs, NIC::Protocol prot, NIC::Buffer * buf);

private:
    void auth_request() {
        _tstp->trace() << "TSTP::Security_Full::update(): Sending Auth_Request";
        // Send Authentication Request
        assert(_pending_keys.size() > 0);
        Buffer * resp = _tstp->alloc(sizeof(Auth_Request));
        new (resp->frame()) Auth_Request(_tstp, _auth, otp(_pending_keys.head()->object()->master_secret(), _id));
        _tstp->marshal(resp);
        _tstp->_nic->send(resp);
        _tstp->trace() << "Sent";
    }

    void pack(unsigned char * msg, const Peer * peer) {
        Time t = _tstp->now() / POLY_TIME_WINDOW;
        _tstp->trace() << "t = " << t;

        const unsigned char * ms = reinterpret_cast<const unsigned char *>(&peer->master_secret());
        const unsigned char * id = reinterpret_cast<const unsigned char *>(&peer->id());

        unsigned char nonce[16];
        memset(nonce, 0, 16);
        memcpy(nonce, &t, sizeof(Time) < 16u ? sizeof(Time) : 16u);

        Poly1305 poly(id, ms);

        poly.stamp(&msg[sizeof(Master_Secret)], nonce, reinterpret_cast<const unsigned char *>(msg), sizeof(Master_Secret));

        if(_tstp->_encrypt) {
            // mi = ms ^ _id
            static const unsigned int MI_SIZE = sizeof(Node_ID) > sizeof(Master_Secret) ? sizeof(Node_ID) : sizeof(Master_Secret);
            unsigned char mi[MI_SIZE];
            unsigned int i;
            for(i = 0; (i < sizeof(Node_ID)) && (i < sizeof(Master_Secret)); i++)
                mi[i] = id[i] ^ ms[i];
            for(; i < sizeof(Node_ID); i++)
                mi[i] = id[i];
            for(; i < sizeof(Master_Secret); i++)
                mi[i] = ms[i];

            OTP key;
            poly.stamp(key, nonce, mi, MI_SIZE);
            _cipher.encrypt(msg, key, msg);
        }
    }

    bool unpack(const Peer * peer, unsigned char * msg, const unsigned char * mac, Time reception_time) {
        unsigned char original_msg[sizeof(Master_Secret)];
        memcpy(original_msg, msg, sizeof(Master_Secret));

        const unsigned char * ms = reinterpret_cast<const unsigned char *>(&peer->master_secret());
        const unsigned char * id = reinterpret_cast<const unsigned char *>(&peer->id());

        // mi = ms ^ _id
        static const unsigned int MI_SIZE = sizeof(Node_ID) > sizeof(Master_Secret) ? sizeof(Node_ID) : sizeof(Master_Secret);
        unsigned char mi[MI_SIZE];
        unsigned int i;
        for(i = 0; (i < sizeof(Node_ID)) && (i < sizeof(Master_Secret)); i++)
            mi[i] = id[i] ^ ms[i];
        for(; i < sizeof(Node_ID); i++)
            mi[i] = id[i];
        for(; i < sizeof(Master_Secret); i++)
            mi[i] = ms[i];

        reception_time /= POLY_TIME_WINDOW;
        _tstp->trace() << "t = " << reception_time;

        OTP key;
        unsigned char nonce[16];
        Poly1305 poly(id, ms);

        memset(nonce, 0, 16);
        memcpy(nonce, &reception_time, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        poly.stamp(key, nonce, mi, MI_SIZE);
        if(_tstp->_encrypt)
            _cipher.decrypt(original_msg, key, msg);
        if(poly.verify(mac, nonce, msg, sizeof(Master_Secret)))
            return true;

        reception_time--;
        memset(nonce, 0, 16);
        memcpy(nonce, &reception_time, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        poly.stamp(key, nonce, mi, MI_SIZE);
        if(_tstp->_encrypt)
            _cipher.decrypt(original_msg, key, msg);
        if(poly.verify(mac, nonce, msg, sizeof(Master_Secret)))
            return true;

        reception_time += 2;
        memset(nonce, 0, 16);
        memcpy(nonce, &reception_time, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        poly.stamp(key, nonce, mi, MI_SIZE);
        if(_tstp->_encrypt)
            _cipher.decrypt(original_msg, key, msg);
        if(poly.verify(mac, nonce, msg, sizeof(Master_Secret)))
            return true;

        memcpy(msg, original_msg, sizeof(Master_Secret));
        return false;
    }

    void encrypt(const void * msg, const Peer * peer, unsigned char * out) {
        OTP key = otp(peer->master_secret(), peer->id());
        _cipher.encrypt(msg, key, out);
    }

    OTP otp(const Master_Secret & master_secret, const Node_ID & id) {
        const unsigned char * ms = reinterpret_cast<const unsigned char *>(&master_secret);

        // mi = ms ^ _id
        static const unsigned int MI_SIZE = sizeof(Node_ID) > sizeof(Master_Secret) ? sizeof(Node_ID) : sizeof(Master_Secret);
        unsigned char mi[MI_SIZE];
        unsigned int i;
        for(i = 0; (i < sizeof(Node_ID)) && (i < sizeof(Master_Secret)); i++)
            mi[i] = id[i] ^ ms[i];
        for(; i < sizeof(Node_ID); i++)
            mi[i] = id[i];
        for(; i < sizeof(Master_Secret); i++)
            mi[i] = ms[i];

        Time t = _tstp->now() / POLY_TIME_WINDOW;

        unsigned char nonce[16];
        memset(nonce, 0, 16);
        memcpy(nonce, &t, sizeof(Time) < 16u ? sizeof(Time) : 16u);

        OTP out;
        Poly1305(id, ms).stamp(out, nonce, mi, MI_SIZE);
        return out;
    }

    bool verify_auth_request(const Master_Secret & master_secret, const Node_ID & id, const OTP & otp) {
        const unsigned char * ms = reinterpret_cast<const unsigned char *>(&master_secret);

        // mi = ms ^ _id
        static const unsigned int MI_SIZE = sizeof(Node_ID) > sizeof(Master_Secret) ? sizeof(Node_ID) : sizeof(Master_Secret);
        unsigned char mi[MI_SIZE];
        unsigned int i;
        for(i = 0; (i < sizeof(Node_ID)) && (i < sizeof(Master_Secret)); i++)
            mi[i] = id[i] ^ ms[i];
        for(; i < sizeof(Node_ID); i++)
            mi[i] = id[i];
        for(; i < sizeof(Master_Secret); i++)
            mi[i] = ms[i];

        unsigned char nonce[16];
        Time t = _tstp->now() / POLY_TIME_WINDOW;

        Poly1305 poly(id, ms);

        memset(nonce, 0, 16);
        memcpy(nonce, &t, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        if(poly.verify(otp, nonce, mi, MI_SIZE))
            return true;

        t--;
        memset(nonce, 0, 16);
        memcpy(nonce, &t, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        if(poly.verify(otp, nonce, mi, MI_SIZE))
            return true;

        t += 2;
        memset(nonce, 0, 16);
        memcpy(nonce, &t, sizeof(Time) < 16u ? sizeof(Time) : 16u);
        if(poly.verify(otp, nonce, mi, MI_SIZE))
            return true;

        return false;
    }

    int key_manager() {

        Peers::Element * last_dh_request = 0;

        _tstp->trace() << "TSTP::Security_Full::key_manager()";
        CPU::int_disable();
        //while(CPU::tsl(_peers_lock));

        // Cleanup expired pending keys
        Pending_Keys::Element * next_key;
        for(Pending_Keys::Element * el = _pending_keys.head(); el; el = next_key) {
            next_key = el->next();
            Pending_Key * p = el->object();
            if(p->expired()) {
                _pending_keys.remove(el);
                delete p;
                //db<TSTP>(INF) << "TSTP::Security_Full::key_manager(): removed pending key";
            }
        }

        // Cleanup expired peers
        Peers::Element * next;
        for(Peers::Element * el = _trusted_peers.head(); el; el = next) {
            next = el->next();
            Peer * p = el->object();
            if(!p->valid_deploy(p->valid().center, _tstp->now())) {
                _trusted_peers.remove(el);
                delete p;
                //db<TSTP>(INF) << "TSTP::Security_Full::key_manager(): permanently removed trusted peer";
            }
        }
        for(Peers::Element * el = _pending_peers.head(); el; el = next) {
            next = el->next();
            Peer * p = el->object();
            if(!p->valid_deploy(p->valid().center, _tstp->now())) {
                _pending_peers.remove(el);
                delete p;
                //db<TSTP>(INF) << "TSTP::Security_Full::key_manager(): permanently removed pending peer";
            }
        }

        // Cleanup expired established keys
        for(Peers::Element * el = _trusted_peers.head(); el; el = next) {
            next = el->next();
            Peer * p = el->object();
            if(_tstp->now() - p->authentication_time() > KEY_EXPIRY) {
                _trusted_peers.remove(el);
                _pending_peers.insert(el);
                //db<TSTP>(INF) << "TSTP::Security_Full::key_manager(): trusted peer's key expired";
            }
        }

        // Send DH Request to at most one peer
        Peers::Element * el;
        if(last_dh_request && last_dh_request->next())
            el = last_dh_request->next();
        else
            el = _pending_peers.head();

        last_dh_request = 0;

        for(; el; el = el->next()) {
            Peer * p = el->object();
            if(p->valid_deploy(p->valid().center, _tstp->now())) {
                last_dh_request = el;
                Buffer * buf = _tstp->alloc(sizeof(DH_Request));
                new (buf->frame()->data<DH_Request>()) DH_Request(_tstp, Region::Space(p->valid().center, p->valid().radius), _dh.public_key());
                _tstp->marshal(buf);
                _dh_requests_open++;
                _tstp->_nic->send(buf);
                //db<TSTP>(INF) << "TSTP::Security_Full::key_manager(): Sent DH_Request: "  << *buf->frame()->data<DH_Request>();
                break;
            }
        }

        //_peers_lock = false;
        CPU::int_enable();

        _tstp->setTimer(_tstp->KEY_MANAGER_TIMER, simtime_t(KEY_MANAGER_PERIOD, SIMTIME_US));

        return 0;
    }

private:
    Cipher _cipher;
    Node_ID _id;
    Node_Auth _auth;
    Diffie_Hellman _dh;
    Pending_Keys _pending_keys;
    Peers _pending_peers;
    Peers _trusted_peers;
    volatile bool _peers_lock;
    bool _key_manager;
    unsigned int _dh_requests_open;
    TSTP * _tstp;
    bool _bootstrapped;
};
