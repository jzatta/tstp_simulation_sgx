
#include "tstp.h"

void TSTP::Security_Full::bootstrap()
{
    _tstp->trace() << "Security_Full::bootstrap()";

    assert(_tstp->_sinks.size() == 1); // Security_Full only works in single-sink mode for now

    KEY_EXPIRY = _tstp->par("keyExpiry").doubleValue();
    DHREQUEST_AUTH_DELAY = _tstp->par("dhRequestProcessingDelay").doubleValue();
    KEY_MANAGER_PERIOD = _tstp->par("keyManagerPeriod").doubleValue();
    POLY_TIME_WINDOW = _tstp->par("securityTimeWindow").doubleValue();

    _tstp->_nic->attach(this, NIC::TSTP);

    if(_tstp->_sink) {
        for(unsigned int i = 0; i < _tstp->_units; i++) {
            if(i == _tstp->_unit)
                continue;
            bool send_messages = !_tstp->_instances[i]->par("startWithKeysEstablished");
            if(send_messages) {
                // Bootstrap might not have been called yet at the other nodes
                VirtualMobilityManager *nodeMobilityModule = check_and_cast<
                            VirtualMobilityManager*>(
                            _tstp->getParentModule()->getParentModule()->getParentModule()->getSubmodule(
                                "node", i)->getSubmodule("MobilityManager"));

                Coordinates::Number x = nodeMobilityModule->getLocation().x * 100.0;
                Coordinates::Number y = nodeMobilityModule->getLocation().y * 100.0;
                Coordinates::Number z = nodeMobilityModule->getLocation().z * 100.0;

                Coordinates there(x, y, z);

                add_peer(((TSTP::Security_Full *)_tstp->_instances[i]->_security)->_id, sizeof(Node_ID), Region(there, 0, 0, -1));
            }
        }
        _bootstrapped = true;
    } else {
        if(_tstp->par("startWithKeysEstablished")) {

            // Establish keys without sending any messages

            TSTP::Security_Full * sink = 0;
            for(unsigned int i = 0; i < _tstp->_units; i++) {
                if(_tstp->_instances[i]->_sink) {
                    sink = (TSTP::Security_Full *)_tstp->_instances[i]->_security;
                    break;
                }
            }
            if(!sink)
                throw cRuntimeError("No sinks found!");
            Master_Secret ms = _dh.shared_key(sink->_dh.public_key());
            Master_Secret ms2 = sink->_dh.shared_key(_dh.public_key());
            assert(ms == ms2);
            Peer * p = new Peer(_id, Region(_tstp->sink(), 0, 0, -1), _tstp);
            Peer * p2 = new Peer(_id, Region(_tstp->here(), 0, 0, -1), sink->_tstp);
            p->master_secret(ms);
            p2->master_secret(ms2);
            _trusted_peers.insert(p->link());
            sink->_trusted_peers.insert(p2->link());
            _bootstrapped = true;
        } else {
            Peer * peer = new Peer(_id, Region(_tstp->sink(), 0, 0, -1), _tstp);
            _pending_peers.insert(peer->link());
        }
    }

    if(bootstrapped())
        _tstp->bootstrap();
}

// TSTP::Security_Full
// Class attributes

// Methods
void TSTP::Security_Full::update(NIC::Observed * obs, NIC::Protocol prot, Buffer * buf)
{
    bool was_bootstrapped = bootstrapped();

    _tstp->trace() << "TSTP::Security_Full::update(obs=" << obs << ",buf=" << buf << ")";

    if(!buf->is_microframe && buf->destined_to_me) {

        switch(buf->frame()->data<Header>()->type()) {

            case CONTROL: {
                _tstp->trace() << "TSTP::Security_Full::update(): Control message received";
                switch(buf->frame()->data<Control>()->subtype()) {
                    case REPORT: {
                        _tstp->trace() << "TSTP::Security_Full::update(): Report";
                        buf->trusted = true;
                    } break;

                    case DH_REQUEST: {
                        if(_tstp->here() != _tstp->sink()) {
                            DH_Request * dh_req = buf->frame()->data<DH_Request>();
                            _tstp->trace() << "TSTP::Security_Full::update(): DH_Request message received: " << *dh_req;

                            //while(CPU::tsl(_peers_lock));
                            //CPU::int_disable();
                            bool valid_peer = false;
                            for(Peers::Element * el = _pending_peers.head(); el; el = el->next())
                                if(el->object()->valid_deploy(dh_req->origin(), _tstp->now())) {
                                    valid_peer = true;
                                    _tstp->trace() << "Found pending peer";
                                    break;
                                }
                            if(!valid_peer)
                                for(Peers::Element * el = _trusted_peers.head(); el; el = el->next())
                                    if(el->object()->valid_deploy(dh_req->origin(), _tstp->now())) {
                                        valid_peer = true;
                                        _trusted_peers.remove(el);
                                        _pending_peers.insert(el);
                                        _tstp->trace() << "Moving trusted peer to pending";
                                        break;
                                    }
                            //_peers_lock = false;
                            //CPU::int_enable();

                            if(valid_peer) {
                                _tstp->trace() << "TSTP::Security_Full::update(): Sending DH_Response";
                                // Respond to Diffie-Hellman request
                                Buffer * resp = _tstp->alloc(sizeof(DH_Response));
                                new (resp->frame()) DH_Response(_tstp, _dh.public_key());
                                _tstp->marshal(resp);
                                _tstp->_nic->send(resp);

                                // Calculate Master Secret
                                Pending_Key * pk = new Pending_Key(buf->frame()->data<DH_Request>()->key(), _tstp);
                                pk->master_secret();
                                //Master_Secret ms = pk->master_secret();
                                //while(CPU::tsl(_peers_lock));
                                //CPU::int_disable();
                                _pending_keys.insert(pk->link());
                                //_peers_lock = false;
                                //CPU::int_enable();

                                _tstp->schedule_auth_request(); // Castalia workaround
                            }
                        }
                    } break;

                    case DH_RESPONSE: {
                        if(_dh_requests_open) {
                            DH_Response * dh_resp = buf->frame()->data<DH_Response>();
                            _tstp->trace() << "TSTP::Security_Full::update(): DH_Response message received: " << *dh_resp;

                            //CPU::int_disable();
                            bool valid_peer = false;
                            for(Peers::Element * el = _pending_peers.head(); el; el = el->next())
                                if(el->object()->valid_deploy(dh_resp->origin(), _tstp->now())) {
                                    valid_peer = true;
                                    _tstp->trace() << "Valid peer found: " << *el->object();
                                    break;
                                }

                            if(valid_peer) {
                                _dh_requests_open--;
                                Pending_Key * pk = new Pending_Key(buf->frame()->data<DH_Response>()->key(), _tstp);
                                _pending_keys.insert(pk->link());
                                _tstp->trace() << "TSTP::Security_Full::update(): Inserting new Pending Key: " << *pk;
                            }
                            //CPU::int_enable();
                        }
                    } break;

                    case AUTH_REQUEST: {

                        Auth_Request * auth_req = buf->frame()->data<Auth_Request>();
                        _tstp->trace() << "TSTP::Security_Full::update(): Auth_Request message received: " << *auth_req;

                        //CPU::int_disable();
                        Peer * auth_peer = 0;
                        for(Peers::Element * el = _pending_peers.head(); el; el = el->next()) {
                            Peer * peer = el->object();

                            if(peer->valid_request(auth_req->auth(), auth_req->origin(), _tstp->now())) {
                                _tstp->trace() << "valid_request ";
                                for(Pending_Keys::Element * pk_el = _pending_keys.head(); pk_el; pk_el = pk_el->next()) {
                                    Pending_Key * pk = pk_el->object();
                                    if(verify_auth_request(pk->master_secret(), peer->id(), auth_req->otp())) {
                                        peer->master_secret(pk->master_secret());
                                        _pending_peers.remove(el);
                                        _trusted_peers.insert(el);
                                        auth_peer = peer;

                                        _pending_keys.remove(pk_el);
                                        delete pk_el->object();

                                        break;
                                    }
                                }
                                if(auth_peer)
                                    break;
                            }
                        }
                        //CPU::int_enable();

                        if(auth_peer) {
                            Node_Auth encrypted_auth;
                            encrypt(auth_peer->auth(), auth_peer, encrypted_auth);

                            Buffer * resp = _tstp->alloc(sizeof(Auth_Granted));
                            new (resp->frame()) Auth_Granted(_tstp, Region::Space(auth_peer->valid().center, auth_peer->valid().radius), encrypted_auth);
                            _tstp->marshal(resp);
                            _tstp->trace() << "TSTP::Security_Full: Sending Auth_Granted message " << resp->frame()->data<Auth_Granted>();
                            _tstp->_nic->send(resp);
                        } else
                            _tstp->trace() << "TSTP::Security_Full::update(): No peer found";
                    } break;

                    case AUTH_GRANTED: {

                        if(_tstp->here() != _tstp->sink()) {
                            Auth_Granted * auth_grant = buf->frame()->data<Auth_Granted>();
                            _tstp->trace() << "TSTP::Security_Full::update(): Auth_Granted message received: " << *auth_grant;
                            //CPU::int_disable();
                            bool auth_peer = false;
                            for(Peers::Element * el = _pending_peers.head(); el; el = el->next()) {
                                Peer * peer = el->object();
                                for(Pending_Keys::Element * pk_el = _pending_keys.head(); pk_el; pk_el = pk_el->next()) {
                                    Pending_Key * pk = pk_el->object();
                                    Node_Auth decrypted_auth;
                                    OTP key = otp(pk->master_secret(), peer->id());
                                    _cipher.decrypt(auth_grant->auth(), key, decrypted_auth);
                                    if(decrypted_auth == _auth) {
                                        peer->master_secret(pk->master_secret());
                                        _pending_peers.remove(el);
                                        _trusted_peers.insert(el);
                                        auth_peer = true;
                                        _pending_keys.remove(pk_el);
                                        delete pk_el->object();

                                        break;
                                    }
                                }
                                if(auth_peer)
                                    break;
                            }
                            if(auth_peer) {
                                _tstp->trace() << "Auth Granted!";
                                _bootstrapped = true;
                            }
                            else
                                _tstp->trace() << "No peer found for Auth code";
                            //CPU::int_enable();
                        }
                    } break;

                    default: break;
                }
            } break;
            case RESPONSE: {
                _tstp->trace() << "TSTP::Security_Full::update(): Response message received";
                _tstp->trace() << "from " << buf->frame()->data<Header>()->origin();
                Response * resp = buf->frame()->data<Response>();
                Time reception_time = _tstp->_nic->count2us(buf->sfd_time_stamp);
                for(Peers::Element * el = _trusted_peers.head(); el; el = el->next()) {
                    if(el->object()->valid_deploy(buf->frame()->data<Header>()->origin(), _tstp->now())) {
                        _tstp->trace() << "Found valid peer";
                        unsigned char * data = resp->data<unsigned char>();
                        if(unpack(el->object(), data, &data[sizeof(Master_Secret)], reception_time)) {
                            buf->trusted = true;
                            _tstp->trace() << "Unpack done!";
                            break;
                        } else {
                            _tstp->trace() << "Unpack failed";
                        }
                    }
                }
            } break;
            case INTEREST: {
                buf->trusted = true;
            } break;
            default: break;
        }
    } else
        buf->trusted = true;

    if((!was_bootstrapped) && bootstrapped())
        _tstp->bootstrap();
}

void TSTP::Security_Full::marshal(Buffer * buf)
{
    _tstp->trace() << "TSTP::Security_Full::marshal(buf=" << buf << ")";
    buf->trusted = false;

    if(buf->frame()->data<Header>()->type() == TSTP::RESPONSE) {
        Peer * peer = 0;
        for(Peers::Element * el = _trusted_peers.head(); el; el = el->next())
            if(el->object()->valid_deploy(_tstp->destination(buf).center, _tstp->now())) {
                peer = el->object();
                break;
            }
        if(!peer)
            return;

        // Pad data to the size of the key
        unsigned int data_size = buf->size() - (sizeof(Response) - sizeof(Response::Data));
        Response * response = buf->frame()->data<Response>();
        unsigned char * data = response->data<unsigned char>();
        buf->size(sizeof(Response) - sizeof(Response::Data) + sizeof(Master_Secret));
        for(unsigned int i = data_size; i < sizeof(Master_Secret); i++)
            data[i] = 0;

        pack(data, peer);
        buf->trusted = true;
    }
}

TSTP::Security_Full::~Security_Full()
{
    _tstp->trace() << "TSTP::~Security_Full()";
    while(auto el = _trusted_peers.remove_head())
        delete el->object();
    while(auto el = _pending_peers.remove_head())
        delete el->object();
    while(auto el = _pending_keys.remove_head())
        delete el->object();

//    _tstp->_nic->detach(this, 0);
}
