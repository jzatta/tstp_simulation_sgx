//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSTP_MAC_H_
#define TSTP_MAC_H_

#include <tstp_common.h>
#include <observer.h>
//#include <dsrc_phy.h>

__USING_SYS

template<typename Radio>
class TSTP_MAC: public TSTP_Common, public Radio
{
    friend class TSTP_MAC_NED;
    static const bool check_hash_collisions = false;
    static const bool break_silence = false;
    static const bool multichannel = true;
    static const bool rely_on_mf_id = false;

    static const unsigned int MICROFRAME_CHANNEL = 26;
    //static const unsigned int DATA_CHANNEL = 15;

protected:
    typedef IEEE802_15_4 Phy_Layer;
    //typedef DSRC_Phy Phy_Layer;

    enum State {
        UPDATE_TX_SCHEDULE = 0,
        SLEEP_S            = 1,
        RX_MF              = 2,
        SLEEP_DATA         = 3,
        RX_DATA            = 4,
        BACKOFF            = 5,
        CCA                = 6,
        TX_MF              = 7,
        TX_DATA            = 8,
    };

public:
    using TSTP_Common::Address;
    using TSTP_Common::Header;
    using TSTP_Common::Frame;
    typedef typename Radio::Timer Timer;
    typedef typename Radio::Timer::Time_Stamp Time_Stamp;

    //static const unsigned int MTU = Frame::MTU;

    const Statistics & statistics() { return _stats; }
    unsigned int period() { return PERIOD_US; }

private:
    bool radio_sleeps_after_tx;
    bool random_backoff;
    bool silence;

    unsigned int MICROFRAME_SIZE;
    unsigned int DESIRED_DUTY_CYCLE;
    unsigned int INT_HANDLING_DELAY;
    unsigned int SLEEP_TO_TX_DELAY;
    unsigned int SLEEP_TO_RX_DELAY;
    unsigned int RX_TO_TX_DELAY;
    unsigned int Tu;
    unsigned int Ti;
    unsigned int TIME_BETWEEN_MICROFRAMES;
    unsigned int Ts;
    unsigned int MICROFRAME_TIME;
    unsigned int Tr;
    unsigned int RX_MF_TIMEOUT;
    unsigned int NMF;
    unsigned int N_MICROFRAMES;
    unsigned int CI;
    unsigned int PERIOD;
    unsigned int PERIOD_US;
    unsigned int SLEEP_PERIOD;
    unsigned int DUTY_CYCLE;
    unsigned int DATA_LISTEN_MARGIN;
    unsigned int DATA_SKIP_TIME;
    unsigned int RX_DATA_TIMEOUT;
    unsigned int G;
    unsigned int CCA_TIME;
    unsigned int OFFSET_GENERAL_LOWER_BOUND;
    unsigned int OFFSET_GENERAL_UPPER_BOUND;
    unsigned int OFFSET_LOWER_BOUND;
    unsigned int OFFSET_UPPER_BOUND;

    typedef typename Radio::Timer Watchdog;

protected:
    TSTP_MAC(unsigned int unit = 0)
        : Radio(unit),
            _unit(unit),
            _keep_alives_sent(0),
            _mfs_sent(0),
            _expired_bufs(0),
            _data_sent(0),
            _data_relayed(0),
            _hash_collisions(0),
            _silence_periods(0),
            _silence_buffer(0),
            _max_times_txed(0)
    {
        Radio::trace() << "TSTP_MAC(u=" << unit << ")" << endl;
        _instances++;
        _instance[unit] = this;
        _global_keep_alives_sent = 0;
    }

    ~TSTP_MAC() {
        for(Buffer::Element * el = _tx_schedule.head(); el; el = _tx_schedule.head()) {
            Buffer * b = el->object();
            _tx_schedule.remove(el);
            if(b)
                free(b);
        }
        delete _offset_vector;
        _instance[_unit] = 0;
        _instances--;
    }

    // Called after the Radio's constructor
    void constructor_epilogue() {
        Radio::trace() << "TSTP_MAC::constructor_epilogue()";
        Radio::trace() << "_unit = " << _unit;
        _offset_vector = new cOutVector("Offset");
        Watchdog::enable();
        update_tx_schedule();
    }

    // Filter and assemble RX Buffer Metainformation
    bool pre_notify(Buffer * buf) {
        if(_in_rx_mf) { // State: RX MF (part 2/3)
            Radio::trace() << "State: RX MF (part 2/3)";
            if(buf->size() == MICROFRAME_SIZE) {

                assert(buf->frame()->data<Microframe>()->count() < N_MICROFRAMES);

                Timer::int_disable();

                Radio::power(Power_Mode::PM_SLEEP);

                _in_rx_mf = false;

                Microframe * mf = buf->frame()->data<Microframe>();
                Frame_ID id = mf->id();

                // Initialize Buffer Metainformation
                buf->id = id;
                buf->downlink = mf->all_listen();
                buf->is_new = false;
                buf->is_microframe = true;
                buf->relevant = mf->all_listen();
                buf->trusted = false;
                buf->hint = mf->hint();
                buf->microframe_count = mf->count();
                buf->offset = OFFSET_GENERAL_UPPER_BOUND;
                buf->progress_bits = 0;
                buf->times_txed = 0;

                // Forge a TSTP identifier to make the radio notify listeners
#ifdef OLDMICROFRAME
                mf->all_listen(false);
                mf->count(TSTP_Common::V0 >> 1);
#else
                mf->all_listen(false);
                mf->id(TSTP_Common::V0 >> 1);
#endif

                return true;
            }
            return false;
        } else if(_in_rx_data) { // State: RX Data (part 2/3)
            Radio::trace() << "State: RX Data (part 2/3)";

            buf->is_new = false;

            if(buf->size() == MICROFRAME_SIZE)
                return false;

            Radio::power(Power_Mode::PM_SLEEP);

            // Initialize Buffer Metainformation
            buf->id = id(buf);
            buf->hint = _receiving_data_hint;
            buf->is_microframe = false;
            buf->trusted = false;
            buf->random_backoff_exponent = 0;
            buf->microframe_count = 0;
            buf->offset = OFFSET_GENERAL_UPPER_BOUND;
            buf->progress_bits = 0;
            buf->times_txed = 0;

#ifndef DISABLE_TRACE
            Radio::trace() << "TSTP_MAC::pre_notify: Data frame of size "
                << buf->size() << " received: " << buf->frame() << " at "
                << Radio::Timer::count2us(buf->sfd_time_stamp) << endl;
#endif

            if(!rely_on_mf_id) {
                // Clear scheduled messages that are equivalent
                Buffer::Element * next;
                for(Buffer::Element * el = _tx_schedule.head(); el; el = next) {
                    next = el->next();
                    Buffer * queued_buf = el->object();
                    if(equals(queued_buf, buf)) {
                        if(!queued_buf->destined_to_me) {
//                            unsigned char received_bitmap = 1; // In multisink, this comes from the microframe
//                            for(unsigned int bit_position = 0; bit_position < sizeof(unsigned char); bit_position++) {
//                                /* For each bit, perform the following operation:
//                                   1 @ 0 @ 0 = 0
//                                   1 @ 0 @ 1 = 0
//                                   1 @ 1 @ 0 = 1
//                                   1 @ 1 @ 1 = 1
//                                   0 @ X @ Y = Y
//                                   with queued_buf->progress_bits = received_bitmap @ buf->progress_bits @ queued_buf->progress_bits
//                                 */
//                                if(received_bitmap & (1 << bit_position)) {
//                                    unsigned char bit =
//                                            (received_bitmap & (1 << bit_position))
//                                            & (buf->progress_bits
//                                                    & (1 << bit_position));
//                                    queued_buf->progress_bits &=
//                                            ~(1 << bit_position);
//                                    queued_buf->progress_bits &= bit;
//                                }
//                            }
//                            if(queued_buf->progress_bits == 0) {
#ifndef DISABLE_TRACE
                            Radio::trace()
                            << "TSTP_MAC::pre_notify: ACK received, ID="
                            << queued_buf->id << " at "
                            << Timer::count2us(buf->sfd_time_stamp) << endl;
#endif
                            if(break_silence && (_silence_buffer == queued_buf)) {
                                _silence_periods = 0;
                                _silence_buffer = 0;
                            }
                            _tx_schedule.remove(el);
                            delete queued_buf;
//                        }
                        }
                    }
                }
            }

            return true;
        } else {
            Radio::trace() << "MAC not in RX state";
            return false;
        }
    }

    bool post_notify(Buffer * buf) {
        if(buf->is_microframe) { // State: RX MF (part 3/3)
            Radio::trace() << "State: RX MF (part 3/3)";

            Time_Stamp data_time = buf->sfd_time_stamp
                + Timer::us2count(
                    MICROFRAME_SIZE * 1000000 / Phy_Layer::BYTE_RATE)
                + TIME_BETWEEN_MICROFRAMES
                + buf->microframe_count
                    * (TIME_BETWEEN_MICROFRAMES + MICROFRAME_TIME);

            Watchdog::kick();

            if(rely_on_mf_id) {
                // Clear scheduled messages with same ID
                Buffer::Element * next;
                for(Buffer::Element * el = _tx_schedule.head(); el; el = next) {
                    next = el->next();
                    Buffer * queued_buf = el->object();
                    if(queued_buf->id == buf->id) {
                        if(!queued_buf->destined_to_me) {
                            if(break_silence && (_silence_buffer == queued_buf)) {
                                _silence_periods = 0;
                                _silence_buffer = 0;
                            }
                            _tx_schedule.remove(el);
                            delete queued_buf;
                        }
                    }
                }
            } else {
                // If we have a buffer with the same ID, we need to receive the data
                // to make sure we can clear it from the tx schedule
                if(!buf->relevant)
                    for(Buffer::Element * el = _tx_schedule.head(); el; el = el->next())
                        if(el->object()->id == buf->id) {
                            buf->relevant = true;
                            break;
                        }
            }

            if(buf->relevant) { // Transition: [Relevant MF]
                _receiving_data_hint = buf->hint;
                _receiving_data_channel = (buf->id % 14) + 11;
                // State: Sleep until Data
                Radio::trace() << "State: Sleep until Data";
                if(data_time > Timer::read() + DATA_LISTEN_MARGIN / 2 + SLEEP_TO_RX_DELAY)
                    data_time -= DATA_LISTEN_MARGIN / 2;
                else
                    data_time = Timer::read() + SLEEP_TO_RX_DELAY + 1;
                Timer::interrupt(data_time - SLEEP_TO_RX_DELAY, rx_data);
            } else { // Transition: [Irrelevant MF]
                Radio::trace() << "Transition: [Irrelevant MF] (2)";
                Timer::interrupt(data_time + DATA_SKIP_TIME, update_tx_schedule);
            }
        }

        free(buf);
        return true;
    }

public:
    Frame_ID id(Buffer * buf) {
        Frame_ID ret = 0;
        assert (buf->size() >= sizeof(Header));

        Header * header = buf->frame()->data<Header>();
        auto conf = header->confidence();
        auto tr = header->time_request();
        header->time_request(false); // Mask out time_request field for hashing
        header->confidence(0); // Mask out confidence field for hashing

        // Hash over the fields config,Origin(t,x,y,z) of the TSTP header
        unsigned int hash = murmur_hash(buf->frame()->data<const char>(), 2 + sizeof(Time) + sizeof(Coordinates));
        ret = ((hash & 0xffff0000) >> 16) ^ (hash & 0x0000ffff);
        ret = ret & Microframe::ID_MAX;

        // Restore masked out fields
        header->time_request(tr);
        header->confidence(conf);

        if(check_hash_collisions) {
            for(unsigned int i = 0; i < _instances; i++) {
                if(i == _unit)
                    continue;
                TSTP_MAC * mac = _instance[i];
                for(Buffer::Element * el = mac->_tx_schedule.head(); el; el = el->next()) {
                    Buffer * other_buffer = el->object();
                    Header * other_header = other_buffer->frame()->data<Header>();

                    Frame_ID ret2 = 0;
                    auto conf = other_header->confidence();
                    auto tr = other_header->time_request();
                    other_header->time_request(false); // Mask out time_request field for hashing
                    other_header->confidence(0); // Mask out confidence field for hashing

                    // Hash over the fields config,Origin(t,x,y,z) of the TSTP header
                    unsigned int hash = murmur_hash(other_buffer->frame()->data<const char>(), 2 + sizeof(Time) + sizeof(Coordinates));
                    ret2 = ((hash & 0xffff0000) >> 16) ^ (hash & 0x0000ffff);
                    ret2 = ret2 & Microframe::ID_MAX;

                    // Restore masked out fields
                    other_header->time_request(tr);
                    other_header->confidence(conf);

                    if(ret2 != other_buffer->id) {
                        //std::cout << "Swapped buffer ID at node " << i << "!" << std::endl;
                        //std::cout << "Buffer = ";
                        //print(other_buffer);
                        //std::cout << "ID = " << other_buffer->id << ", should be " << ret2 << "!" << std::endl;
                        //throw cRuntimeError("Swapped buffer ID!");
                    }

                    if(other_buffer->id == ret) {
                        if( (other_header->version() != header->version()) ||
                            (other_header->type() != header->type()) ||
                            (other_header->scale() != header->scale()) ||
                            (other_header->time() != header->time()) ||
                            (other_header->origin() != header->origin()) )
                        {
                            _hash_collisions++;
                            Radio::trace() << "Hash collision detected!";
                            Radio::trace() << other_buffer->frame()->data<Header>()->time() << " " << other_buffer->frame()->data<Header>()->origin() << " " << other_buffer->id << " " << i;
                            Radio::trace() << buf->frame()->data<Header>()->time() << " " << buf->frame()->data<Header>()->origin() << " " << ret << " " << Radio::self;
                            //std::cout << "Hash collision detected!" << std::endl;
                            //print(buf);
                            //print(other_buffer);
                            //std::cout << other_buffer->frame()->data<Header>()->time() << " " << other_buffer->frame()->data<Header>()->origin() << " " << other_buffer->id << " " << i << std::endl;
                            //std::cout << buf->frame()->data<Header>()->time() << " " << buf->frame()->data<Header>()->origin() << " " << ret << " " << _unit << std::endl;
                            //throw cRuntimeError("Hash collision detected!");
                        }
                    }
                }
            }
        }

        return ret;
    }

    // Assemble TX Buffer Metainformation
    void marshal(Buffer * buf,
                 const Address & src,
                 const Address & dst,
                 const Type & type)
    {
        buf->is_microframe = false;
        buf->trusted = false;
        buf->is_new = true;
        buf->random_backoff_exponent = 0;
        buf->microframe_count = 0;
        buf->offset = OFFSET_GENERAL_UPPER_BOUND;
        buf->times_txed = 0;
    }

    unsigned int unmarshal(Buffer * buf,
                           Address * src,
                           Address * dst,
                           Type * type,
                           void * data,
                           unsigned int size)
    {
        *src = Address::BROADCAST;
        *dst = Address::BROADCAST;
        *type = buf->frame()->data<Header>()->version();
        memcpy(data, buf->frame()->data<Frame>(), (buf->size() < size ? buf->size() : size));
        return buf->size();
    }

    int send(Buffer * buf) {
        if(buf->is_new)
            buf->id = id(buf);

        if(buf->destined_to_me)
            buf->offset = OFFSET_LOWER_BOUND;
        else {
            if(buf->offset < OFFSET_GENERAL_LOWER_BOUND)
                buf->offset = OFFSET_GENERAL_LOWER_BOUND;
            else if(buf->offset > OFFSET_GENERAL_UPPER_BOUND)
                buf->offset = OFFSET_GENERAL_UPPER_BOUND;
        }

        // Check if we already have this message queued. If so, replace it
        Buffer::Element * next;
        for(Buffer::Element * el = _tx_schedule.head(); el; el = next) {
            next = el->next();
            Buffer * queued_buf = el->object();
            if(queued_buf->id == buf->id) {
                if(equals(queued_buf, buf)) {
                    if(_tx_pending && (_tx_pending == queued_buf))
                        _tx_pending = buf;
                    if(break_silence && _silence_buffer && (_silence_buffer == queued_buf))
                        _silence_buffer = buf;
                    _tx_schedule.remove(queued_buf->link());
                    buf->progress_bits |= queued_buf->progress_bits;
                    delete queued_buf;
                } else {
                    /*
                    std::cout << "Hash collision detected!" << std::endl;
                    print(buf);
                    print(queued_buf);
                    std::cout << id(buf) << std::endl;
                    std::cout << queued_buf->frame()->data<Header>()->time() << " " << queued_buf->frame()->data<Header>()->origin() << " " << queued_buf->id << " " << queued_buf->is_new << std::endl;
                    std::cout << buf->frame()->data<Header>()->time() << " " << buf->frame()->data<Header>()->origin() << " " << buf->id << " " << buf->is_new << std::endl;
                    //throw cRuntimeError("Hash collision detected!");
                     */
                    _hash_collisions++;
                }
            }
        }
        _tx_schedule.insert(buf->link());

        return buf->size();
    }

private:

    // State Machine

    static void update_tx_schedule(const Interrupt_Id & id) { _instance[id]->update_tx_schedule(); }
    void update_tx_schedule()
    {
        Timer::int_disable();
        Watchdog::kick();
        Radio::trace() << "State: Update TX Schedule";

        // State: Update TX Schedule
        Radio::power(Power_Mode::PM_SLEEP);
        _in_rx_data = false;
        _in_rx_mf = false;

        _tx_pending = 0;

        Time_Stamp now_ts = Timer::read();
        Microsecond now_us = Timer::count2us(now_ts);

        if(break_silence && drop_expired && _silence_buffer && (_silence_buffer->deadline <= now_us))
            _silence_periods = 0;

        if(_silence_periods && silence)
            _silence_periods--;
        else {
            if(break_silence)
                _silence_buffer = 0;
            // Fetch next message and remove expired ones
            // TODO: Turn _tx_schedule into an ordered list
            for(Buffer::Element * el = _tx_schedule.head(); el;) {
                Buffer::Element * next = el->next();
                Buffer * b = el->object();
                // Drop expired messages
                if(drop_expired && (b->deadline <= now_us)) {
                    _tx_schedule.remove(el);
                    delete b;
                    _expired_bufs++;
                } else if(!_tx_pending) {
                    _tx_pending = b;
                // Prioritize ACKs
                } else if(_tx_pending->destined_to_me) {
                    if(b->destined_to_me) {
                        if(b->times_txed < _tx_pending->times_txed)
                            _tx_pending = b;
                        else if((b->times_txed == _tx_pending->times_txed)
                                && (b->deadline < _tx_pending->deadline))
                            _tx_pending = b;
                    }
//                } else if(!_tx_pending->is_new) {
//                    if(!b->is_new) {
//                        if(b->times_txed < _tx_pending->times_txed)
//                            _tx_pending = b;
//                        else if((b->times_txed == _tx_pending->times_txed)
//                                && (b->deadline < _tx_pending->deadline))
//                            _tx_pending = b;
//                        /*
//                        if(b->deadline < _tx_pending->deadline)
//                            _tx_pending = b;
//                        else if((b->deadline == _tx_pending->deadline)
//                                && (b->times_txed < _tx_pending->times_txed))
//                            _tx_pending = b;
//                            */
//                    }
                // Non-keepalives are prioritized over keepalives
                } else if((_tx_pending->frame()->data<Header>()->type() == CONTROL)
                        && (_tx_pending->frame()->data<Control>()->subtype() == KEEP_ALIVE)) {
                    if(((b->frame()->data<Header>()->type() == CONTROL)
                            && (b->frame()->data<Control>()->subtype() == KEEP_ALIVE))) {
                        if(b->times_txed < _tx_pending->times_txed)
                            _tx_pending = b;
                        else if((b->times_txed == _tx_pending->times_txed)
                                && (b->deadline < _tx_pending->deadline))
                            _tx_pending = b;
                    } else
                        _tx_pending = b;
                } else {
                    if(b->times_txed < _tx_pending->times_txed)
                        _tx_pending = b;
                    else if((b->times_txed == _tx_pending->times_txed)
                            && (b->deadline < _tx_pending->deadline))
                        _tx_pending = b;
                }
                el = next;
            }
        }

        if(_tx_pending) { // Transition: [TX pending]
            Radio::trace() << "Transition: [TX pending]";
            // State: Contend CCA (Contend part)

            Watchdog::kick();
            Radio::trace() << "State: Contend CCA (Contend part)";

            Time_Stamp offset = _tx_pending->offset;
            if(random_backoff) {
                // Increase ACK priority and decrease non-ACK priority by a random component,
                // based on number of transmission attempts.
                // This prevents permanent inteference by a given pair of nodes, and
                // makes unresponded messages have the lowest priorities
                _tx_pending->random_backoff_exponent++;
                unsigned int lim = G * _tx_pending->random_backoff_exponent;
                if((lim > OFFSET_UPPER_BOUND) || (lim == 0))
                    lim = OFFSET_UPPER_BOUND;
                if(_tx_pending->destined_to_me) {
                    offset -= ((unsigned int) (Random::random()) % lim);
                    if((offset < OFFSET_LOWER_BOUND) || (offset > OFFSET_GENERAL_LOWER_BOUND)) {
                        offset = OFFSET_LOWER_BOUND;
                        _tx_pending->random_backoff_exponent--;
                    }
                } else {
                    offset += ((unsigned int) (Random::random()) % lim);

                    if(offset < OFFSET_GENERAL_LOWER_BOUND) {
                        offset = OFFSET_GENERAL_LOWER_BOUND;
                        _tx_pending->random_backoff_exponent--;
                    } else if(offset > OFFSET_UPPER_BOUND) {
                        offset = OFFSET_UPPER_BOUND;
                        _tx_pending->random_backoff_exponent--;
                    }
                }
            }

            new (&_mf) Microframe((!_tx_pending->destined_to_me) && _tx_pending->downlink,
                    _tx_pending->id, N_MICROFRAMES - 1, _tx_pending->hint);

            Radio::trace() << "offset = " << offset;
            _offset_vector->record(Radio::count2us(offset) / 1000000.0);

            Timer::interrupt(now_ts + offset - SLEEP_TO_RX_DELAY, start_cca);

            //Timer::interrupt(now_ts + offset, cca);
        } else { // Transition: [No TX pending]
            // State: Sleep S
            Radio::trace() << "Transition [No TX pending]";
            Radio::trace() << "State: Sleep S";
            Watchdog::kick();
            Timer::interrupt(now_ts + SLEEP_PERIOD - SLEEP_TO_RX_DELAY, rx_mf);
        }
    }

    static void start_cca(const Interrupt_Id & id) { _instance[id]->start_cca(); }
    void start_cca() {
        if(multichannel)
            Radio::channel(MICROFRAME_CHANNEL);
        Radio::power(PM_LIGHT);
        Radio::listen();
        Radio::schedule_cca(Timer::read() + SLEEP_TO_RX_DELAY, CCA_TIME, cca);
    }

    // State: Contend CCA (CCA part)
    static void cca(const Interrupt_Id & id) { _instance[id]->cca(); }
    void cca() {
        Radio::trace() << "State: Contend CCA (CCA part)";
        Watchdog::kick();
        assert(N_MICROFRAMES > 1);

        //Radio::listen();

        // Try to send the first Microframe
        if(Radio::cca()) {
            Radio::copy_to_nic(&_mf, MICROFRAME_SIZE);
            _mf_time = Timer::read() + RX_TO_TX_DELAY;
            if(Radio::transmit()) { // Transition: [Channel free]
                _mfs_sent++;
                _mf_time += TIME_BETWEEN_MICROFRAMES + MICROFRAME_TIME;
                _mf.dec_count();
                Watchdog::kick();
                while(!Radio::tx_done());
                //Radio::copy_to_nic(&_mf, MICROFRAME_SIZE);
                //tx_mf();
                if(radio_sleeps_after_tx)
                    Timer::interrupt(_mf_time - SLEEP_TO_TX_DELAY, tx_mf);
                else
                    Timer::interrupt(_mf_time - RX_TO_TX_DELAY, tx_mf);
            } else { // Transition: [Channel busy]
                rx_mf(false);
            }
        } else { // Transition: [Channel busy]
            rx_mf(false);
        }
    }

    // State: RX MF (part 1/3)
    static void rx_mf(const Interrupt_Id & id) { _instance[id]->rx_mf(); }
    void rx_mf(bool radio_sleeping = true) {
#ifndef DISABLE_TRACE
        Radio::trace() << "State: RX MF (part 1/3)";
        Radio::trace() << "Timer::read() = " << Timer::read();
        Radio::trace() << "RX_MF_TIMEOUT = " << RX_MF_TIMEOUT;
#endif

        Watchdog::kick();
        _in_rx_data = false;
        _in_rx_mf = true;

        // If timeout is reached, Transition: [No MF]
        Timer::interrupt(Timer::read() + RX_MF_TIMEOUT + (radio_sleeping ? SLEEP_TO_RX_DELAY : 0), update_tx_schedule);

        if(multichannel && radio_sleeping)
            Radio::channel(MICROFRAME_CHANNEL);
        Radio::power(Power_Mode::PM_FULL);
        Radio::listen();
    }

    // State: RX Data (part 1/3)
    static void rx_data(const Interrupt_Id & id) { _instance[id]->rx_data(); }
    void rx_data() {
        Radio::trace() << "State: RX Data (part 1/3)";
        Watchdog::kick();
        _in_rx_data = true;
        _in_rx_mf = false;

        // Set timeout
        Timer::interrupt(Timer::read() + RX_DATA_TIMEOUT + SLEEP_TO_RX_DELAY, update_tx_schedule);

        if(multichannel)
            Radio::channel(_receiving_data_channel);
            //Radio::channel(DATA_CHANNEL);
        Radio::power(Power_Mode::PM_FULL);
        Radio::listen();
    }

    // State: TX MFs
    static void tx_mf(const Interrupt_Id & id) { _instance[id]->tx_mf(); }
    void tx_mf() {
#ifndef DISABLE_TRACE
        Radio::trace() << "State: TX MFs";
        Radio::trace() << (int)_mf.count();
#endif
        // The first Microframe is sent at cca()
        Radio::copy_to_nic(&_mf, MICROFRAME_SIZE);
        Radio::transmit_no_cca();
        _mfs_sent++;

        _mf_time += TIME_BETWEEN_MICROFRAMES + MICROFRAME_TIME;

        Watchdog::kick();

        if(_mf.dec_count() > 0) {
            if(radio_sleeps_after_tx)
                Timer::interrupt(_mf_time - SLEEP_TO_TX_DELAY, tx_mf);
            else
                Timer::interrupt(_mf_time - RX_TO_TX_DELAY, tx_mf);
        } else {
            if(radio_sleeps_after_tx)
                Timer::interrupt(_mf_time - SLEEP_TO_TX_DELAY, tx_data);
            else
                Timer::interrupt(_mf_time - RX_TO_TX_DELAY, tx_data);
        }
    }

    static void tx_data(const Interrupt_Id & id) { _instance[id]->tx_data(); }
    void tx_data() {
        bool transmit, remove;

        remove = _tx_pending->destined_to_me;
        transmit = true;

        if(transmit) { // Transition: [Is not dest.]
            if(multichannel)
                Radio::channel((_tx_pending->id % 14) + 11);
                //Radio::channel(DATA_CHANNEL);

            bool is_keep_alive = (_tx_pending->frame()->data<Header>()->type() == CONTROL)
                        && (_tx_pending->frame()->data<Control>()->subtype() == KEEP_ALIVE);
            _tx_pending->times_txed++;
            if(_tx_pending->times_txed > _max_times_txed)
                _max_times_txed = _tx_pending->times_txed;
            if(silence && !is_keep_alive && !_tx_pending->destined_to_me) {
                unsigned int r = Random::random();
                _silence_periods += (r % _tx_pending->times_txed) + 1;
                if(break_silence)
                    _silence_buffer = _tx_pending;
            }

            // State: TX Data
            // The precise time when this frame is actually sent by the physical layer
            _tx_pending->frame()->data<Header>()->last_hop_time(Timer::read() + SLEEP_TO_TX_DELAY);
#ifndef DISABLE_TRACE
            Radio::trace() << "State: TX Data";
#ifndef EXPLICIT_SYNC_ONLY
            Radio::trace() << "Data frame should arrive at "
                << _tx_pending->frame()->data<Header>()->last_hop_time();
#endif
#endif
            Radio::copy_to_nic(_tx_pending->frame(), _tx_pending->size());
            Radio::trace() << "TXing Data frame of size " << _tx_pending->size();
            Radio::transmit_no_cca();
            _stats.tx_packets++;
            _stats.tx_bytes += _tx_pending->size();
            if(!_tx_pending->is_new)
                _stats.tx_relayed++;
            Watchdog::kick();
            //while(!Radio::tx_done());

            _mf_time = Timer::read();

            // Keep Alive messages are never ACK'ed or forwarded
            if(is_keep_alive) {
                _keep_alives_sent++;
                _global_keep_alives_sent++;
                remove = true;
            }
        }
        if(remove) {
            Watchdog::kick();
            _tx_schedule.remove(_tx_pending->link());
            delete _tx_pending;
        } else {
            // State: Sleep S
            Radio::trace() << "State: Sleep S";
            if(!radio_sleeps_after_tx)
                Radio::power(Power_Mode::PM_SLEEP, true);
        }

        Timer::interrupt(_mf_time + SLEEP_PERIOD - SLEEP_TO_RX_DELAY, rx_mf);
    }

    void free(Buffer * b);
    void print(Buffer * b);
    bool equals(Buffer *, Buffer *);

    Microframe _mf;
    Time_Stamp _mf_time;
    Hint _receiving_data_hint;
    unsigned char _receiving_data_channel;
    Buffer::List _tx_schedule;
    Buffer * _tx_pending;
    bool _in_rx_mf;
    bool _in_rx_data;

protected:
    static TSTP_MAC<Radio> * _instance[2048];
    static unsigned int _instances;
    unsigned int _unit;

public:
    static unsigned int _global_keep_alives_sent;
    unsigned int _keep_alives_sent;
    unsigned int _mfs_sent;
    unsigned int _expired_bufs;
    unsigned int _data_sent;
    unsigned int _data_relayed;
    unsigned int _hash_collisions;

protected:
    cOutVector * _offset_vector;

private:
    Statistics _stats;
    unsigned int _silence_periods;
    Buffer * _silence_buffer;
    unsigned int _max_times_txed;
};

// The compiler makes sure that template static variables are only defined once

// Class attributes
template<typename Radio>
TSTP_MAC<Radio> * TSTP_MAC<Radio>::_instance[2048];

template<typename Radio>
unsigned int TSTP_MAC<Radio>::_instances;

template<typename Radio>
unsigned int TSTP_MAC<Radio>::_global_keep_alives_sent;

#include <castalia_nic.h>

class TSTP_MAC_NED: public IEEE802_15_4::NIC_Base<IEEE802_15_4, true>, public TSTP_MAC<Radio_Engine>
{
    typedef TSTP_MAC<Radio_Engine> MAC;
    typedef IEEE802_15_4::NIC_Base<IEEE802_15_4, true> NIC;

public:
    typedef MAC::Buffer Buffer;
    typedef NIC::Statistics Statistics;
    typedef NIC::Address Address;
    typedef NIC::Protocol Protocol;

    unsigned int period() { return MAC::period(); }
    const NIC::Statistics & statistics() { return MAC::statistics(); }
    void reset() { }

    // NIC Timer Interface
    Timer::Time_Stamp read() { return Timer::read(); }
    Timer::Time_Stamp frequency() { return Timer::frequency(); }
    double frequency_error() { return Timer::frequency_error(); }
    Timer::Time_Stamp sfd() { return Timer::read(); } // Not implemented
    void adjust(const Timer::Time_Stamp & t2, const Timer::Time_Stamp & t3) { return Timer::adjust(t2, t3); }
    Timer::Time_Stamp us2count(const Timer::Microsecond & us) { return Timer::us2count(us); }
    Timer::Microsecond count2us(const Timer::Time_Stamp & ts) { return Timer::count2us(ts); }

public:
    typedef IEEE802_15_4::Observer Observer;
    typedef IEEE802_15_4::Observed Observed;

    static unsigned int _units;

    TSTP_MAC_NED()
        : TSTP_MAC<Radio_Engine>(_units++) {
        Timer::_radio = this;
        trace() << "TSTPMAC::TSTPMAC()";
    }

    void startup();
    void initialize();

    void finishSpecific();

    void fromNetworkLayer(cPacket *, int) { } // Unused, but required by Castalia

    int send(const Address & dst,
             const Protocol & prot,
             const void * data,
             unsigned int size) {
        Buffer * b = alloc(0, dst, prot, 0, 0, size);
        memcpy(b->frame()->data<void>(), data, size);
        return send(b);
    }

    Buffer * alloc(void * nic,
                   const Address & dst,
                   const Protocol & prot,
                   unsigned int once,
                   unsigned int always,
                   unsigned int payload) {
        // Initialize the buffer
        Buffer * buf = new Buffer(0);
        new (buf) Buffer(nic, once + always + payload + sizeof(MAC::Header));
        MAC::marshal(buf, address(), dst, prot);

        return buf;
    }

    void free(Buffer * buf) {
        buf->size(0);
        buf->unlock();
    }

    int send(Buffer * buf) { return MAC::send(buf); }

    int receive(Address * src, Protocol * prot, void * data, unsigned int size) { return 0; } // Unused
    const Address & address() { return _address; }
    void address(const Address & address) {}

    void fromRadioLayer(cPacket * pkt, double rssi, double lqi) {
        trace() << "TSTPMAC::fromRadioLayer";
        TSTPMACPacket *macPkt = dynamic_cast<TSTPMACPacket*>(pkt);
        if(macPkt == NULL) {
            trace() << "macPkt == NULL";
            return;
        }

        if(_power != PM_FULL) {
            trace() << "_power != PM_FULL";
            cancelAndDelete(pkt);
            return;
        }

        Buffer * buf = new Buffer(0);
        buf->owner(this);
        buf->size(Radio_Engine::copy_from_nic(macPkt, buf->frame()));
        if(!buf->size()) {
            trace() << "!buf->size()";
            delete buf;
            cancelAndDelete(pkt);
            return;
        }

        buf->rssi = rssi; // TODO

        buf->sfd_time_stamp = Timer::read()
            - Timer::us2count(
                PROCESSING_DELAY * 1000000ull
                    + buf->size() * 1000000ull / Phy_Layer::BYTE_RATE);

        if(MAC::pre_notify(buf)) {
            bool notified = notify(
                reinterpret_cast<IEEE802_15_4::Header *>(buf->frame())->type(),
                buf);
            if(!MAC::post_notify(buf) && !notified)
                delete buf; // No one was waiting for this frame
        } else {
            delete buf;
        }

        cancelAndDelete(pkt);
    }

    void mac2net(cPacket * pkt) {
        toNetworkLayer(pkt);
    }

private:
    Address _address;
};

#endif /* TSTPMAC_H_ */
