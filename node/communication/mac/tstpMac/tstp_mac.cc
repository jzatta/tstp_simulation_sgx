#include <tstp_mac.h>
#include <tstp.h>

Define_Module(TSTP_MAC_NED);

unsigned int TSTP_MAC_NED::_units = 0;

void TSTP_MAC_NED::initialize()
{
    VirtualMac::initialize();
    TSTP::_instances[_unit]->_nic =  reinterpret_cast<NIC*>(this);
}

void TSTP_MAC_NED::startup()
{
    trace() << "TSTPMAC::startup(), _unit = " << _unit;

    Radio_Engine::start();

    int_disable();
    toRadioLayer(createRadioCommand(SET_STATE, SLEEP));

    string st = getParentModule()->getSubmodule("Radio")->par("stateAfterTX");
    if(st.compare("RX") == 0)
        radio_sleeps_after_tx = false;
    else if(st.compare("SLEEP") == 0)
        radio_sleeps_after_tx = true;
    else
        throw cRuntimeError("stateAfterTX parameter not valid!");

    RoutingPacket * pkt = new RoutingPacket("TSTP pointer check", NETWORK_LAYER_PACKET);
    pkt->setBitLength(reinterpret_cast<int64>(reinterpret_cast<NIC*>(this)));
    trace() << "TSTPMAC::startup(): this = " << reinterpret_cast<int64>(reinterpret_cast<NIC*>(this));
    toNetworkLayer(pkt);

    int mfs = par("microframeSize");

    Tu = Timer::us2count(Phy_Layer::TURNAROUND_TIME);
    G = Timer::us2count(Phy_Layer::CCA_TX_GAP);

    if(mfs < 0)
        MICROFRAME_SIZE = sizeof(Microframe);
    else
        MICROFRAME_SIZE = mfs;

    INT_HANDLING_DELAY = par("intHandlingDelay"); // Time delay between scheduled tx_mf interrupt and actual Radio TX
    INT_HANDLING_DELAY = Timer::us2count(INT_HANDLING_DELAY);

    SLEEP_TO_TX_DELAY = INT_HANDLING_DELAY + Timer::us2count(Radio_Engine::SLEEP_TO_TX_DELAY);
    RX_TO_TX_DELAY = INT_HANDLING_DELAY + Timer::us2count(Radio_Engine::RX_TO_TX_DELAY);
    SLEEP_TO_RX_DELAY = Timer::us2count(Radio_Engine::SLEEP_TO_RX_DELAY);

    // Time to send a single Microframe (including PHY headers)
    Ts = Timer::us2count((MICROFRAME_SIZE + Phy_Layer::PHY_HEADER_SIZE) * 1000000ull / Phy_Layer::BYTE_RATE);
    MICROFRAME_TIME = Ts;

    unsigned int radio_delay = INT_HANDLING_DELAY + 1;
    if(radio_sleeps_after_tx)
        radio_delay += Timer::us2count(Radio_Engine::SLEEP_TO_TX_DELAY + Radio_Engine::TX_TO_SLEEP_DELAY);
    else
        radio_delay += Timer::us2count(Radio_Engine::RX_TO_TX_DELAY + Radio_Engine::TX_TO_RX_DELAY);

    Ti = (radio_delay > Tu) ? radio_delay : Tu;
    TIME_BETWEEN_MICROFRAMES = Ti;

    Tr = 2*Ts + Ti + (2*Ts + Ti) / 10;
    RX_MF_TIMEOUT = Tr;

    NMF = par("NMF");
    if(NMF == 0) {
        unsigned int DESIRED_PERIOD = par("period");
        if(DESIRED_PERIOD == 0) {
            // Calculate period based on user-defined dutyCycle
            DESIRED_DUTY_CYCLE = par("dutyCycle").doubleValue() * 1000000.0;

            NMF = 1 + (((1000000ull * Tr) / DESIRED_DUTY_CYCLE) + (Ti + Ts) - 1) / (Ti + Ts);
            if(NMF > Microframe::COUNT_MAX + 1)
                NMF = Microframe::COUNT_MAX + 1;
            N_MICROFRAMES = NMF;

            CI = Ts + (NMF - 1) * (Ts + Ti);
            PERIOD = CI;

            DUTY_CYCLE = Tr * 1000000ull / CI;

            if(DUTY_CYCLE > DESIRED_DUTY_CYCLE)
                throw cRuntimeError("TSTP_MAC is unable to provide a duty cycle smaller than or equal to DESIRED_DUTY_CYCLE");

        } else {
            DESIRED_PERIOD = Timer::us2count(DESIRED_PERIOD);
            // Calculate duty cycle based on user-defined period
            NMF = 1 + ((DESIRED_PERIOD - Ts) / (Ti + Ts));
            if(NMF > Microframe::COUNT_MAX + 1)
                NMF = Microframe::COUNT_MAX + 1;
            N_MICROFRAMES = NMF;

            CI = Ts + (NMF - 1) * (Ts + Ti);
            PERIOD = CI;

            if(PERIOD > DESIRED_PERIOD)
                throw cRuntimeError("TSTP_MAC is unable to provide a period smaller than or equal to DESIRED_PERIOD");

            DUTY_CYCLE = Tr * 1000000ull / CI;
        }
    } else {
        if(NMF > Microframe::COUNT_MAX + 1)
            NMF = Microframe::COUNT_MAX + 1;
        N_MICROFRAMES = NMF;
        CI = Ts + (NMF - 1) * (Ts + Ti);
        PERIOD = CI;
        DUTY_CYCLE = Tr * 1000000ull / CI;
    }

    PERIOD_US = Timer::count2us(PERIOD);
    SLEEP_PERIOD = CI - RX_MF_TIMEOUT;

    DATA_LISTEN_MARGIN = (TIME_BETWEEN_MICROFRAMES + MICROFRAME_TIME) * par("dataListenMarginMultiplier").doubleValue(); // Subtract this amount when calculating time until data transmission
    if(DATA_LISTEN_MARGIN < Timer::us2count(PROCESSING_DELAY * 1000000))
        DATA_LISTEN_MARGIN = Timer::us2count(PROCESSING_DELAY * 1000000);

    DATA_SKIP_TIME = Timer::us2count((Phy_Layer::MTU + Phy_Layer::PHY_HEADER_SIZE) * 1000000ull / Phy_Layer::BYTE_RATE);

    RX_DATA_TIMEOUT = DATA_SKIP_TIME + DATA_LISTEN_MARGIN;

    CCA_TIME = Tr;

    random_backoff = par("randomBackoff");
    silence = par("silence");

    OFFSET_LOWER_BOUND = G + SLEEP_TO_RX_DELAY;
    OFFSET_GENERAL_LOWER_BOUND = OFFSET_LOWER_BOUND + 3 * G;
    OFFSET_UPPER_BOUND = SLEEP_PERIOD - CCA_TIME - RX_TO_TX_DELAY - MICROFRAME_TIME;
    OFFSET_GENERAL_UPPER_BOUND = OFFSET_UPPER_BOUND - 3 * G;

    if(_unit == 0) {
        std::cout << "NMF = " << NMF << std::endl;
        std::cout << "CI = " << CI << std::endl;
        std::cout << "DUTY_CYCLE = " << DUTY_CYCLE << std::endl;
    }
    MAC::constructor_epilogue();
}

void TSTP_MAC_NED::finishSpecific()
{
    recordScalar("TX Schedule final size", _tx_schedule.size());
    recordScalar("Microframes sent", _mfs_sent);
    recordScalar("Buffers expired", _expired_bufs);
    recordScalar("Data frames sent", _stats.tx_packets);
    recordScalar("Bytes sent", _stats.tx_bytes);
    recordScalar("Hash collisions", _hash_collisions);
    recordScalar("Max times txed", _max_times_txed);
    recordScalar("Keep Alives sent", _keep_alives_sent);

    recordScalar("drop_expired", drop_expired);
    recordScalar("Radio RX processing delay", PROCESSING_DELAY);

    if(_unit == 0) {
        recordScalar("Global Keep Alives sent", _global_keep_alives_sent);
        recordScalar("MICROFRAME_SIZE", MICROFRAME_SIZE);
        recordScalar("DESIRED_DUTY_CYCLE", DESIRED_DUTY_CYCLE);
        recordScalar("INT_HANDLING_DELAY", INT_HANDLING_DELAY);
        recordScalar("SLEEP_TO_TX_DELAY", SLEEP_TO_TX_DELAY);
        recordScalar("RX_TO_TX_DELAY", RX_TO_TX_DELAY);
        recordScalar("Tu", Tu);
        recordScalar("Ti", Ti);
        recordScalar("TIME_BETWEEN_MICROFRAMES", TIME_BETWEEN_MICROFRAMES);
        recordScalar("Ts", Ts);
        recordScalar("MICROFRAME_TIME", MICROFRAME_TIME);
        recordScalar("Tr", Tr);
        recordScalar("RX_MF_TIMEOUT", RX_MF_TIMEOUT);
        recordScalar("NMF", NMF);
        recordScalar("N_MICROFRAMES", N_MICROFRAMES);
        recordScalar("CI", CI);
        recordScalar("PERIOD", PERIOD);
        recordScalar("SLEEP_PERIOD", SLEEP_PERIOD);
        recordScalar("DUTY_CYCLE", DUTY_CYCLE);
        recordScalar("DATA_LISTEN_MARGIN", DATA_LISTEN_MARGIN);
        recordScalar("DATA_SKIP_TIME", DATA_SKIP_TIME);
        recordScalar("RX_DATA_TIMEOUT", RX_DATA_TIMEOUT);
        recordScalar("G", G);
        recordScalar("CCA_TIME", CCA_TIME);
        recordScalar("OFFSET_GENERAL_LOWER_BOUND", OFFSET_GENERAL_LOWER_BOUND);
        recordScalar("OFFSET_GENERAL_UPPER_BOUND", OFFSET_GENERAL_UPPER_BOUND);
        recordScalar("OFFSET_UPPER_BOUND", OFFSET_UPPER_BOUND);
        recordScalar("OFFSET_LOWER_BOUND", OFFSET_LOWER_BOUND);
        recordScalar("Preamble time", ((double)N_MICROFRAMES * ((double)MICROFRAME_TIME + (double)TIME_BETWEEN_MICROFRAMES) / 1000000.0));
    }

    if(_unit == 0) {
        unsigned long long sum = 0;
        unsigned long long sum_sq = 0;
        unsigned long long hash_collision_sum = 0;
        unsigned long long expired_bufs_sum = 0;
        unsigned long long crc_error_sum = 0;
        unsigned int non_sinks = 0;


        for(unsigned int i = 0; i < _instances; i++) {
            TSTP_MAC<Radio_Engine> * mac = _instance[i];
            class TSTP * tstp = TSTP::_instances[i];
            // Do not count sinks for Fairness Index
            if(!tstp->_sink) {
                non_sinks++;
                sum += mac->statistics().tx_packets;
                sum_sq += mac->statistics().tx_packets * mac->statistics().tx_packets;
            }
            hash_collision_sum += mac->_hash_collisions;
            expired_bufs_sum += mac->_expired_bufs;
            crc_error_sum += mac->_crc_errors;
        }
        double num = sum * sum;
        double den = non_sinks * sum_sq;
        num /= den;
        recordScalar("Fairness Index", num);
        recordScalar("Total hash collisions", hash_collision_sum);
        recordScalar("Total buffers expired", expired_bufs_sum);
        recordScalar("Instances", _instances);
    }

    Radio_Engine::end();

    _units = 0;
}
