//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSTP_SYNC_MAC_NOMF_H_
#define TSTP_SYNC_MAC_NOMF_H_

#include <tstp_common.h>
#include <observer.h>
//#include <dsrc_phy.h>

__USING_SYS

template<typename Radio>
class TSTP_SYNC_MAC_NOMF: public TSTP_Common, public Radio
{
    friend class TSTP_SYNC_MAC_NOMF_NED;

protected:
    typedef IEEE802_15_4 Phy_Layer;
    //typedef DSRC_Phy Phy_Layer;

    enum State {
        UPDATE_TX_SCHEDULE = 0,
        SLEEP_S            = 1,
        RX_MF              = 2,
        SLEEP_DATA         = 3,
        RX_DATA            = 4,
        BACKOFF            = 5,
        CCA                = 6,
        TX_MF              = 7,
        TX_DATA            = 8,
    };

public:
    using TSTP_Common::Address;
    using TSTP_Common::Header;
    using TSTP_Common::Frame;
    typedef typename Radio::Timer Timer;
    typedef typename Radio::Timer::Time_Stamp Time_Stamp;

    //static const unsigned int MTU = Frame::MTU;

    const Statistics & statistics() { return _stats; }
    unsigned int period() { return CONTENTION_PERIOD; }

private:
public: //TODO: public for debugging

    static const bool sniffer = false;
    static const bool state_machine_debugged = true;
    static const bool always_tx_data = true;

    bool radio_sleeps_after_tx;
    unsigned int INT_HANDLING_DELAY; // Time delay between scheduled tx_mf interrupt and actual Radio TX
    unsigned int SLEEP_TO_TX_DELAY;
    unsigned int SLEEP_TO_RX_DELAY;
    unsigned int RX_TO_TX_DELAY;
    static const unsigned int Tu = Phy_Layer::TURNAROUND_TIME;
    unsigned int CI;
    unsigned int PERIOD;
    unsigned int DUTY_CYCLE;
    unsigned int DATA_LISTEN_MARGIN;
    unsigned int RX_DATA_TIMEOUT;
    static const unsigned int G = Phy_Layer::CCA_TX_GAP;
    unsigned int CCA_TIME;

    unsigned int CONTENTION_PERIOD;
    unsigned int DATA_SLOT_TIME;
    unsigned int RX_PRE_TIME;

    unsigned int MAXIMUM_DRIFT;

    bool random_backoff;
    bool silence;
    unsigned int OFFSET_GENERAL_LOWER_BOUND;
    unsigned int OFFSET_GENERAL_UPPER_BOUND;
    unsigned int OFFSET_LOWER_BOUND;
    unsigned int OFFSET_UPPER_BOUND;

    typedef typename Radio::Timer Watchdog;

protected:
    TSTP_SYNC_MAC_NOMF(unsigned int unit = 0)
        : Radio(unit),
            _unit(unit),
            _data_sent(0),
            _data_relayed(0),
            _silence_periods(0),
            _max_times_txed(0)
    {
        Radio::trace() << "TSTP_SYNC_MAC_NOMF(u=" << unit << ")" << endl;
        _instances++;
        _instance[unit] = this;
        _global_keep_alives_sent = 0;
    }

    ~TSTP_SYNC_MAC_NOMF() {
        for(Buffer::Element * el = _tx_schedule.head(); el; el = _tx_schedule.head()) {
            Buffer * b = el->object();
            _tx_schedule.remove(el);
            if(b)
                free(b);
        }
        delete _offset_vector;
        delete _pickup;
        delete _ids_txed;
        _instance[_unit] = 0;
        _instances--;
    }

    // Called after the Radio's constructor
    void constructor_epilogue() {
        Radio::trace() << "TSTP_SYNC_MAC_NOMF::constructor_epilogue()";
        Radio::trace() << "_unit = " << _unit;
        _offset_vector = new cOutVector("Offset");
        _pickup = new cOutVector("MAC Pickup Time");
        _ids_txed = new cOutVector("Frame IDs transmitted");
        if(sniffer)
            sniff();
        else {
            Watchdog::enable();
            update_tx_schedule();
        }
    }

    void sniff() {
        Radio::power(Power_Mode::PM_FULL);
        Radio::listen();
    }

    // Filter and assemble RX Buffer Metainformation
    bool pre_notify(Buffer * buf) {
        Radio::trace() << "State: RX Data (part 2/3)";

        buf->is_new = false;

        Radio::power(Power_Mode::PM_SLEEP);

        // Initialize Buffer Metainformation
        buf->is_microframe = false;
        buf->trusted = false;
        buf->random_backoff_exponent = 0;
        buf->offset = 1000000llu * OFFSET_GENERAL_UPPER_BOUND;
        buf->progress_bits = 0;
        buf->times_txed = 0;

        Radio::trace() << "TSTP_SYNC_MAC_NOMF::pre_notify: Data frame of size "
            << buf->size() << " received: " << buf->frame() << " at "
            << Radio::Timer::count2us(buf->sfd_time_stamp) << endl;

        return true;
    }

    bool post_notify(Buffer * buf) {
        Radio::trace() << "State: RX Data (part 3/3)";

        Watchdog::kick();

        unsigned char received_bitmap = 1; // In multisink, this comes from the microframe
        // Clear scheduled messages with same ID
        Buffer::Element * next;
        for(Buffer::Element * el = _tx_schedule.head(); el; el = next) {
            next = el->next();
            Buffer * queued_buf = el->object();
            if(queued_buf->my_distance >= buf->sender_distance) {
                if(equals(queued_buf, buf)) {
                    for(unsigned int bit_position = 0; bit_position < sizeof(unsigned char); bit_position++) {
                        /* For each bit, perform the following operation:
                                 1 @ 0 @ 0 = 0
                                 1 @ 0 @ 1 = 0
                                 1 @ 1 @ 0 = 1
                                 1 @ 1 @ 1 = 1
                                 0 @ X @ Y = Y
                               with queued_buf->progress_bits = received_bitmap @ buf->progress_bits @ queued_buf->progress_bits
                         */
                        if(received_bitmap & (1 << bit_position)) {
                            unsigned char bit =
                                (received_bitmap & (1 << bit_position))
                                & (buf->progress_bits
                                    & (1 << bit_position));
                            queued_buf->progress_bits &=
                                ~(1 << bit_position);
                            queued_buf->progress_bits &= bit;
                        }
                    }
                    if(queued_buf->progress_bits == 0) {
                        Radio::trace()
                        << "TSTP_SYNC_MAC_NOMF::pre_notify: ACK received, ID="
                        << queued_buf->id << " at "
                        << Timer::count2us(buf->sfd_time_stamp) << endl;
                        _tx_schedule.remove(el);
                        delete queued_buf;
                    }
                }
            }
        }

        free(buf);
        return true;
    }

public:
    // Assemble TX Buffer Metainformation
    void marshal(Buffer * buf,
                 const Address & src,
                 const Address & dst,
                 const Type & type)
    {
        buf->is_microframe = false;
        buf->trusted = false;
        buf->is_new = true;
        buf->random_backoff_exponent = 0;
        buf->offset = 1000000llu * OFFSET_GENERAL_UPPER_BOUND;
        buf->times_txed = 0;
    }

    unsigned int unmarshal(Buffer * buf,
                           Address * src,
                           Address * dst,
                           Type * type,
                           void * data,
                           unsigned int size)
    {
        *src = Address::BROADCAST;
        *dst = Address::BROADCAST;
        *type = buf->frame()->data<Header>()->version();
        memcpy(data, buf->frame()->data<Frame>(), (buf->size() < size ? buf->size() : size));
        return buf->size();
    }

    int send(Buffer * buf) {
        bool keep_alive = (buf->frame()->data<Header>()->type() == CONTROL) && (buf->frame()->data<Control>()->subtype() == KEEP_ALIVE);
        if(keep_alive && !_tx_schedule.empty()) {
            delete buf;
            return 0;
        }

        if(sniffer) {
            delete buf;
            return 0;
        } else {

            if(buf->destined_to_me)
                buf->offset = Timer::us2count(OFFSET_LOWER_BOUND);
            else {
                // Components calculate the offset in microseconds according to their own metrics.
                // We finish the calculation here to keep SLEEP_PERIOD, G, and Timestamps
                // encapsulated by the MAC, and MAC::marshal() happens before the other components' marshal methods
                buf->offset = Timer::us2count((buf->offset / G) * G / 1000000llu);

                if(buf->offset < Timer::us2count(OFFSET_GENERAL_LOWER_BOUND))
                    buf->offset = Timer::us2count(OFFSET_GENERAL_LOWER_BOUND);
                else if(buf->offset > Timer::us2count(OFFSET_GENERAL_UPPER_BOUND))
                    buf->offset = Timer::us2count(OFFSET_GENERAL_UPPER_BOUND);
            }

            // Check if we already have this message. If so, replace it
            Buffer::Element * next;
            for(Buffer::Element * el = _tx_schedule.head(); el; el = next) {
                next = el->next();
                Buffer * queued_buf = el->object();
                if(equals(queued_buf, buf)) {
                    if(_tx_pending && (_tx_pending == queued_buf))
                        _tx_pending = buf;
                    _tx_schedule.remove(queued_buf->link());
                    buf->progress_bits |= queued_buf->progress_bits;
                    delete queued_buf;
                }
            }
            _tx_schedule.insert(buf->link());

            return buf->size();
        }
    }

private:

    // State Machine

    static void update_tx_schedule(const Interrupt_Id & id) { _instance[id]->update_tx_schedule(); }
    void update_tx_schedule()
    {
        Timer::int_disable();
        Watchdog::kick();
        Radio::trace() << "State: Update TX Schedule";

        // State: Update TX Schedule
        Radio::power(Power_Mode::PM_SLEEP);

        _tx_pending = 0;

        Time_Stamp now_ts = Timer::read();
        Microsecond now_us = Timer::count2us(now_ts);

        if(_silence_periods && silence)
            _silence_periods--;
        else {
            // Fetch next message and remove expired ones
            // TODO: Turn _tx_schedule into an ordered list
            for(Buffer::Element * el = _tx_schedule.head(); el;) {
                Buffer::Element * next = el->next();
                Buffer * b = el->object();
                if(drop_expired && (b->deadline <= now_us)) {
                    _tx_schedule.remove(el);
                    delete b;
                } else if(!_tx_pending) {
                    _tx_pending = b;
                } else if(((_tx_pending->frame()->data<Header>()->type() == CONTROL) && (_tx_pending->frame()->data<Control>()->subtype() == KEEP_ALIVE))
                    && !((b->frame()->data<Header>()->type() == CONTROL) && (b->frame()->data<Control>()->subtype() == KEEP_ALIVE))) {
                    _tx_schedule.remove(_tx_pending->link());
                    delete _tx_pending;
                    _tx_pending = b;
                } else if(_tx_pending->destined_to_me) {
                    if(b->destined_to_me) {
                        if(b->random_backoff_exponent < _tx_pending->random_backoff_exponent) {
                            _tx_pending = b;
                        } else if((b->random_backoff_exponent
                            == _tx_pending->random_backoff_exponent)
                            && (b->deadline < _tx_pending->deadline)) {
                            _tx_pending = b;
                        }
                    }
                } else {
                    if(b->random_backoff_exponent < _tx_pending->random_backoff_exponent) {
                        _tx_pending = b;
                    } else if((b->random_backoff_exponent
                    == _tx_pending->random_backoff_exponent)
                    && (b->deadline < _tx_pending->deadline)) {
                        _tx_pending = b;
                    }
                }
                el = next;
            }
        }

        if(_tx_pending) { // Transition: [TX pending]
            Radio::trace() << "Transition: [TX pending]";
            // State: Contend CCA (Contend part)

            Watchdog::kick();
            Radio::trace() << "State: Contend CCA (Contend part)";

            if(_tx_pending->is_new && _tx_pending->random_backoff_exponent == 0)
                _pickup->record(
                    (simTime().inUnit(-6)
                        - _tx_pending->data()->data<Header>()->time())
                        / 1000000.0);

            _offset = _tx_pending->offset;
            Radio::trace() << "pre_backoff_offset = " << Timer::count2us(_tx_pending->offset) << "us";
            Radio::trace() << "upper bound = " << Timer::us2count(OFFSET_UPPER_BOUND);
            if(random_backoff) {
                // Increase ACK priority and decrease non-ACK priority by a random component,
                // based on number of transmission attempts.
                // This prevents permanent inteference by a given pair of nodes, and
                // makes unresponded messages have the lowest priorities
                _tx_pending->random_backoff_exponent++;
                unsigned int lim = Timer::us2count(G) * _tx_pending->random_backoff_exponent;
                if(!lim)
                    lim = Timer::us2count(OFFSET_UPPER_BOUND);

                if(!_tx_pending->destined_to_me) {
                    _offset += ((unsigned int) (Random::random()) % lim);

                    if(_offset < Timer::us2count(OFFSET_GENERAL_LOWER_BOUND)) {
                        _offset = Timer::us2count(OFFSET_GENERAL_LOWER_BOUND);
                        _tx_pending->random_backoff_exponent--;
                    } else if(_offset > Timer::us2count(OFFSET_UPPER_BOUND)) {
                        _offset = Timer::us2count(OFFSET_UPPER_BOUND);
                        _tx_pending->random_backoff_exponent--;
                    }
                }
            }
            Radio::trace() << "post_backoff_offset = " << Timer::count2us(_offset) << "us";

            Radio::trace() << "offset = " << _offset;
            _offset_vector->record(Radio::count2us(_offset) / 1000000.0);

            Timer::interrupt(next_period_border(now_ts) - Timer::us2count(SLEEP_TO_RX_DELAY), start_cca);

        } else { // Transition: [No TX pending]
            // State: Sleep S
            Radio::trace() << "Transition [No TX pending]";
            Radio::trace() << "State: Sleep S";
            Watchdog::kick();
            Timer::interrupt(next_period_border(now_ts) - Timer::us2count(SLEEP_TO_RX_DELAY) - Timer::us2count(DATA_LISTEN_MARGIN),
                rx_pre);
        }
    }

    Time_Stamp next_period_border(const Time_Stamp & t) {
        Radio::trace() << "t = " << t;
        Radio::trace() << "n = " << t + Timer::us2count(CI) - (t % Timer::us2count(CI));
        return t + Timer::us2count(CI) - (t % Timer::us2count(CI));
    }
    Time_Stamp last_period_border(const Time_Stamp & t) {
        Radio::trace() << "t = " << t;
        Radio::trace() << "CI = " << Timer::us2count(CI);
        Radio::trace() << "l = " << t - (t % Timer::us2count(CI));
        return t - (t % Timer::us2count(CI));
    }

    static void start_cca(const Interrupt_Id & id) { _instance[id]->start_cca(); }
    void start_cca() {
        Radio::power(Power_Mode::PM_FULL);
        Radio::listen();
        Radio::schedule_cca(Timer::read() + Timer::us2count(SLEEP_TO_RX_DELAY), _offset, cca);
    }

    // State: Contend CCA (CCA part)
    static void cca(const Interrupt_Id & id) { _instance[id]->cca(); }
    void cca()
    {
        Radio::trace() << "State: Contend CCA (CCA part)";
        Watchdog::kick();

        if(Radio::cca()) { // Transition: [Channel free]
            tx_data();
        } else { // Transition: [Channel busy]
            rx_data();
        }
    }

    static void skip_data_next(const Interrupt_Id & id) { _instance[id]->skip_data_next(); }
    void skip_data_next() {
        Radio::trace() << "State: Skip Data";
        Radio::power(Power_Mode::PM_SLEEP);
        Timer::interrupt(next_period_border(Timer::read()) + Timer::us2count(DATA_SLOT_TIME), update_tx_schedule);
    }

    // State: RX Pre
    static void rx_pre(const Interrupt_Id & id) { _instance[id]->rx_pre(); }
    void rx_pre() {
        Radio::trace() << "State: RX Pre";
        Watchdog::kick();

        Radio::power(Power_Mode::PM_FULL);
        Radio::listen();

        Radio::schedule_cca(Timer::read() + Timer::us2count(SLEEP_TO_RX_DELAY), Timer::us2count(RX_PRE_TIME), rx_or_skip_data);
    }

    static void rx_or_skip_data(const Interrupt_Id & id) { _instance[id]->rx_or_skip_data(); }
    void rx_or_skip_data() {
        if(Radio::cca())
            skip_data();
        else
            rx_data();
    }

    void skip_data() {
        Radio::trace() << "State: Skip Data (1)";
        Radio::power(Power_Mode::PM_SLEEP);
        Timer::interrupt(last_period_border(Timer::read()) + Timer::us2count(RX_PRE_TIME + DATA_SLOT_TIME), update_tx_schedule);
    }

    // State: RX Data (part 1/3)
    void rx_data() {
        Radio::trace() << "State: RX Data (part 1/3)";
        Watchdog::kick();

        // Set timeout
        Timer::interrupt(last_period_border(Timer::read()) + Timer::us2count(RX_PRE_TIME + DATA_SLOT_TIME), update_tx_schedule);
    }

    static void tx_data(const Interrupt_Id & id) { _instance[id]->tx_data(); }
    void tx_data() {
        bool remove;

        remove = _tx_pending->destined_to_me;

        _tx_pending->times_txed++;
        if(_tx_pending->times_txed > _max_times_txed)
            _max_times_txed = _tx_pending->times_txed;
        if(silence && !_tx_pending->destined_to_me) {
            unsigned int r = Random::random();
            _silence_periods += (r % _tx_pending->times_txed) + 1;
        }
        last_send = simTime();
        if(first_send == 0)
            first_send = simTime();

        // State: TX Data
        Radio::trace() << "State: TX Data";
        Radio::trace() << "Data should arrive at " << Timer::count2us(Timer::read()) + RX_TO_TX_DELAY;
        _tx_pending->frame()->data<Header>()->last_hop_time(Timer::read() + Timer::us2count(RX_TO_TX_DELAY));
        Radio::copy_to_nic(_tx_pending->frame(), _tx_pending->size());
        Radio::trace() << "TXing Data frame of size " << _tx_pending->size();
        Radio::transmit_no_cca();
        _stats.tx_packets++;
        _stats.tx_bytes += _tx_pending->size();
        if(!_tx_pending->is_new)
            _stats.tx_relayed++;
        Watchdog::kick();
        //while(!Radio::tx_done());

        // Keep Alive messages are never ACK'ed or forwarded
        if((_tx_pending->frame()->data<Header>()->type() == CONTROL)
            && (_tx_pending->frame()->data<Control>()->subtype()
                == KEEP_ALIVE)) {
            _global_keep_alives_sent++;
            remove = true;
        }

        if(remove) {
            Watchdog::kick();
            _tx_schedule.remove(_tx_pending->link());
            delete _tx_pending;
        }

        // State: Sleep S
        Radio::trace() << "State: Sleep S";
        if(!radio_sleeps_after_tx)
            Radio::power(Power_Mode::PM_SLEEP, true);
        Timer::interrupt(next_period_border(Timer::read() + Timer::us2count(RX_PRE_TIME + DATA_SLOT_TIME)) - Timer::us2count(SLEEP_TO_RX_DELAY) - Timer::us2count(DATA_LISTEN_MARGIN), rx_pre);
    }

    void free(Buffer * b);
    void print(Buffer * b);
    bool equals(Buffer *, Buffer *);

    Buffer::List _tx_schedule;
    Buffer * _tx_pending;

protected:
public:
    static TSTP_SYNC_MAC_NOMF<Radio> * _instance[2048];
    static unsigned int _instances;
    unsigned int _unit;

public:
    static unsigned int _global_keep_alives_sent;
    unsigned int _data_sent;
    unsigned int _data_relayed;
    simtime_t first_send;
    simtime_t last_send;
    Time_Stamp _offset;

protected:
    unsigned int _F;
    cOutVector * _offset_vector;
    cOutVector * _pickup;
    cOutVector * _ids_txed;

private:
    Statistics _stats;
    unsigned int _silence_periods;
    unsigned int _max_times_txed;
};

// The compiler makes sure that template static variables are only defined once

// Class attributes
template<typename Radio>
TSTP_SYNC_MAC_NOMF<Radio> * TSTP_SYNC_MAC_NOMF<Radio>::_instance[2048];

template<typename Radio>
unsigned int TSTP_SYNC_MAC_NOMF<Radio>::_instances;

template<typename Radio>
unsigned int TSTP_SYNC_MAC_NOMF<Radio>::_global_keep_alives_sent;

#include <castalia_nic.h>

class TSTP_SYNC_MAC_NOMF_NED: public IEEE802_15_4::NIC_Base<IEEE802_15_4, true>, public TSTP_SYNC_MAC_NOMF<Radio_Engine>
{
    typedef TSTP_SYNC_MAC_NOMF<Radio_Engine> MAC;
    typedef IEEE802_15_4::NIC_Base<IEEE802_15_4, true> NIC;

public:
    typedef MAC::Buffer Buffer;
    typedef NIC::Statistics Statistics;
    typedef NIC::Address Address;
    typedef NIC::Protocol Protocol;

    unsigned int period() { return MAC::period(); }
    const NIC::Statistics & statistics() { return MAC::statistics(); }
    void reset() { }

    // NIC Timer Interface
    Timer::Time_Stamp read() { return Timer::read(); }
    Timer::Time_Stamp frequency() { return Timer::frequency(); }
    double frequency_error() { return Timer::frequency_error(); }
    Timer::Time_Stamp sfd() { return Timer::read(); } // Not implemented
    void adjust(const Timer::Time_Stamp & t2, const Timer::Time_Stamp & t3) { return Timer::adjust(t2, t3); }
    Timer::Time_Stamp us2count(const Timer::Microsecond & us) { return Timer::us2count(us); }
    Timer::Microsecond count2us(const Timer::Time_Stamp & ts) { return Timer::count2us(ts); }

public:
    typedef IEEE802_15_4::Observer Observer;
    typedef IEEE802_15_4::Observed Observed;

    static unsigned int _units;

    TSTP_SYNC_MAC_NOMF_NED()
        : TSTP_SYNC_MAC_NOMF<Radio_Engine>(_units++) {
        Timer::_radio = this;
        trace() << "TSTPMAC::TSTPMAC()";
    }

    void startup();
    void initialize();

    void finishSpecific();

    void fromNetworkLayer(cPacket *, int) { } // Unused, but required by Castalia

    int send(const Address & dst,
             const Protocol & prot,
             const void * data,
             unsigned int size) {
        Buffer * b = alloc(0, dst, prot, 0, 0, size);
        memcpy(b->frame()->data<void>(), data, size);
        return send(b);
    }

    Buffer * alloc(void * nic,
                   const Address & dst,
                   const Protocol & prot,
                   unsigned int once,
                   unsigned int always,
                   unsigned int payload) {
        // Initialize the buffer
        Buffer * buf = new Buffer(0);
        new (buf) Buffer(nic, once + always + payload + sizeof(MAC::Header));
        MAC::marshal(buf, address(), dst, prot);

        return buf;
    }

    void free(Buffer * buf) {
        buf->size(0);
        buf->unlock();
    }

    int send(Buffer * buf) { return MAC::send(buf); }

    int receive(Address * src, Protocol * prot, void * data, unsigned int size) { return 0; } // Unused
    const Address & address() { return _address; }
    void address(const Address & address) {}

    void fromRadioLayer(cPacket * pkt, double rssi, double lqi) {
        trace() << "TSTPMAC::fromRadioLayer";
        TSTPMACPacket *macPkt = dynamic_cast<TSTPMACPacket*>(pkt);
        if(macPkt == NULL) {
            trace() << "macPkt == NULL";
            return;
        }

        if(_power != PM_FULL) {
            trace() << "_power != PM_FULL";
            cancelAndDelete(pkt);
            return;
        }

        Buffer * buf = new Buffer(0);
        buf->owner(this);
        buf->size(Radio_Engine::copy_from_nic(macPkt, buf->frame()));
        if(!buf->size()) {
            trace() << "!buf->size()";
            delete buf;
            cancelAndDelete(pkt);
            return;
        }

        buf->rssi = rssi; // TODO

        buf->sfd_time_stamp = Timer::read()
            - Timer::us2count(
                PROCESSING_DELAY * 1000000ull
                    + buf->size() * 1000000ull / Phy_Layer::BYTE_RATE);

        if(MAC::pre_notify(buf)) {
            bool notified = notify(
                reinterpret_cast<IEEE802_15_4::Header *>(buf->frame())->type(),
                buf);
            if(!MAC::post_notify(buf) && !notified)
                delete buf; // No one was waiting for this frame
        } else {
            delete buf;
        }

        cancelAndDelete(pkt);
    }

    void mac2net(cPacket * pkt) {
        toNetworkLayer(pkt);
    }

private:
    Address _address;
};

#endif /* TSTPMAC_H_ */
