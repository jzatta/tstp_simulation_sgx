#include <tstp_sync_mac_nomf.h>
#include <tstp.h>

Define_Module(TSTP_SYNC_MAC_NOMF_NED);

unsigned int TSTP_SYNC_MAC_NOMF_NED::_units = 0;

void TSTP_SYNC_MAC_NOMF_NED::initialize()
{
    VirtualMac::initialize();
    TSTP::_instances[_unit]->_nic =  reinterpret_cast<NIC*>(this);
}

void TSTP_SYNC_MAC_NOMF_NED::startup()
{
    trace() << "TSTPMAC::startup(), _unit = " << _unit;

    Radio_Engine::start();

    first_send = 0;

    int_disable();
    toRadioLayer(createRadioCommand(SET_STATE, SLEEP));

    string st = getParentModule()->getSubmodule("Radio")->par("stateAfterTX");
    if(st.compare("RX") == 0)
        radio_sleeps_after_tx = false;
    else if(st.compare("SLEEP") == 0)
        radio_sleeps_after_tx = true;
    else
        throw cRuntimeError("stateAfterTX parameter not valid!");

    RoutingPacket * pkt = new RoutingPacket("TSTP pointer check", NETWORK_LAYER_PACKET);
    pkt->setBitLength(reinterpret_cast<int64>(reinterpret_cast<NIC*>(this)));
    trace() << "TSTPMAC::startup(): this = " << reinterpret_cast<int64>(reinterpret_cast<NIC*>(this));
    toNetworkLayer(pkt);

    INT_HANDLING_DELAY = par("intHandlingDelay"); // Time delay between scheduled tx_mf interrupt and actual Radio TX

    SLEEP_TO_TX_DELAY = INT_HANDLING_DELAY + Radio_Engine::SLEEP_TO_TX_DELAY;
    RX_TO_TX_DELAY = INT_HANDLING_DELAY + Radio_Engine::RX_TO_TX_DELAY;
    SLEEP_TO_RX_DELAY = Radio_Engine::SLEEP_TO_RX_DELAY;

    unsigned int radio_delay = INT_HANDLING_DELAY + 1;
    if(radio_sleeps_after_tx)
        radio_delay += Radio_Engine::SLEEP_TO_TX_DELAY + Radio_Engine::TX_TO_SLEEP_DELAY;
    else
        radio_delay += Radio_Engine::RX_TO_TX_DELAY + Radio_Engine::TX_TO_RX_DELAY;

    CONTENTION_PERIOD = par("contentionSlots").doubleValue() * Phy_Layer::CCA_TX_GAP;

    unsigned int phy_header_time = (Phy_Layer::PHY_HEADER_SIZE * 1000000ull / Phy_Layer::BYTE_RATE);

    MAXIMUM_DRIFT = par("maximumDrift");

    DATA_LISTEN_MARGIN = MAXIMUM_DRIFT; // Subtract this amount when calculating time until data transmission

    RX_PRE_TIME = CONTENTION_PERIOD + phy_header_time + 2*DATA_LISTEN_MARGIN;
    DATA_SLOT_TIME = (Phy_Layer::MTU + 2*Phy_Layer::PHY_HEADER_SIZE) * 1000000ull / Phy_Layer::BYTE_RATE;

    DUTY_CYCLE = par("dutyCycle").doubleValue() * 1000000.0;
    if(DUTY_CYCLE != 0) { // Calculate period based on user-defined dutyCycle
        CI = 1000000ull * (RX_PRE_TIME + DATA_LISTEN_MARGIN + DATA_SLOT_TIME) / DUTY_CYCLE;
    } else { // user-defined period
        CI = par("period");
        DUTY_CYCLE = 1000000ull * (RX_PRE_TIME + DATA_LISTEN_MARGIN + DATA_SLOT_TIME) / CI;
    }
    PERIOD = CI;

    if(RX_PRE_TIME - DATA_LISTEN_MARGIN + DATA_SLOT_TIME > PERIOD)
        throw cRuntimeError("TSTP_MAC: RX_PRE_TIME - DATA_LISTEN_MARGIN + DATA_SLOT_TIME > PERIOD");

    CCA_TIME = 160;

    random_backoff = par("randomBackoff");
    silence = par("silence");
    OFFSET_LOWER_BOUND = CCA_TIME;
    OFFSET_GENERAL_LOWER_BOUND = 2 * OFFSET_LOWER_BOUND;
    OFFSET_UPPER_BOUND = CONTENTION_PERIOD;
    OFFSET_GENERAL_UPPER_BOUND = CONTENTION_PERIOD - 2*CCA_TX_GAP;

    if(OFFSET_GENERAL_LOWER_BOUND > CONTENTION_PERIOD)
        OFFSET_GENERAL_LOWER_BOUND = CONTENTION_PERIOD;

    if(OFFSET_GENERAL_UPPER_BOUND > CONTENTION_PERIOD)
        OFFSET_GENERAL_UPPER_BOUND = CONTENTION_PERIOD;

    if(_unit == 0) {
        std::cout << "CI = " << CI << std::endl;
        std::cout << "DUTY_CYCLE = " << DUTY_CYCLE << std::endl;
    }
    MAC::constructor_epilogue();
}

void TSTP_SYNC_MAC_NOMF_NED::finishSpecific()
{
//    if(_tx_schedule.size())
//        throw cRuntimeError("Buffers in TX Schedule!");
    recordScalar("TX Schedule final size", _tx_schedule.size());
    recordScalar("Data frames sent", _data_sent);
    recordScalar("Max times txed", _max_times_txed);

    recordScalar("drop_expired", drop_expired);
    recordScalar("sniffer", sniffer);
    recordScalar("state_machine_debugged", state_machine_debugged);
    recordScalar("First data frame send time", first_send);
    recordScalar("Last data frame send time", last_send);
    recordScalar("Radio RX processing delay", PROCESSING_DELAY);

    if(_unit == 0) {
        recordScalar("Global Keep Alives sent", _global_keep_alives_sent);
        recordScalar("INT_HANDLING_DELAY", INT_HANDLING_DELAY);
        recordScalar("SLEEP_TO_TX_DELAY", SLEEP_TO_TX_DELAY);
        recordScalar("RX_TO_TX_DELAY", RX_TO_TX_DELAY);
        recordScalar("Tu", Tu);
        recordScalar("CI", CI);
        recordScalar("PERIOD", PERIOD);
        recordScalar("DUTY_CYCLE", DUTY_CYCLE);
        recordScalar("DATA_LISTEN_MARGIN", DATA_LISTEN_MARGIN);
        recordScalar("RX_DATA_TIMEOUT", RX_DATA_TIMEOUT);
        recordScalar("G", G);
        recordScalar("CCA_TIME", CCA_TIME);
        recordScalar("OFFSET_GENERAL_LOWER_BOUND", OFFSET_GENERAL_LOWER_BOUND);
        recordScalar("OFFSET_GENERAL_UPPER_BOUND", OFFSET_GENERAL_UPPER_BOUND);
        recordScalar("OFFSET_UPPER_BOUND", OFFSET_UPPER_BOUND);
        recordScalar("OFFSET_LOWER_BOUND", OFFSET_LOWER_BOUND);

        recordScalar("CONTENTION_PERIOD", CONTENTION_PERIOD);
        recordScalar("DATA_SLOT_TIME", DATA_SLOT_TIME);
        recordScalar("RX_PRE_TIME", RX_PRE_TIME);
    }

    if(_unit == 0) {
        unsigned long long sum = 0;
        unsigned long long sum_sq = 0;
        unsigned long long crc_error_sum = _crc_errors;

        // Do not count the sink for fairness index
        for(unsigned int i = 1; i < _instances; i++) {
            TSTP_SYNC_MAC_NOMF<Radio_Engine> * mac = _instance[i];
            sum += statistics().tx_packets;
            sum_sq += statistics().tx_packets * statistics().tx_packets;
            crc_error_sum += mac->_crc_errors;
        }
        double num = sum * sum;
        double den = (_instances - 1) * sum_sq;
        num /= den;
        recordScalar("Fairness Index", num);
        recordScalar("Instances", _instances);
    }

    Radio_Engine::end();

    _units = 0;
}
