//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include "RoadMobilityManager.h"

Define_Module(RoadMobilityManager);

void RoadMobilityManager::initialize()
{
	VirtualMobilityManager::initialize();

	updateInterval = par("updateInterval");
	updateInterval = updateInterval / 1000;

	reverse = par("reverse");

	if(reverse) {
	    // reverse the direction
	    loc2_x = nodeLocation.x;
        loc2_y = nodeLocation.y;
        loc2_z = nodeLocation.z;
        loc1_x = par("xCoorDestination");
        loc1_y = par("yCoorDestination");
        loc1_z = par("zCoorDestination");
	} else {
        loc1_x = nodeLocation.x;
        loc1_y = nodeLocation.y;
        loc1_z = nodeLocation.z;
        loc2_x = par("xCoorDestination");
        loc2_y = par("yCoorDestination");
        loc2_z = par("zCoorDestination");
    }

	speed = par("speed");
	startupOffset = getParentModule()->par("startupOffset");
	destroyNode = par("destroyNode");
	destructionOffset = par("destructionOffset");

	if(destructionOffset < 0)
	    throw cRuntimeError("RoadMobilityManager: destructionOffset < 0!");

	distance = sqrt(pow(loc1_x - loc2_x, 2) + pow(loc1_y - loc2_y, 2) + pow(loc1_z - loc2_z, 2));

	if(distance > 0){
	    arrived = false;

        if (speed > 0) {
            double totalTime = distance / speed;
            double tmp = totalTime / updateInterval;

            trace() << "Travel ("<<loc1_x<<","<<loc1_y<<","<<loc1_z<<") -> ("<<loc2_x<<","<<loc2_y<<","<<loc2_z<<") in "<<totalTime<<"s ("<<speed<<" m/s)";

            incr_x = (loc2_x - loc1_x) / tmp;
            incr_y = (loc2_y - loc1_y) / tmp;
            incr_z = (loc2_z - loc1_z) / tmp;

            setLocation(loc1_x, loc1_y, loc1_z);

            scheduleAt(simTime() + startupOffset, new MobilityManagerMessage("Mobility start message", MOBILITY_START));
        }
    }else if(destroyNode)
        scheduleAt(simTime() + destructionOffset + startupOffset, new MobilityManagerMessage("Periodic location update message", MOBILITY_DESTROY_NODE));
}

void RoadMobilityManager::handleMessage(cMessage * msg)
{
	int msgKind = msg->getKind();
	switch (msgKind) {
	    case MOBILITY_START:{
	        double totalTime = distance / speed;
	        trace() << "Mobility Startup ----------------------- ("<<loc1_x<<","<<loc1_y<<","<<loc1_z<<") -> ("<<loc2_x<<","<<loc2_y<<","<<loc2_z<<") in "<<totalTime<<"s ("<<speed<<" m/s)";
	        scheduleAt(simTime() + updateInterval, new MobilityManagerMessage("Periodic location update message", MOBILITY_PERIODIC));
	        break;
	    }

		case MOBILITY_PERIODIC:{

            nodeLocation.x += incr_x;
            nodeLocation.y += incr_y;
            nodeLocation.z += incr_z;

            if (   (incr_x > 0 && nodeLocation.x >= loc2_x) || (incr_x < 0 && nodeLocation.x <= loc2_x)
                || (incr_y > 0 && nodeLocation.y >= loc2_y) || (incr_y < 0 && nodeLocation.y <= loc2_y)
                || (incr_z > 0 && nodeLocation.z >= loc2_z) || (incr_z < 0 && nodeLocation.z <= loc2_z)) {

                trace() << "Node arrived ("<<nodeLocation.x<<","<<nodeLocation.y<<","<<nodeLocation.z<<")";

                nodeLocation.x = nodeLocation.x < 0 ? 0 : nodeLocation.x > loc2_x ? loc2_x : nodeLocation.x;
                nodeLocation.y = nodeLocation.y < 0 ? 0 : nodeLocation.y > loc2_y ? loc2_y : nodeLocation.y;
                nodeLocation.z = nodeLocation.z < 0 ? 0 : nodeLocation.z > loc2_z ? loc2_z : nodeLocation.z;

                arrived = true;

                if(destroyNode){
                    scheduleAt(simTime() + destructionOffset, new MobilityManagerMessage("Periodic location update message", MOBILITY_DESTROY_NODE));
                }

            }else{
                scheduleAt(simTime() + updateInterval, new MobilityManagerMessage("Periodic location update message", MOBILITY_PERIODIC));
            }

            notifyWirelessChannel();
			trace() << "changed location(x:y:z) to " << nodeLocation.x << ":" << nodeLocation.y << ":" << nodeLocation.z;
			break;
		}

		case MOBILITY_DESTROY_NODE:{
            trace() << "Scheduled node destruction";
            destroyed = true;
		    (check_and_cast<ResourceManager*>(getParentModule()->getSubmodule("ResourceManager")))->destroyNode();
		    break;
		}

		default:{
			trace() << "WARNING: Unexpected message " << msgKind;
		}
	}

	delete msg;
	msg = NULL;
}
