//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#ifndef _MOBILITYMODULE_H_
#define _MOBILITYMODULE_H_

#include "MobilityManagerMessage_m.h"
#include "VirtualMobilityManager.h"
#include "ResourceManager.h"

using namespace std;

class RoadMobilityManager: public VirtualMobilityManager {
 private:
	/*--- The .ned file's parameters ---*/
	double updateInterval;
	double loc1_x;
	double loc1_y;
	double loc1_z;
	double loc2_x;
	double loc2_y;
	double loc2_z;
	double speed;
	double startupOffset;
	bool destroyNode;
	double destructionOffset;

	/*--- Custom class parameters ---*/
	double incr_x;
	double incr_y;
	double incr_z;
	double distance;
	bool reverse;

 protected:
	void initialize();
	void handleMessage(cMessage * msg);
};

#endif
