#ifndef _SensorNode_H_
#define _SensorNode_H_

#include "VirtualApplication.h"
#include <tstp.h>
#include <smart_data.h>
#include <epos_module.h>

using namespace EPOS::S;

class SensorNode : public VirtualApplication, public EPOS_Module {

 private:
    enum Timers{
        SMARTDATA,
        CHECK_VALUE,
    };

 protected:
	virtual void initialize();
	virtual void startup();
	void finishSpecific();
	void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
	void timerFiredCallback(int);

	//std::ostream & log() { return trace(); };

 public:
	cMessage * EPOS_scheduleAt(simtime_t t, cMessage *msg) { Enter_Method("EPOS_scheduleAt"); cMessage * msg2 = msg->dup(); scheduleAt(t, msg2); return msg2; }

	void EPOS_cancelAndDelete(cMessage *msg) { Enter_Method("EPOS_cancelAndDelete"); cancelAndDelete(msg); }
     TSTP::Microsecond now() { return _tstp->now(); }

 private:
	TSTP * _tstp;
	Temperature * _temperature;
};

#endif // _SensorNode_H_
