#include "SinkNode.h"
#include "tstp.h"

Define_Module(SinkNode);


void SinkNode::initialize()
{
    trace() << "SinkNode::initialize";
    VirtualApplication::initialize();
}

void SinkNode::startup()
{
    trace() << "SinkNode::startup";
    _tstp = check_and_cast <TSTP*>(getParentModule()->getSubmodule("Communication")->getSubmodule("Routing"));
    setTimer(0, 13);
}

void SinkNode::timerFiredCallback(int timer)
{
    trace() << "SinkNode::timerFiredCallback: " << timer;

    TSTP * node = check_and_cast<TSTP*>(getParentModule()->getParentModule()->getSubmodule("node", 1)->getSubmodule("Communication")->getSubmodule("Routing"));
#include "FULL_FILE"
#if FULL==42
    Region region(node->here(), 0, 0, -1);
    _temperature = new Temperature(this, _tstp, region, INTEREST_EXPIRY, INTEREST_PERIOD);
    trace() << "Sink Node> The Remote SmartData has been created!";
    _temperature->attach((Observer*)this);
#else
    TSTP::Security_Stub::instance()->go();
    trace() << "Sink Node> The Remote SmartData has been created!";
#endif
}

void SinkNode::update(Observed * o) {
    Temperature * temperature = static_cast<Temperature*>(o);
    trace() << "Sink Node> The Remote SmartData has been updated! Temperature: " << (Temperature::Value)*temperature << "ºC at " << temperature->time();
}

void SinkNode::fromNetworkLayer(ApplicationPacket * rcvPacket, const char *source, double rssi, double lqi)
{
    trace() << "SinkNode::fromNetworkLayer" << *rcvPacket;
}

void SinkNode::finishSpecific()
{
    trace() << "SinkNode::finishSpecific";
}
