#ifndef _SinkNode_H_
#define _SinkNode_H_

#include "VirtualApplication.h"
#include <periodic_thread.h>
#include <smart_data.h>
#include <tstp.h>

using namespace EPOS;

typedef TSTP::Coordinates Coordinates;
typedef TSTP::Region Region;

const unsigned int INTEREST_PERIOD = 1 * 1000000;
const unsigned int INTEREST_EXPIRY = 2 * INTEREST_PERIOD;

class SinkNode : public VirtualApplication, public Observer {

 protected:
	virtual void initialize();
	virtual void startup();
	void finishSpecific();
	void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
	void timerFiredCallback(int);

	void update(Observed * o);

 private:
	TSTP * _tstp;
	Temperature * _temperature;
};

#endif // _SinkNode_H_
