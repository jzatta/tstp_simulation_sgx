#include "SensorNode.h"

#include "DataPacketC_m.h"


Define_Module(SensorNode);

void SensorNode::initialize()
{
    trace() << "SensorNode::initialize";
    VirtualApplication::initialize();
}

void SensorNode::startup()
{
    trace() << "SensorNode::startup";
    _tstp = check_and_cast <TSTP*>(getParentModule()->getSubmodule("Communication")->getSubmodule("Routing"));

    setTimer(SMARTDATA, 2);
    setTimer(CHECK_VALUE, 10);
}

void SensorNode::timerFiredCallback(int timer)
{
    switch (timer) {
        case SMARTDATA+5:
        case SMARTDATA:
            _temperature = new Temperature(this, _tstp, 1, 1000000, Temperature::ADVERTISED);
            trace() << "Sensor Node> The Local SmartData has been created!";
            break;
        case CHECK_VALUE:
            setTimer(CHECK_VALUE, 5);
            trace() << "Sensor Node> Temperature: " << (Temperature::Value)*_temperature << " ºC";
            break;
        default:
            break;
    }
}

void SensorNode::fromNetworkLayer(ApplicationPacket * rcvPacket, const char *source, double rssi, double lqi)
{
	trace() << "SensorNode::fromNetworkLayer";
}

void SensorNode::finishSpecific()
{
    trace() << "SensorNode::finishSpecific";
}
