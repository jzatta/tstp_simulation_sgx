//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#ifndef _RoadTest_H_
#define _RoadTest_H_

#include "VirtualApplication.h"

class RoadTest:public VirtualApplication {
public:
    RoadTest() : receivedDataPackets(0), sentDataPackets(0), etedMin(1000000000, SIMTIME_US), etedMean(0), etedMax(0), etedVector("End-to-end Delay") {
        globalReceivedDataPackets = 0;
        globalSentDataPackets = 0;
    }

    ~RoadTest() {
        // put delete here
    }

 protected:

    enum RoadMessageType{
        DataPacket = 1,
        FSRequest = 2,
        FSResponse = 3,
    };

	// Statistics
    unsigned int receivedDataPackets;
    unsigned int sentDataPackets;

	static unsigned int globalReceivedDataPackets;
	static unsigned int globalSentDataPackets;

	simtime_t etedMin;
	simtime_t etedMean;
	simtime_t etedMax;
	cOutVector etedVector;
};

#endif // _RoadTest_H_
