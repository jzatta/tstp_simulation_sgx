//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include "Vehicle.h"
#include "RoadPacketType_m.h"

Define_Module(Vehicle);

unsigned int RoadTest::globalSentDataPackets = 0;
unsigned int RoadTest::globalReceivedDataPackets = 0;

void Vehicle::startup()
{
    dataPeriod = par("dataPeriod");
    double startupDelay = par("startupDelay");
    double startupRandomDelay = par("startupRandomDelay");
    packetTimeLimit = par("packetTimeLimit");
    dataPacketLimit = par("dataPacketLimit");
    dataPacketGenerationChance = par("dataPacketGenerationChance");

    if(dataPeriod > 0)
        setTimer(TIMER_DATA_PERIOD, startupDelay + uniform(0, startupRandomDelay));

    trace() << "Vehicle::startup";

    pktSerial = 0;
}

void Vehicle::timerFiredCallback(int timer)
{
    //if((packetTimeLimit > 0) and (simTime() >= packetTimeLimit)){
    if(((packetTimeLimit > 0) and (simTime() >= packetTimeLimit)) or mobilityModule->isArrived() ){
    //if(((packetTimeLimit > 0) and (simTime() >= packetTimeLimit)) or mobilityModule->isDestroyed() ){
        return;
    }

	switch (timer) {

	    case TIMER_DATA_PERIOD:{
	        if((dataPacketLimit <= 0) || (sentDataPackets < dataPacketLimit)) {
                setTimer(TIMER_DATA_PERIOD, dataPeriod);
                if(uniform(0, 1) < dataPacketGenerationChance)
                    sendData(pktSerial++);
            }
	        break;
	    }
	}
}

void Vehicle::fromNetworkLayer(ApplicationPacket * rcvPacket, const char *source, double rssi, double lqi)
{

	string packetName(rcvPacket->getName());

	trace() << "Vehicle - Packet received: [" << packetName << "] in " << SELF_NETWORK_ADDRESS;

}

void Vehicle::finishSpecific()
{

}


void Vehicle::sendData(double value)
{
    RoadPacketType * pkt = new RoadPacketType("Vehicle Data packet", APPLICATION_PACKET);
    pkt->setType(RoadTest::RoadMessageType::DataPacket);

    double x_coor = mobilityModule->getLocation().x;
    double y_coor = mobilityModule->getLocation().y;

    trace() << "Vehicle ("<<x_coor<<","<<y_coor<<") - Packet sent: [value="<<value<<"] [" << pkt << "] in " << SELF_NETWORK_ADDRESS;

    pkt->setData(value);

    globalSentDataPackets++;
    sentDataPackets++;

    toNetworkLayer(pkt, "-1");
}


void Vehicle::handleMessage(cMessage * msg)
{
    if (msg->getKind() == DESTROY_NODE){
        cancelTimer(TIMER_DATA_PERIOD);
    }

    VirtualApplication::handleMessage(msg);
}
