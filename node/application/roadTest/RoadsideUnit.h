//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#ifndef _RoadsideUnit_H_
#define _RoadsideUnit_H_

#include "RoadTest.h"

class RoadsideUnit:public RoadTest {
 private:

    enum RoadsideUnitTimers {
        TIMER1 = 1,
        TIMER2 = 2,
        TIMER3 = 3,
    };

 protected:
	virtual void startup();
	void finishSpecific();
	void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
	void timerFiredCallback(int);

	// NED Parameters

};

#endif // _RoadsideUnit_H_
