//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include "RoadsideUnit.h"
#include "RoadPacketType_m.h"

Define_Module(RoadsideUnit);

void RoadsideUnit::startup()
{
    trace() << "RSU::startup ("<< self <<")";
    double debug = par("debug");
    if(debug != -1){
        trace() << "Debug : " << debug;
    }

}

void RoadsideUnit::timerFiredCallback(int timer)
{
	switch (timer) {
		default:{
			break;
		}
	}
}

void RoadsideUnit::fromNetworkLayer(ApplicationPacket * rcvPacket, const char *source, double rssi, double lqi)
{
    string packetName(rcvPacket->getName());

    double x_coor = mobilityModule->getLocation().x;
    double y_coor = mobilityModule->getLocation().y;

	trace() << "RSU("<<x_coor<<","<<y_coor<<") - Packet received: [value=" << rcvPacket->getData() << "] [" << packetName << "] from "<< source <<" in " << SELF_NETWORK_ADDRESS;

	receivedDataPackets++;
    globalReceivedDataPackets++;
    simtime_t eted = (simTime() - rcvPacket->getAppNetInfoExchange().timestamp);
    if(eted < etedMin)
        etedMin = eted;
    if(eted > etedMax)
        etedMax = eted;
    etedMean += eted;
}

void RoadsideUnit::finishSpecific()
{
    recordScalar("Data Packets Sent", sentDataPackets);

    if(self == 0) {
       recordScalar("Global Data Packets Sent", globalSentDataPackets);
    }

    recordScalar("Data Packets Received", receivedDataPackets);

    if(self == 0) {
       recordScalar("Global Data Packets Received", globalReceivedDataPackets);
    }

    if(receivedDataPackets) {
        recordScalar("E-T-E Delay Min", etedMin);
        recordScalar("E-T-E Delay Mean", etedMean / receivedDataPackets);
        recordScalar("E-T-E Delay Max", etedMax);
        double num = globalReceivedDataPackets;
        double den = globalSentDataPackets;
        recordScalar("Delivery Ratio", num / den);
    }else{
        recordScalar("Delivery Ratio", 0);
    }
}
