
all:
	(cd .. && make -j 8)

full:
	@echo "#define FULL 42" > FULL_FILE

stub:
	@echo "#define FULL 13" > FULL_FILE

clean:
	(cd .. && make clean)
